import { configureStore } from "@reduxjs/toolkit";
import counter from "../slices/counter";
import { retailerReducer } from "../slices/retailer";
import stepSlice from "../slices/step";
import retailerMobile from "../slices/retailerMobile";
import notification from "../slices/notification";
import retailerAddress from "../slices/retailerAddress";
import retailerPan from "../slices/retailerPan";
import adminSlice from "../slices/admin";
export const store = configureStore({
  reducer: {
    retailerData: retailerReducer,
    counter,
    retailerMobile,
    notification,
    stepSlice,
    retailerAddress,
    retailerPan,
    adminSlice
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});
