import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getErrorMsg } from "../../helper/utils";
import MainServer from "../../services/main_service";
import { NotificationActions } from "./notification";
import { retailerActions } from "./retailer";
import { toggleShowOtp } from "./step";
const name = "retailerMobile";

const initialState = {
  mobile: "",
  error: "",
  otp: "",
  otpError: "",
  requestId: "",
  submittingMobile: false,
  submittingOtp: false,
  isMobileVerified: false,
  countryCode: "+91",
  resendCount: 0,
  resendTimer: 0,
};

const extraActions = createExtraActions();
const extraReducers = createExtraReducers();

export const retailerMobile = createSlice({
  name: "retailerMobile",
  initialState,
  reducers: {
    onChangeMobile: (state, action) => {
      state.mobile = action.payload;
    },
    onChangeOtp: (state, action) => {
      state.otp = action.payload;
    },
    setError: (state, action) => {
      state.error = action.payload;
    },
    increaseResendCount: (state) => {
      state.resendCount += 1;
    },
    setOtpError: (state, action) => {
      state.otpError = action.payload;
    },
    verifyMobile: (state) => {
      state.isMobileVerified = true;
    },
    unVerifyMobile: (state) => {
      state.isMobileVerified = false;
    },
  },
  extraReducers,
});

function createExtraActions() {
  return {
    submitMobile: submitMobile(),
    submitOtp: submitOtp(),
  };
  function submitMobile() {
    return createAsyncThunk(
      `${name}/submitMobile`,
      async (_, { getState, dispatch, rejectWithValue }) => {
        const rootState = getState(); // has type YourRootState
        const { mobile, countryCode } = rootState.retailerMobile;
        dispatch(toggleShowOtp(true));
        try {
          const res = await MainServer.submitMobile({
            mobile: mobile,
            country_code: countryCode,
          });
          dispatch(
            NotificationActions.addNotification({
              type: "success",
              timeout: 2000,
              message: "OTP sent successfully!",
            })
          );
          return res;
        } catch (err) {
          // dispatch(
          //   NotificationActions.addNotification({
          //     type: "error",
          //     message: "Something went wrong. Please try again!",
          //   })
          // );
          return rejectWithValue(err);
        }
      }
    );
  }

  function submitOtp() {
    return createAsyncThunk(
      `${name}/submitOtp`,
      async (_, { getState, dispatch, rejectWithValue }) => {
        const rootState = getState(); // has type YourRootState
        const { otp, request_id } = rootState.retailerMobile;
        try {
          const res = await MainServer.verifyMobileOtp({
            otp,
            request_id,
          });
          sessionStorage.setItem("accessToken", res.headers.authorization);
          dispatch(toggleShowOtp(false));
          dispatch(
            NotificationActions.addNotification({
              type: "success",
              timeout: 2000,
              message: "Mobile validated successfully!",
            })
          );
          dispatch(retailerActions.updateSellerData(res.data));
          return res;
        } catch (err) {
          // dispatch(
          //   NotificationActions.addNotification({
          //     type: "error",
          //     message: "Something went wrong. Please try again!",
          //   })
          // );
          return rejectWithValue(err);
        }
      }
    );
  }
}

function createExtraReducers() {
  return {
    ...submitMobile(),
    ...submitOtp(),
  };

  function submitMobile() {
    var { pending, fulfilled, rejected } = extraActions.submitMobile;
    return {
      [pending]: (state) => {
        state.submittingMobile = true;
      },
      [fulfilled]: (state, action) => {
        state.request_id = action.payload.data.request_id;
        state.resendTimer = action.payload.data.resend_timer;
        state.submittingMobile = false;
      },
      [rejected]: (state, action) => {
        state.submittingMobile = false;
      },
    };
  }

  function submitOtp() {
    var { pending, fulfilled, rejected } = extraActions.submitOtp;
    return {
      [pending]: (state) => {
        state.submittingOtp = true;
      },
      [fulfilled]: (state, action) => {
        state.submittingOtp = false;
        state.isMobileVerified = true;
      },
      [rejected]: (state, action) => {
        state.submittingOtp = false;
        state.otpError = getErrorMsg(action.payload);
      },
    };
  }
}

export const retailerMobileActions = {
  ...retailerMobile.actions,
  ...extraActions,
};

//getters
export const getMobile = (state) => state.retailerMobile.mobile;
export const getError = (state) => state.retailerMobile.error;

export default retailerMobile.reducer;
