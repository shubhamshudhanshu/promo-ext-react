import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    showAdminOtp: false,
};

export const adminSlice = createSlice({
    name: "admin",
    initialState,
    reducers: {

        toggleShowAdminOtp: (state, action) => {
            state.showAdminOtp = action.payload;
        },

    },
});

// Action creators are generated for each case reducer function
export const {
    toggleShowAdminOtp
} = adminSlice.actions;

export default adminSlice.reducer
