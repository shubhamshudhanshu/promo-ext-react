import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import MainServer from "../../services/main_service";

const initialState = {
  pan: "",
  error: "",
  submittingPan: false,
  panDetails: {},
};

const name = "retailerPan";
const extraActions = createExtraActions();

export const retailerPan = createSlice({
  name: "retailerPan",
  initialState,
  reducers: {
    setPan: (state, action) => {
      state.pan = action.payload;
    },
    setPanError: (state, action) => {
      state.error = action.payload;
    },
  },
});

function createExtraActions() {
  return {
    submitPan: submitPan(),
  };
  function submitPan() {
    return createAsyncThunk(
      `${name}/submitPan`,
      async (_, { getState, rejectWithValue }) => {
        const rootState = getState(); // has type YourRootState
        const { pan } = rootState.retailerPan;
        try {
          const res = await MainServer.postPanNumber({
            panNumber: pan,
          });
          return res;
        } catch (err) {
          return rejectWithValue(err);
        }
      }
    );
  }
}

// function createExtraReducers() {
//   return {
//     ...submitPan(),
//   };

//   function submitPan() {
//     var { pending, fulfilled, rejected } = extraActions.submitPan;
//     return {
//       [pending]: (state) => {
//         state.submittingPan = true;
//       },
//       [fulfilled]: (state, action) => {
//         state.submittingPan = false;
//         state.panDetails = action.payload.data;
//       },
//       [rejected]: (state, action) => {
//         state.submittingPan = false;
//         state.error = getErrorMsg(action.payload);
//       },
//     };
//   }
// }

export const retailerPanActions = {
  ...retailerPan.actions,
  ...extraActions,
};

export default retailerPan.reducer;
