import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { AddressType } from "../../helper/constants";
import { getErrorMsg } from "../../helper/utils";
import MainServer from "../../services/main_service";
import { NotificationActions } from "./notification";
// create slice

const name = "retailer";
const initialState = {
  retailerData: {
    data: {
      id: 2,
      mobileNumber: null,
      countryCode: null,
      prmId: null,
      sellerName: null,
      storeName: null,
      status: null,
      tabletDevice: null,
      totalTurnover10Crore: null,
      tds194Q: null,
      storeImages: null,
      bankDetail: {
        bankDetailId: "",
        accountNumber: "",
        accountName: "",
        ifsc: "",
        bankName: "",
      },
      panDetail: {
        panDetailId: "",
        pan: "",
        panResponse: "",
        name: "",
        status: null,
        issueDate: null,
      },
      gstDetail: {
        gstDetailId: 90,
        gstin: "",
        gstResponse: "",
        sellerName: "",
        storeName: "",
      },
      aadharDetail: null,
      addresses: [
        {
          addressId: null,
          addressType: "",
          buildingName: "",
          streetName: null,
          floor: "",
          landmark: "",
          house: "",
          pincode: "",
          city: "",
          district: "",
          state: "",
          retailerId: null,
        },
      ],
    },
    error: {},
  },
  addressError: {
    buildingName: "",
    streetName: null,
    floor: "",
    landmark: "",
    house: "",
    pincode: "",
    city: "",
    district: "",
    state: "",
  },
  AdhaarVerified: false,
  request_id_adhaar: "",
  patchAddress: false,
  tdsUpdated: false,
  bankDetailsUpdated: false,
  isLoading: false,
  submittingAdhaar: false,
  verifyingAdhaar: false,
  typeOfAddressProof: "",
  addressProof: "",
  addressProofFileName: "",
  storePhotos: [],
  isAddressEdited: false,
  error: {},
  bankNameError: "",
  fetchingRetailerData: false,
  stepLoading: false,
  gstError: "",
  gstVerified: false,
  submittingGst: false,
  fetchingCurrentAddress: false,
};
const extraActions = createExtraActions();
const extraReducers = createExtraReducers();

const slice = createSlice({
  name,
  initialState,
  reducers: {
    updateSellerData: (state, action) => {
      state.retailerData.data = action.payload;
      if (action.payload.gstDetail && action.payload.gstDetail.gstDetailId) {
        state.gstVerified = true;
      }
    },
    setCompleteAddress: (state, action) => {
      state.retailerData.data.addresses = [action.payload];
    },
    setSellerFieldValue: (state, action) => {
      const { type, value } = action.payload;
      state.retailerData.data[type] = value;
    },
    setAddressFieldError: (state, action) => {
      const { fieldName, text } = action.payload;
      const addressError = state.addressError;
      addressError[fieldName] = text;
      state.addressError = addressError;
    },
    onChangeTypeOfAddressProof: (state, action) => {
      state.typeOfAddressProof = action.payload;
      state.addressProof = "";
      state.addressProofFileName = "";
    },
    onUploadAddressProof: (state, action) => {
      state.addressProof = action.payload;
      state.addressProofFileName = action.payload.name;
    },
    onRemoveAddressProof: (state, action) => {
      state.addressProof = action.payload;
      state.addressProofFileName = "";
    },
    onUploadStorePhotos: (state, action) => {
      const updateArray = action.payload.map((val) => {
        return URL.createObjectURL(val);
      });
      state.storePhotos = [...state.storePhotos, ...updateArray];
    },
    deletePhotos: (state, action) => {
      const newArr = [...state.storePhotos];
      newArr.splice(action.payload, 1);
      state.storePhotos = newArr;
    },
    setAddressFieldValue: (state, action) => {
      const { fieldName, value } = action.payload;
      const addresses = state.retailerData.data.addresses.map((p) => {
        if (p.addressType === AddressType.NOT_VERIFIED) {
          p[fieldName] = value;
          return p;
        }
        return p;
      });
      const newAddress = {};
      if (addresses.length === 0) {
        newAddress["addressType"] = AddressType.NOT_VERIFIED;
        newAddress[fieldName] = value;
        state.retailerData.data.addresses = [newAddress];
      } else {
        state.retailerData.data.addresses = addresses;
      }
      state.isAddressEdited = true;
    },
    setIsAddressEdited: (state, action) => {
      state.isAddressEdited = action.payload;
    },
    setBankNameError: (state, action) => {
      state.bankNameError = action.payload;
    },
    onChangeGst: (state, action) => {
      state.retailerData.data.gstDetail.gstin = action.payload;
    },
    setGstError: (state, action) => {
      state.gstError = action.payload;
    },
    setGstVerified: (state) => {
      state.gstVerified = true;
    },
    setFetchingCurrentAddress: (state, action) => {
      state.fetchingCurrentAddress = action.payload;
    },
  },
  extraReducers,
});

// exports

export const retailerActions = { ...slice.actions, ...extraActions };
export const retailerReducer = slice.reducer;

function createExtraActions() {
  return {
    fetchRetailerData: fetchRetailerData(),
    updateSellerDetails: updateSellerDetails(),
    postPanNumber: postPanNumber(),
    verifyAdhaarManualy: verifyAdhaarManualy(),
    sendAdhaarOtp: sendAdhaarOtp(),
    verifyAdhaarOtp: verifyAdhaarOtp(),
    postBankDetails: postBankDetails(),
    verifyBankDetails: verifyBankDetails(),
    submitGst: submitGst(),
  };

  function fetchRetailerData() {
    return createAsyncThunk(
      `${name}/fetchData`,
      async (id, { dispatch, rejectWithValue }) => {
        try {
          const res = await MainServer.fetchRetailerData();
          dispatch(retailerActions.updateSellerData(res.data));
          return res;
        } catch (err) {
          return rejectWithValue(err);
        }
      }
    );
  }

  function submitGst() {
    return createAsyncThunk(
      `${name}/submitGst`,
      async (_, { getState, dispatch, rejectWithValue }) => {
        const rootState = getState(); // has type YourRootState
        const { gst } = rootState.retailerMobile;
        try {
          const res = await MainServer.submitGst({
            gst,
          });
          dispatch(retailerActions.updateSellerData(res.data));
          return res;
        } catch (err) {
          return rejectWithValue(err);
        }
      }
    );
  }

  function updateSellerDetails() {
    return createAsyncThunk(
      `${name}/updateSellerDetails`,
      async (body, { rejectWithValue }) => {
        try {
          const res = await MainServer.updateSellerDetails({ body });
          return res;
        } catch (err) {
          return rejectWithValue(err);
        }
      }
    );
  }

  function postPanNumber() {
    return createAsyncThunk(
      `${name}/postPanNumber`,
      async (panNumber, { dispatch, rejectWithValue }) => {
        try {
          const res = await MainServer.postPanNumber({
            panNumber,
          });
          return res;
        } catch (err) {
          // dispatch(
          //   NotificationActions.addNotification({
          //     type: "error",
          //     timeout: 2000,
          //     message: "Something went wrong!",
          //   })
          // );
          return rejectWithValue(err);
        }
      }
    );
  }

  function verifyAdhaarManualy() {
    return createAsyncThunk(
      `${name}/verifyAdhaarManualy`,
      async (body, { rejectWithValue }) => {
        // debugger
        try {
          const res = await MainServer.verifyAdhaarManualy({ body });
          return res;
        } catch (err) {
          return rejectWithValue(err);
        }
      }
    );
  }
  function sendAdhaarOtp() {
    return createAsyncThunk(
      `${name}/sendAdhaarOtp`,
      async (body, { dispatch, rejectWithValue }) => {
        try {
          const res = await MainServer.sendAdhaarOtp({ body });
          dispatch(
            NotificationActions.addNotification({
              type: "success",
              timeout: 2000,
              message: "Otp sent successfully!",
            })
          );
          return res;
        } catch (err) {
          // dispatch(
          //   NotificationActions.addNotification({
          //     type: "error",
          //     timeout: 2000,
          //     message: "Something went wrong!",
          //   })
          // );
          return rejectWithValue(err);
        }
      }
    );
  }

  function verifyAdhaarOtp() {
    return createAsyncThunk(
      `${name}/verifyAdhaarOtp`,
      async (body, { getState, dispatch, rejectWithValue }) => {
        const rootState = getState(); // has type YourRootState
        const { request_id_adhaar } = rootState.retailerData;
        try {
          const res = await MainServer.verifyAdhaarOtp({
            body: {
              ...body,
              requestId: request_id_adhaar,
            },
          });
          return res;
        } catch (err) {
          // dispatch(
          //   retailerMobileActions.setOtpError(
          //     "Something went wrong in verifying aadhaar details!"
          //   )
          // );
          return rejectWithValue(err);
        }
      }
    );
  }

  function postBankDetails() {
    return createAsyncThunk(
      `${name}/postBankDetails`,
      async (body, { rejectWithValue }) => {
        try {
          const res = await MainServer.postBankDetails({ body });
          return res;
        } catch (err) {
          return rejectWithValue(err);
        }
      }
    );
  }

  function verifyBankDetails() {
    return createAsyncThunk(
      `${name}/verifyBankDetails`,
      async (body, { dispatch, rejectWithValue }) => {
        try {
          const res = await MainServer.verifyBankDetails({
            body,
          });
          return res;
        } catch (err) {
          dispatch(
            NotificationActions.addNotification({
              type: "error",
              timeout: 6000,
              message: "Bank Verification failed!",
            })
          );
          return rejectWithValue(err);
        }
      }
    );
  }
}

function createExtraReducers() {
  return {
    ...fetchRetailerData(),
    ...updateSellerDetails(),
    ...verifyAdhaarManualy(),
    ...sendAdhaarOtp(),
    ...verifyAdhaarOtp(),
    ...postPanNumber(),
    ...postBankDetails(),
    ...submitGst(),
  };

  function fetchRetailerData() {
    var { pending, fulfilled, rejected } = extraActions.fetchRetailerData;
    return {
      [pending]: (state) => {
        state.retailerData.error = null;
        state.isLoading = true;
        state.fetchingRetailerData = true;
      },
      [fulfilled]: (state, action) => {
        state.retailerData = {
          data: action.payload?.data,
          error: null,
        };
        state.isLoading = false;
        state.fetchingRetailerData = false;
      },
      [rejected]: (state, action) => {
        state.retailerData.error = action.error;
        state.isLoading = false;
        state.fetchingRetailerData = false;
      },
    };
  }

  function submitGst() {
    var { pending, fulfilled, rejected } = extraActions.submitGst;
    return {
      [pending]: (state) => {
        state.submittingGst = true;
      },
      [fulfilled]: (state, action) => {
        state.retailerData.data = action.payload.data;
        state.submittingGst = false;
        state.gstVerified =
          action.payload.data.gstDetail &&
          action.payload.data.gstDetail.gstDetailId
            ? true
            : false;
        state.gstError = "";
      },
      [rejected]: (state, action) => {
        state.submittingGst = false;
        state.gstError = getErrorMsg(action.payload);
      },
    };
  }

  function updateSellerDetails() {
    var { pending, fulfilled, rejected } = extraActions.updateSellerDetails;
    // debugger;
    return {
      [pending]: (state) => {
        state.stepLoading = true;
      },
      [fulfilled]: (state, action) => {
        state.stepLoading = false;
        // updste retialer data here by getting updated response in api
      },
      [rejected]: (state, action) => {
        state.stepLoading = false;
      },
    };
  }
  function postPanNumber() {
    var { pending, fulfilled, rejected } = extraActions.postPanNumber;

    return {
      [pending]: (state) => {
        state.stepLoading = true;
      },
      [fulfilled]: (state, action) => {
        state.stepLoading = false;

        // updste retialer data here by getting updated response in api
      },
      [rejected]: (state, action) => {
        state.stepLoading = false;
      },
    };
  }
  function sendAdhaarOtp() {
    var { pending, fulfilled, rejected } = extraActions.sendAdhaarOtp;
    return {
      [pending]: (state) => {
        state.isLoading = true;
        state.submittingAdhaar = true;
      },
      [fulfilled]: (state, action) => {
        state.request_id_adhaar = action.payload.data.requestId;
        state.submittingAdhaar = false;
        state.isLoading = true;
      },
      [rejected]: (state, action) => {
        state.isLoading = true;
        state.submittingAdhaar = false;
      },
    };
  }
  function verifyAdhaarOtp() {
    var { pending, fulfilled, rejected } = extraActions.verifyAdhaarOtp;
    return {
      [pending]: (state) => {
        state.isLoading = true;
        state.verifyingAdhaar = true;
      },
      [fulfilled]: (state, action) => {
        state.isLoading = false;
        state.verifyingAdhaar = false;
        state.AdhaarVerified = true;
        // updste retialer data here by getting updated response in api
      },
      [rejected]: (state, action) => {
        state.isLoading = false;
        state.verifyingAdhaar = false;
      },
    };
  }
  function verifyAdhaarManualy() {
    var { pending, fulfilled, rejected } = extraActions.verifyAdhaarManualy;
    return {
      [pending]: (state) => {
        state.stepLoading = true;
      },
      [fulfilled]: (state, action) => {
        state.stepLoading = false;
      },
      [rejected]: (state, action) => {
        state.stepLoading = false;
      },
    };
  }
  function postBankDetails() {
    var { pending, fulfilled, rejected } = extraActions.postBankDetails;
    // debugger;
    return {
      [pending]: (state) => {
        state.stepLoading = true;
      },
      [fulfilled]: (state, action) => {
        state.stepLoading = false;
        // updste retialer data here by getting updated response in api
      },
      [rejected]: (state, action) => {
        state.stepLoading = false;
      },
    };
  }
  // function verifyBankDetails() {
  //   var { pending, fulfilled, rejected } = extraActions.verifyBankDetails;
  //   return {
  //     [pending]: (state) => {
  //       state.stepLoading = true;
  //     },
  //     [fulfilled]: (state, action) => {
  //       state.stepLoading = false;
  //     },
  //     [rejected]: (state, action) => {
  //       state.stepLoading = false;
  //     },
  //   };
  // }
}
