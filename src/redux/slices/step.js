import { createSlice } from "@reduxjs/toolkit";
const last_step = sessionStorage.getItem("form_step");

const initialState = {
  currentStep: last_step ? parseInt(last_step) : 0,
  enteredAdhaarNumber: "",
  showAdhaarVerifyPage: false,
  showOtp: false,
  showForm: false,
};

export const stepSlice = createSlice({
  name: "step",
  initialState,
  reducers: {
    goNext: (state) => {
      state.currentStep += 1;
    },
    goBack: (state) => {
      state.currentStep -= 1;
    },
    goToStep: (state, action) => {
      state.currentStep = action.payload;
    },
    toggleAdhaarVerifyPage: (state, action) => {
      state.showAdhaarVerifyPage = action.payload;
    },
    toggleShowOtp: (state, action) => {
      state.showOtp = action.payload;
    },
    updateAdhaarNumber: (state, action) => {
      state.enteredAdhaarNumber = action.payload;
    },
    toggleShowForm: (state, action) => {
      state.showForm = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  goNext,
  goBack,
  goToStep,
  toggleAdhaarVerifyPage,
  toggleShowOtp,
  updateAdhaarNumber,
  toggleShowForm,
} = stepSlice.actions;

export default stepSlice.reducer;
