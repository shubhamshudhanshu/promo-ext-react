import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  values: {},
  errors: {},
};

export const retailerAddress = createSlice({
  name: "retailerAddress",
  initialState,
  reducers: {
    setFieldError: (state, action) => {
      const { fieldName, text } = action.payload;
      state.errors[fieldName] = text;
    },
    setFieldValue: (state, action) => {
      const { fieldName, value } = action.payload;
      state.values[fieldName] = value;
    },
  },
});

export const { setFieldError, setFieldValue } = retailerAddress.actions;

export default retailerAddress.reducer;
