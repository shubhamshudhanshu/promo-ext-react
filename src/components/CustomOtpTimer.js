import React, { useState, useRef, useEffect } from "react";
import Link from "./CustomLink";

const CustomOtpTimer = ({ secs = 10, resend, resendBtnClass, timerClass }) => {
  const Ref = useRef(null);
  const [timer, setTimer] = useState("00:00");
  const [showResend, setShowResend] = useState(false);
  const getTimeRemaining = (e) => {
    const total = Date.parse(e) - Date.parse(new Date());
    const seconds = Math.floor((total / 1000) % 60);
    const minutes = Math.floor((total / 1000 / 60) % 60);
    return {
      total,
      minutes,
      seconds,
    };
  };

  const startTimer = (e) => {
    let { total, minutes, seconds } = getTimeRemaining(e);
    if (total >= 0) {
      setTimer(
        (minutes > 9 ? minutes : "0" + minutes) +
          ":" +
          (seconds > 9 ? seconds : "0" + seconds)
      );
    } else {
      setShowResend(true);
    }
  };

  const clearTimer = (e) => {
    setTimer("00:" + (secs > 9 ? secs : "0" + secs));
    if (Ref.current) clearInterval(Ref.current);
    const id = setInterval(() => {
      startTimer(e);
    }, 1000);
    Ref.current = id;
  };

  const getDeadTime = () => {
    let deadline = new Date();
    deadline.setSeconds(deadline.getSeconds() + secs);
    return deadline;
  };
  useEffect(() => {
    clearTimer(getDeadTime());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const onClickReset = () => {
    setShowResend(false);
    resend();
    clearTimer(getDeadTime());
  };

  return (
    <div>
      {showResend ? (
        <Link
          className={resendBtnClass}
          onClick={function noRefCheck() {
            onClickReset();
          }}
          textAppearance="body-xxs-bold"
          title="Resend"
        />
      ) : (
        <p className={timerClass}>{timer}</p>
      )}
    </div>
  );
};

export default CustomOtpTimer;
