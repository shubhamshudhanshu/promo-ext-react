import React from "react";
import PropTypes from "prop-types";
import { styled } from "@mui/material/styles";
import StepConnector, {
  stepConnectorClasses,
} from "@mui/material/StepConnector";

import CheckCircle from "../../assets/images/onboarding/check-circle.svg";
import { Icon } from "@mui/material";

const QontoConnector = styled(StepConnector)(({ theme }) => ({
  [`&.${stepConnectorClasses.alternativeLabel}`]: {
    top: 16,
    left: "calc(-50% + 20px)",
    right: "calc(50% + 20px)",
  },
  [`&.${stepConnectorClasses.active}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      borderColor: "#1ECCB0",
    },
  },
  [`&.${stepConnectorClasses.completed}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      borderColor: "#1ECCB0",
    },
  },
  [`& .${stepConnectorClasses.line}`]: {
    borderColor: "#eaeaf0",

    borderTopWidth: 1,
    // borderRadius: 1,

    borderRadius: "0px 80px 80px 0px;",
  },
}));

const QontoStepIconRoot = styled("div")(({ theme, ownerState }) => ({
  color: "black",
  display: "flex",
  width: "32",
  height: "32",
  fontFamily: "JioType",
  fontStyle: "normal",
  fontWeight: "700",
  fontSize: "16px",
  lineheight: "24px",
  "& .QontoStepIcon-completedIcon": {
    backgroundColor: "#ffffff",
    width: "32px",
    height: "32px",
    zIndex: 1,
    fontSize: 18,
  },
  "& .QontoStepIcon-circle": {
    backgroundColor: "#ffffff",
    width: "32px",
    height: "32px",
    borderRadius: "50%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    border: "1px solid #B5B5B5",
    "& .text-step": {
      color: "#B5B5B5",
    },
    ...(ownerState.active && {
      backgroundColor: "#1ECCB0",
      border: "none",
      "& .text-step": {
        color: "black !important",
      },
    }),
  },
}));

const QontoStepIcon = (props) => {
  const { active, completed, className, icon } = props;
  return (
    <QontoStepIconRoot ownerState={{ active, completed }} className={className}>
      {completed ? (
        // <CheckCircle className="QontoStepIcon-completedIcon" />
        <Icon className="QontoStepIcon-completedIcon">
          <img src={CheckCircle} height={32} width={32} alt="check-circle" />
        </Icon>
      ) : (
        // <Icon ic={IcArrowBack} className="QontoStepIcon-completedIcon" />
        <div className="QontoStepIcon-circle">
          <span className="text-step">{icon}</span>
        </div>
      )}
    </QontoStepIconRoot>
  );
};

QontoStepIcon.propTypes = {
  /**
   * Whether this step is active.
   * @default false
   */
  active: PropTypes.bool,
  className: PropTypes.string,

  /**
   * Mark the step as completed. Is passed to child components.
   * @default false
   */
  completed: PropTypes.bool,
};
export { QontoStepIcon, QontoConnector };
