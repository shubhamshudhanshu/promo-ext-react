
import React from "react";


import Box from '@mui/material/Box';
import { createStyles, makeStyles } from "@mui/styles";
import { CircularProgress } from "@mui/material";

const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            display: "flex",
            "& > * + *": {
                marginLeft: "16px"
            },
            position: "absolute",
            top: 0,
            width: "100%",
            height: "100%",
            justifyContent: "center",
            alignItems: "center"
        }
    })
);

export default function Loader() {
    const classes = useStyles();
    return (
        <Box className={classes.root}>
            <CircularProgress />
        </Box>
    );
}