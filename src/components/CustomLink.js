import * as React from "react";
import { styled } from "@mui/system";

const style = {
  "body-m": {
    fontWeight: 500,
    fontSize: "1.125rem",
    letterSpacing: "-1.2px",
    lineHeight: 1,
  },
  "heading-m": {
    fontWeight: 900,
    fontSize: "2.5rem",
    letterSpacing: "-1.2px",
    lineHeight: 1,
  },
  "body-xxs-bold": {
    fontWeight: 700,
    fontSize: "0.875rem",
    letterSpacing: "-0.07px",
    lineHeight: 1.4285714286,
  },
  "body-xxs": {
    fontWeight: 500,
    fontSize: "0.75rem",
    letterSpacing: "-0.06px",
    lineHeight: 1.3333333333,
  },
  "body-s-bold": {
    fontWeight: 700,
    fontSize: "1rem",
    letterSpacing: "-0.08px",
    lineHeight: 1.5,
  },
  "body-xs": {
    fontWeight: 900,
    fontSize: "0.875rem",
    letterSpacing: "-0.07px",
    lineHeight: 1.4285714286,
  },
};
const CustomeText = styled("a")({
  fontFamily: "JioType",
  fontStyle: "normal",
  textDecoration: "none",
  cursor: "pointer",
});

export default function Link(props) {
  const { color, textAppearance, newTab, title, ...rest } = props;
  return (
    <CustomeText
      id="standard-basic"
      target={newTab && "_blank"}
      style={{ color: `var(--color-primary-60)`, ...style[textAppearance] }}
      {...rest}
    >
      {title}
    </CustomeText>
  );
}
