import * as React from "react";
import Modal from "@mui/material/Modal";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "none",
  boxShadow: 24,
  padding: 24,
  maxHeight: 660,
  maxWidth: 450,
  borderRadius: 16,
  overflow: "auto",
};

export default function TransitionsModal({
  handleClose,
  open,
  children,
  ...rest
}) {
  return (
    <div>
      <Modal open={open} onClose={handleClose}>
        {}
        <div className="modal-content" style={style}>
          {children}
        </div>
      </Modal>
    </div>
  );
}
