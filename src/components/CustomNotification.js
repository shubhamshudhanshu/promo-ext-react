import { Snackbar, Alert } from "@mui/material";
import React from "react";
import { useSelector } from "react-redux";
import { useNotification } from "./../helper/hooks/useNotification";

export const Notification = () => {
  const snackbarDetails = useSelector((state) => {
    return state.notification;
  });
  const { clearNotification } = useNotification();

  const handleClose = (_, reason) => {
    reason !== "clickaway" && clearNotification();
  };

  return (
    <Snackbar
      open={snackbarDetails.open}
      autoHideDuration={snackbarDetails.timeout}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      onClose={handleClose}
    >
      <Alert
        onClose={handleClose}
        severity={snackbarDetails.type}
        sx={{ width: "100%" }}
      >
        {snackbarDetails.message}
      </Alert>
    </Snackbar>
  );
};
