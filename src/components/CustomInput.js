import * as React from "react";
import { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import { TextField } from "@mui/material";
import { styled } from "@mui/system";
import ErrorIcon from "../assets/images/onboarding/error-cross.svg";
import InputAdornment from "@mui/material/InputAdornment";
import InputMask from "react-input-mask";

const CustomeTextField = styled(TextField)({
  fontFamily: "JioType",
  fontStyle: "normal",
  fontWeight: "500",
  lineHeight: "24px",

  letterSpacing: "-0.005em",

  color: " #141414",
  root: {
    color: "Red",
  },

  label: {
    color: "rgba(0, 0, 0, 0.65) !important",
    fontFamily: "JioType",
    fontWeight: "500",
  },
  "& input": {
    color: "#141414",
    fontWeight: "500",
    fontFamily: "JioType",
  },

  "& input:focus": {
    borderColor: `#000093`,
  },
  "& input:after": {
    border: "1px solid #000093",
  },
  "& input :before": {
    border: "1px solid #000093",
  },

  "& MuiInput:hover": {
    borderColor: "#000093",
  },
  "& label.Mui-focused": {
    color: "green",
  },
  "& .MuiInput-underline:before": {
    borderWidth: "2px",
    borderBottomColor: "rgba(0, 0, 0, 0.65)",
  },
  "& .MuiInputBase-readOnly": {
    color: "var(--color-primary-grey-80)",
    borderBottomColor: "var(--color-primary-grey-80)",
  },
  "& .MuiFormLabel-asterisk": {
    color: "red",
  },
});

export default function CustomInputField(props) {
  const [val, setVal] = useState("");
  const {
    value,
    handleChange = () => {},
    error,
    pattern,
    fullWidth = true,
    setError = () => {},
    prefix,
    maxLength,
    onChange,
    readOnly,
    suffix,
    length,
    typeConfig,
    mask,
    validation,
    verified,
    show,
    uppercase,
    strictCheck = false,
    ...rest
  } = props;

  useEffect(() => {
    setVal(value);
  }, [value]);

  const onChangeInput = (e) => {
    const val = e.target.value;
    let err = "";
    // give either mask or pattern
    if (mask && val.length < mask.length) {
      err = "Invalid input!";
    } else if (val && pattern && !pattern?.test(val)) {
      err = "Invalid input!";
      if (strictCheck) {
        return;
      }
    }
    setError(err);
    setVal(val);
    handleChange(e);
  };
  return (
    <Box sx={{ margin: "12px 0px", width: fullWidth && "100%" }}>
      <InputMask
        fullWidth={fullWidth}
        mask={mask}
        maskChar={""}
        value={val}
        formatChars={{
          "#": "[0-9]",
          $: "[A-Za-z]",
          "*": "[A-Za-z0-9]",
        }}
        onChange={onChangeInput}
      >
        {(inputProps) => {
          return (
            <CustomeTextField
              id="standard-basic"
              variant="standard"
              margin="normal"
              value={val}
              onChange={onChangeInput}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">{prefix}</InputAdornment>
                ),
                endAdornment: (
                  <InputAdornment position="start">{suffix}</InputAdornment>
                ),
                inputProps: {
                  maxLength: maxLength,
                  style: { textTransform: uppercase ? "uppercase" : "none" },
                },
                readOnly: readOnly,
              }}
              {...inputProps}
              {...rest}
            />
          );
        }}
      </InputMask>
      {error && (
        <span
          style={{
            display: "flex",
            gap: "8px",
            alignItems: "center",
            fontSize: "14px",
          }}
        >
          <img
            src={ErrorIcon}
            alt=""
            style={{
              height: "16px",
              width: "16px",
            }}
          />
          <span style={{ wordBreak: "break-all" }}> {error}</span>
        </span>
      )}
    </Box>
  );
}
