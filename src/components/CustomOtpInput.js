import * as React from "react";
import { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import ErrorIcon from "../assets/images/onboarding/error-cross.svg";
import OtpInput from "react-otp-input";

export default function CustomOtpInputField(props) {
  const [val, setVal] = useState("");
  const {
    value,
    handleChange = () => {},
    pattern,
    error,
    setError = () => {},
    fullWidth,
    length,
  } = props;

  useEffect(() => {
    setVal(value);
  }, [value]);

  const onChangeInput = (val) => {
    let err = "";
    if (val && pattern && !pattern?.test(val)) {
      err = "Invalid input!";
    }
    setError(err);
    setVal(val);
    handleChange(val);
  };

  return (
    <Box sx={{ margin: "12px 0px", width: fullWidth && "100%" }}>
      <OtpInput
        value={val}
        onChange={onChangeInput}
        inputStyle={{
          width: "100%",
          maxWidth: "40px",
          margin: "0px 10px",
          marginBottom: "15px",
          outline: "none",
          border: "none",
          borderBottom: "2px solid grey",
          background: "none",
        }}
        isInputNum
        numInputs={length || 4}
        separator={<span></span>}
        shouldAutoFocus
      />
      {error && (
        <span
          style={{
            display: "flex",
            gap: "8px",
            alignItems: "center",
            fontSize: "14px",
          }}
        >
          <img
            src={ErrorIcon}
            alt=""
            style={{
              height: "16px",
              width: "16px",
            }}
          />
          <span style={{ wordBreak: "break-all" }}> {error}</span>
        </span>
      )}
    </Box>
  );
}
