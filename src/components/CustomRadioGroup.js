import * as React from "react";
import { styled } from "@mui/material/styles";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";

const BpIcon = styled("span")(({ theme }) => ({
  borderRadius: "50%",
  width: 24,
  height: 24,
  backgroundColor: "white",
  backgroundImage: "white",
  border: "1px solid #000000a6",

  "input:disabled ~ &": {
    boxShadow: "none",
    background: "rgba(206,217,224,.5)",
  },
}));

const BpCheckedIcon = styled(BpIcon)({
  backgroundColor: "#3535f3",
  backgroundImage:
    "linear-gradient(90deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
  "&:before": {
    display: "block",
    width: 24,
    height: 24,

    backgroundImage: "radial-gradient(#fff,#fff 37%,transparent 1%)",
    marginTop: "-1px",
    marginLeft: "-1px",
    content: '""',
  },
});

// Inspired by blueprintjs
function BpRadio(props) {
  return (
    <Radio
      sx={{
        "&:hover": {
          bgcolor: "transparent",
        },
      }}
      disableRipple
      color="default"
      checkedIcon={<BpCheckedIcon />}
      icon={<BpIcon />}
      {...props}
    />
  );
}

export default function CustomizedRadios({
  handleChange,
  options,
  label,
  ...rest
}) {
  return (
    <FormControl>
      {label && <FormLabel id="demo-customized-radios">{label}</FormLabel>}
      <RadioGroup name="customized-radios" {...rest} onChange={handleChange}>
        {options.map((ele, i) => {
          return (
            <FormControlLabel
              key={i}
              value={ele.value}
              control={<BpRadio />}
              label={ele.label}
              disabled={ele.disabled || false}
            />
          );
        })}
      </RadioGroup>
    </FormControl>
  );
}
