import axios from "axios";
import { NotificationActions } from "../redux/slices/notification";
import { store } from "../redux/store";

const client = axios.create({
  responseType: "text/plain",
  withCredentials: false,
  headers: {},
  timeout: 50000,
});

const isDev = process.env.NODE_ENV === "development";

const request = function (
  { method, url, headers = {}, data = {} },
  showErrorToast = true
) {
  const token = sessionStorage.getItem("accessToken");
  if (token) {
    headers.Authorization = `Bearer ${token}`;
  }
  const onSuccess = function (response) {
    if (response.status === 200) {
      return response;
    }

    return response;
  };

  const onError = function (error) {
    if (error.response.status === 403) {
      sessionStorage.removeItem("accessToken");
      if (isDev) {
        window.location = "http://localhost:3000/onboarding/home";
      } else {
        window.location =
          "https://jio-clickstream-product-suggestion.extensions.jiox0.de/onboarding/home";
      }
    }
    const message =
      error?.response?.data?.message?.[0] || "Something Went Wrong";
    if (showErrorToast) {
      store.dispatch(
        NotificationActions.addNotification({
          type: "error",
          timeout: 2000,
          message,
        })
      );
    }
    return Promise.reject(message);
  };

  // set payload

  const payload = {
    method,
    url,
    headers: {
      ...headers,
    },
    data: data,
  };
  return client(payload).then(onSuccess).catch(onError);
};

export default request;
