import {
  getAllPromoDetailsData,
  getPromoDetailById,
  submitFormResponse,
} from "./../dummyData";
import axios from "axios";
import URLS from "./endpoint.service";
import request from "./request";
import { HTTP_METHOD } from "../helper/constants";

const isDev = process.env.NODE_ENV === "development";

axios.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    return Promise.reject(error);
  }
);

const dummyPromise = (data) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({ data });
    }, 2300);
  });
};
const MainServer = {
  getAllPromos(page = 0, size = 5, sort = "position,asc") {
    const options = {
      params: {
        page,
        size,
        sort,
      },
    };
    if (isDev) {
      return dummyPromise(getAllPromoDetailsData);
    }
    return axios.get(URLS.GET_ALL_PROMOS(page, size, sort), options);
  },
  getPromoDetailByUrl(url) {
    if (isDev) {
      return dummyPromise(getPromoDetailById(url));
    }
    return axios.get(URLS.GET_PROMO_DETAIL_BY_URL(url));
  },

  submitMobile(payload) {
    return axios.post(URLS.SUBMIT_MOBILE(), payload);
  },
  verifyMobileOtp(payload) {
    return request({
      method: HTTP_METHOD.POST,
      url: URLS.VERIFY_MOBILE_OTP(),
      data: payload,
    });
  },

  submitGst(params) {
    return request({
      method: HTTP_METHOD.POST,
      url: URLS.SUBMIT_GST({ ...params }),
      data: {},
    });
  },
  fetchRetailerData(param) {
    return request({
      method: HTTP_METHOD.GET,
      url: URLS.FETCH_RETAILER_DATA({ ...param }),
    });
  },
  updateSellerDetails(payload) {
    const { body = {} } = payload;
    return request({
      method: HTTP_METHOD.POST,
      url: URLS.UPDATE_SELLER_DETAILS(),
      data: body,
    });
  },
  postPanNumber(payload) {
    const { panNumber } = payload;
    const data = { pan: panNumber };
    return request({
      method: HTTP_METHOD.POST,
      url: URLS.POST_PAN_DETAILS(),
      data,
    });
  },
  verifyAdhaarManualy(payload) {
    const { body = {} } = payload;
    return request({
      method: HTTP_METHOD.POST,
      url: URLS.VERIFY_ADHAAR_MANUALLY(),
      data: body,
    });
  },
  sendAdhaarOtp(payload) {
    const { body = {} } = payload;
    return request({
      method: HTTP_METHOD.POST,
      url: URLS.SEND_ADHAAR_OTP(),
      data: body,
    });
  },
  verifyAdhaarOtp(payload) {
    const { body = {} } = payload;
    return request({
      method: HTTP_METHOD.POST,
      url: URLS.VERIFY_ADHAAR_OTP(),
      data: body,
    });
  },
  postBankDetails(payload) {
    const { body = {} } = payload;
    return request({
      method: HTTP_METHOD.POST,
      url: URLS.POST_BANK_DETAILS(),
      data: body,
    });
  },
  verifyBankDetails(payload) {
    const { body = {} } = payload;
    return request({
      method: HTTP_METHOD.POST,
      url: URLS.VERIFY_BANK_DETAILS(),
      data: body,
    });
  },
  submitForm(payload) {
    if (isDev) {
      return dummyPromise(submitFormResponse);
    }
    return axios.post(URLS.SUBMIT_FORM(), payload);
  },
};
export default MainServer;
