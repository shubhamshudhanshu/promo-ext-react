import urlJoin from "url-join";
import root from "window-or-global";
let envVars = root.env || {};
envVars.URL = "https://jio-clickstream-product-suggestion.extensions.jiox0.de";

// envVars.URL = "https://86a1-116-50-84-116.in.ngrok.io";
if (
  root &&
  root.process &&
  root.process.env &&
  root.process.NODE_ENV === "test"
) {
  envVars = root.process.env;
}

const Endpoints = {
  GET_ALL_PROMOS() {
    return urlJoin(envVars.URL, `/api/promotions`);
  },
  GET_PROMO_DETAIL_BY_URL(url) {
    return urlJoin(envVars.URL, `/api/promotions/url/${url}/full`);
  },
  SUBMIT_FORM() {
    return urlJoin(envVars.URL, `/api/form-submissions-full`);
  },
  SUBMIT_MOBILE() {
    return urlJoin(envVars.URL, `/otp/api/v1/send-otp`);
  },
  VERIFY_MOBILE_OTP() {
    return urlJoin(envVars.URL, `/otp/api/v1/verify-otp`);
  },
  SUBMIT_GST({ gst }) {
    return urlJoin(envVars.URL, `/api/retailer-onboarding/gst/${gst}`);
  },
  FETCH_RETAILER_DATA() {
    return urlJoin(envVars.URL, `/api/retailer-onboarding`);
  },
  PATCH_SELLER_DATA() {
    return urlJoin(envVars.URL, `/api/retailer-onboarding/address/update`);
  },
  UPDATE_TDS() {
    return urlJoin(envVars.URL, `/api/retailer-onboarding/consent`);
  },
  UPDATE_SELLER_DETAILS() {
    return urlJoin(envVars.URL, `/api/retailer-onboarding/seller-detail`);
  },
  POST_PAN_DETAILS() {
    return urlJoin(envVars.URL, `/api/retailer-onboarding/pan`);
  },
  VERIFY_ADHAAR_MANUALLY() {
    return urlJoin(envVars.URL, `/api/retailer-onboarding/aadhar/via-proof`);
  },
  SEND_ADHAAR_OTP() {
    return urlJoin(envVars.URL, `/api/retailer-onboarding/aadhar/send-otp`);
  },
  VERIFY_ADHAAR_OTP() {
    return urlJoin(envVars.URL, `/api/retailer-onboarding/aadhar/verify-otp`);
  },
  POST_BANK_DETAILS() {
    return urlJoin(
      envVars.URL,
      `/api/retailer-onboarding/bank-detail/create-update`
    );
  },
  VERIFY_BANK_DETAILS() {
    return urlJoin(envVars.URL, `/api/retailer-onboarding/bank-detail/verify`);
  },
};
export default Endpoints;
