export const isValidLocationId = (text) => {
    text = text.trim();
    const textDelimiter = text.split(' ');
    return textDelimiter.map(x => x.trim()).join('');
}