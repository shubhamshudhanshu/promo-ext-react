const marker = `mark`;
/**
 * Add the marker defined on before and after of the text
 * @param {string} highLightedText
 * @returns
 */
const highLighter = (highLightedText) =>
  `<${marker}>${highLightedText}</${marker}>`;

/**
 * cleanse the string, wer are basiccaly trimming for whitespaces and making it uppercase for the standard comparison
 * @param {string} str
 * @returns
 */
const cleanUpString = (str) => str.trim().toUpperCase();

/**
 * adds this custom highligh fnction to any String type
 * @param {string} searchKey
 * @returns
 */
String.prototype.highLightText = function (searchKey) {
  let text = this;
  if (!text || !searchKey) {
    return text;
  }

  text = text.trim();
  searchKey = searchKey.trim();
  let start = 0,
    end = 0,
    searchIndex = 0;
  const textLength = text.length;
  const searchKeyLength = searchKey.length;
  const wordMap = new Set();

  if (searchKeyLength === 0 || textLength === 0) {
    return text;
  }

  while (start < textLength) {
    end = start + searchKeyLength - 1;
    // check if current iterating character is equal with the first letter in the searchKey,
    // if yes, then check again if the substring from the current iteration to the length of search key length is same as search key
    // if yes again, then go ahead and add this string as matched one to the unique set
    // else just hop on to next letter in the text
    if (
      cleanUpString(text[start]) === cleanUpString(searchKey[searchIndex]) &&
      cleanUpString(text.slice(start, end + 1)) === cleanUpString(searchKey)
    ) {
      wordMap.add(text.slice(start, end + 1));
    }
    start++;
  }

  // after getting unique pairs of matched words from the text replace all those with the highlighter
  wordMap.forEach((word) => {
    text = text.replaceAll(word, highLighter(word));
  });

  return text;
};

/**
 * Driver function which is contained in the String util itself
 * @param {string} text
 * @param {string} searchKey
 * @returns
 */
const highLightText = (text, searchKey) => {
  return text.highLightText(searchKey);
};
export { highLightText };
