import { eraseCookie, getCookie, setCookie } from "./utils";

const isDev = process.env.NODE_ENV === "development";

const currentSession = getCookie("ext_session_1");
const lastSession = getCookie("ext_session_last");
const formSessions = JSON.parse(getCookie("form-sessions"));
const setFormSession = (formKey) => {
  const formSessionDetails = {
    ...formSessions,
    [formKey]: currentSession,
  };
  setCookie("form-sessions", formSessionDetails);
};
const checkFromSession = (formKey) => {
  if (!formSessions) return false;
  if (formSessions && !formSessions.hasOwnProperty(formKey)) {
    return false;
  } else {
    if (formSessions[formKey] !== currentSession) {
      return false;
    } else return true;
  }
};
const updateLastSession = () => setCookie("ext_session_last", currentSession);
const eraseFormSessions = () => eraseCookie("form-sessions");
const onAppLoad = () => {
  if (currentSession !== lastSession) {
    eraseFormSessions();
    updateLastSession();
  }
};

export default {
  isDev,
  currentSession,
  lastSession,
  updateLastSession,
  formSessions,
  setFormSession,
  checkFromSession,
  eraseFormSessions,
  onAppLoad,
};
