import moment from "moment";
export const dateRangeShortcuts = [
  {
    text: "Last Week",
    start: moment().subtract(7, "days"),
    end: new Date(),
  },
  {
    text: "Last Two Weeks",
    start: moment().subtract(14, "days"),
    end: new Date(),
  },
  {
    text: "Last Month",
    start: moment().subtract(1, "months"),
    end: new Date(),
  },
];
