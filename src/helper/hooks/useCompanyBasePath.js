import { useParams } from "react-router-dom";
const useCompanyBasePath = () => {
  const params = useParams();
  const company_id = params.company_id;
  if (!company_id) return "";
  else return `/company/${company_id}`;
};

export default useCompanyBasePath;
