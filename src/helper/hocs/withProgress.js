/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import { retailerActions } from "../../redux/slices/retailer";

const routes = {
  "/enter-mobile": {
    step: 1,
    route: "/enter-mobile",
    authRequired: false,
    status: "inProgress",
  },
  "/enter-otp?otptype=mobile": {
    step: 2,
    route: "/enter-otp?type=mobile",
    authRequired: false,
    status: "inProgress",
  },
  "/enter-gst": {
    step: 3,
    route: "/enter-gst",
    authRequired: false,
    status: "inProgress",
  },
  "/form?step=1": {
    step: 4,
    route: "/form?step=1",
    authRequired: false,
    status: "inProgress",
  },
  "/form?step=2": {
    step: 5,
    route: "/form?step=2",
    authRequired: false,
    status: "inProgress",
  },
  "/form?step=3": {
    step: 6,
    route: "/form?step=3",
    authRequired: false,
    status: "inProgress",
  },
};

function withProgress(WrappedComponent) {
  return ({ ...props }) => {
    const dispatch = useDispatch();
    let navigate = useNavigate();
    let location = useLocation();

    // const stepState = useSelector((state) => state.stepSlice);
    const state = useSelector((state) => state.retailerData.retailerData);

    useEffect(() => {
      const accessToken = sessionStorage.getItem("accessToken");
      if (accessToken) dispatch(retailerActions.fetchRetailerData());
    }, []);

    useEffect(() => {
      const status = state.data.status;
      console.log(location.pathname);

      if (status === "FOR_APPROVAL") {
        navigate("/onboarding/success");
      }
    }, [state.data]);

    return <WrappedComponent {...props} />;
  };
}
export default withProgress;
