import React from "react";
import "../App.css";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import useCompanyBasePath from "../helper/hooks/useCompanyBasePath";

function Home() {
  let navigate = useNavigate();
  const companyBasePath = useCompanyBasePath();
  useEffect(() => {
    navigate(`${companyBasePath}/promos`);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <div>Redirecting...</div>;
}

export default Home;
