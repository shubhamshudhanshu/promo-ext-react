import React from "react";
import "../App.css";
import { useState, useEffect } from "react";
import MainService from "./../services/main_service";
function Promos() {
  const [, setShowform] = useState(false);
  const [promos, setPormos] = useState([]);
  const [fetching, setFetching] = useState(false);

  useEffect(() => {
    getAllPromoDetails();
  }, []);

  const getAllPromoDetails = () => {
    setFetching(true);
    MainService.getAllPromos()
      .then(({ data }) => {
        setPormos(data);
        setFetching(false);
      })
      .catch((err) => {
        setFetching(false);
      });
  };

  return (
    <div>
      {fetching ? (
        <p>Fetching data...</p>
      ) : (
        <div
          id="carouselExampleIndicators"
          className="carousel slide"
          data-bs-ride="true"
        >
          <div className="carousel-indicators">
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to="0"
              className="active"
              aria-current="true"
              aria-label="Slide 1"
            ></button>
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to="1"
              aria-label="Slide 2"
            ></button>
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to="2"
              aria-label="Slide 3"
            ></button>
          </div>
          <div className="carousel-inner">
            {promos.map((promo, index) => (
              <div key={index} className="carousel-item active" id="car">
                <img
                  src={promo.desktopImageUrl}
                  className="d-block w-80 "
                  marginLeft="200px"
                  alt="..."
                  onClick={() => setShowform(true)}
                />
                <br />
                <div>
                  <h3>{promo.title}</h3>
                  <br />
                  {promo.description}
                </div>
              </div>
            ))}
          </div>
          <button
            className="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide="prev"
          >
            <span
              className="carousel-control-prev-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Previous</span>
          </button>
          <button
            className="carousel-control-next"
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide="next"
          >
            <span
              className="carousel-control-next-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>
      )}
      {/* <Form /> */}
    </div>
  );
}

export default Promos;
