import React from "react";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import Header from "../header/header";
import "./terms.scss";

const TermsAndCondtion = () => {
  return (
    <>
      <Header />

      <div className="terms">
        <Container maxWidth="xl">
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <h2 className="termsTitle">
                TERMS AND CONDITIONS FOR ENGAGEMENT OF JIOPRIME PARTNER
              </h2>
              <p className="subTermText">
                I am hereby appointed as a JioPrime Partner of RELIANCE RETAIL
                LTD (“RRL”), subject to below given terms and conditions,
                various annexure, amendment or addendum attached or communicated
                separately from time to time by RRL. I hereby agree that I have
                reviewed and understood these terms and conditions and agree to
                carry out the Activities, as defined hereinafter, as JioPrime
                Partner to RRL on the terms and conditions of the Agreement by
                clicking on “I Agree” button at the end of these terms and
                conditions. I agree and understand that my clicking on “I Agree”
                button and carrying out Activities mentioned in these terms and
                conditions shall be treated as binding agreement between the
                Parties and I shall be bound by the same.
              </p>
            </Grid>
            <Grid item xs={12}>
              <h3 className="agreeTitle">I agree as under-</h3>
              <div id="using-ril" class="section-title">
                <span>1.</span>
                <strong> DEFINITIONS</strong>
              </div>
              <div class="section-content">
                <span>1.1</span>
                <p>
                  “Affiliates” shall mean any person, directly or indirectly
                  controlling, controlled by or under direct, indirect or common
                  control with, such person. For the purposes of this
                  definition, “control” (including any correlative meanings,
                  like terms “controlled by” and “under common control with”)
                  means the possession, directly or indirectly, of the power to
                  direct or cause the direction of the management or policies of
                  such person, whether through the ownership of voting
                  securities, by agreement with respect to the voting of
                  securities, by other agreement conferring control over
                  management or policy decisions, by virtue of the power to
                  control the composition of the board of directors or managers,
                  or otherwise. The terms “controlling” and “controlled” shall
                  have correlative meanings
                </p>
              </div>
              <div class="section-content">
                <span>1.2</span>
                <p>
                  “Agreement” shall mean these terms and conditions of
                  engagement of JioPrime Partner accepted by me/us/JioPrime
                  Partner by clicking on “I Agree” button at the end of these
                  terms and conditions together with its schedules, Annexures,
                  appendices etc. as may be amended or modified in writing or
                  through emails or as communicated by RRL from time to time by
                  accepting the same online
                </p>
              </div>
              <div class="section-content">
                <span>1.3</span>
                <p>
                  “Annexure” shall mean the specific annexure attached or added
                  to this Agreement or communicated by RRL from time to time in
                  relation to specific Products and/or Services which RRL may,
                  at its own discretion, add, amend or substitute from time to
                  time. Annexures shall form part of this Agreement and the
                  terms of this Agreement shall be applicable to the Annexures.
                  It is agreed that in the event RRL wishes to introduce a new
                  Product and/or Service or category of Products and/or Services
                  or wishes to discontinue a Product and/or Service or category
                  of Products and/or Services, it shall do so by way of adding
                  or terminating the relevant Annexure for the Product and/or
                  Service or category of Products and/or Services being added or
                  discontinued, as the case may be. It is further agreed that
                  RRL may, at its sole discretion, prescribe one or more terms,
                  conditions, processes or procedures for carrying out business
                  by JioPrime Partner, which may be common to an entire category
                  of Products and/or Services or all or some of them and which
                  will be provided as such in the relevant Annexures either
                  specifically or by reference;
                </p>
              </div>
              <div class="section-content">
                <span>1.4</span>
                <p>
                  “Applicable Law” means all applicable laws, brought into force
                  and effect by GoI or the State Governments, Local/ Municipal
                  Authorities, etc. or by any other competent, statutory
                  authorities etc., including rules, regulations, notifications
                  etc. made there under, and judgments, decrees, injunctions,
                  writs and orders of any court of record, as may be in force
                  and effect during the subsistence of the Agreement;
                </p>
              </div>
              <div class="section-content">
                <span>1.5</span>
                <p>
                  “Confidential Information” includes any data or information
                  disclosed hereunder (whether written, oral or graphical) that
                  relates to the Products and/or Services, financial or
                  commercial information, technology, research, development,
                  processes, know-how, computer programs, prototypes, designs,
                  specifications, techniques, drawings, business development,
                  marketing, customers including customer data or business
                  activities, and which is confidential or proprietary to or a
                  trade secret of RRL and/or Vendor and/or RRL, whether or not
                  the information is marked or identified as confidential at the
                  time of disclosure, or that it is reasonably apparent to the
                  recipient that the information is confidential;
                </p>
              </div>
              <div class="section-content">
                <span>1.6</span>
                <p>
                  “Effective Date” shall mean the date communicated by RRL to
                  JioPrime Partner, and time of such appointment shall be the
                  time when RRL approves the same in its system;
                </p>
              </div>
              <div class="section-content">
                <span>1.7</span>
                <p>
                  “Execution Date” shall mean the date of acceptance of this
                  Agreement by me/us/JioPrime Partner by clicking on “I Agree”
                  button;
                </p>
              </div>
              <div class="section-content">
                <span>1.8</span>
                <p>
                  “GoI” includes the Government of India and any of its duly
                  authorized agency, authority, department, inspectorate,
                  ministry or person (whether autonomous or not) under the
                  lawful and effective control and direction of the Government
                  of India;
                </p>
              </div>
              <div class="section-content">
                <span>1.9</span>
                <p>
                  “Intellectual Property” shall include all vested contingent
                  and future intellectual property rights which are either owned
                  by RRL and/or licensed to RRL including but not limited to
                  goodwill, reputation, rights in Confidential Information,
                  Software, copyright, trademarks, logos, service marks,
                  patents, devices, plans, models, diagrams, specifications,
                  source and object code materials, data and processes, design
                  rights, patents, know how, trade secrets, inventions, get up,
                  database rights (whether registered or unregistered) and any
                  products or registrations for the protection of these rights
                  and all renewals and extensions thereof existing in any part
                  of the world whether now known or created in the future.
                </p>
              </div>
              <div class="section-content">
                <span>1.10</span>
                <p>
                  “JioPrime Partner” shall mean the Person who accepts these
                  terms and conditions and completes the appointment formalities
                  as prescribed by RRL, and further includes any subsequent
                  terms & conditions communicated by RRL shall deem to have
                  activated & applicable for the JioPrime Partner
                </p>
              </div>
              <div class="section-content">
                <span>1.11</span>
                <p>
                  “Manual” shall mean the operational manual prepared/to be
                  prepared by or on behalf of RRL (which shall always remain the
                  sole property of RRL) for use by RRL and/or JioPrime Partner
                  which sets forth information, rules, policies, operational
                  instructions, marketing commercial and statutory/regulatory
                  guidelines to be adhered by JioPrime Partner hereunder. Manual
                  may be updated and modified by RRL periodically and changes
                  shall be communicated to JioPrime Partner by RRL, from time to
                  time;
                </p>
              </div>
              <div class="section-content">
                <span>1.12</span>
                <p>
                  “Outlet” shall mean and include requisite and safe
                  infrastructure, materials, sufficient place and/or offices for
                  the purpose of satisfactory warehousing, storage, display,
                  fulfilment and supply of Products and Services, under this
                  Agreement;
                </p>
              </div>
              <div class="section-content">
                <span>1.13</span>
                <p>
                  “Person” shall mean and include any natural or legal person,
                  who is capable of entering into an agreement;
                </p>
              </div>
              <div class="section-content">
                <span>1.14</span>
                <p>
                  “Products” shall have the meaning assigned to it in specific
                  Annexure executed between the Parties;
                </p>
              </div>
              <div class="section-content">
                <span>1.15</span>
                <p>
                  “RRL” shall mean Reliance Retail Limited, a company
                  incorporated under the Companies Act, 1956, having its
                  registered office at Third floor, Court House, Lokmanya Tilak
                  Marg, Dhobi Talao, Mumbai - 400002
                </p>
              </div>
              <div class="section-content">
                <span>1.16</span>
                <p>
                  “Services” shall have the meaning assigned to it in specific
                  Annexure executed between the Parties; and
                </p>
              </div>
              <div class="section-content">
                <span>1.17</span>
                <p>
                  “Territory” shall have the meaning assigned to it in the
                  applicable Annexures entered from time to time; and
                </p>
              </div>
              <div class="section-content">
                <span>1.18</span>
                <p>
                  “Vendor” shall mean and include the suppliers, manufacturer,
                  importer, sellers and distributors of the Products with whom
                  RRL has entered into enabling arrangements for sale and/or
                  fulfilment and/or supply of the Products and Services.
                </p>
              </div>
              <div class="section-content">
                <span>1.19</span>
                <p>
                  “Consumer Finance Company” shall mean a legal entity which
                  possess required registration and has obtained relevant
                  license from appropriate authority(ties) by which such legal
                  entity is allowed to provide financial assistance to the
                  borrower on the terms and conditions agreed between such legal
                  entity and the borrower.
                </p>
              </div>
              <div class="section-content">
                <span>1.20</span>
                <p>
                  “RRL Indemnified Parties” shall mean RRL, Affiliates of RRL,
                  and their respective shareholders, directors, officers,
                  agents, representatives and employees and persons.
                </p>
              </div>
              <div class="section-title">
                <span>2.</span>
                <strong> APPOINTMENT OF JIOPRIME PARTNER</strong>
              </div>
              <div class="section-content">
                <span>2.1</span>
                <p>
                  By accepting the terms and conditions set forth herein, RRL
                  hereby appoints JioPrime Partner, and JioPrime Partner hereby
                  accepts appointment in the Territory, for the purposes of
                  delivery and/or supply of Services and/or Products to end
                  consumers, on a non-exclusive basis.
                </p>
              </div>
              <div class="section-content">
                <span>2.2</span>
                <p>
                  JioPrime Partner acknowledges that it is acting for the
                  limited purpose of this Agreement and shall not be construed,
                  in any manner whatsoever, as an agent or servant or employee
                  or partner or joint venture of RRL or its Affiliates. For the
                  removal of doubt, JioPrime Partner shall have no rights
                  whatsoever to further sub-franchise or sub-license the rights
                  given under this Agreement.
                </p>
              </div>
              <div class="section-content">
                <span>2.3</span>
                <p>
                  Subject to provisions of this Agreement, the JioPrime Partner
                  shall conduct business as per the relevant Annexure(s) on its
                  own behalf and at its sole risks.
                </p>
              </div>
              <div class="section-title">
                <span>3.</span>
                <strong> APPOINTMENT OF JIOPRIME PARTNER</strong>
              </div>
              <div class="section-content">
                <span>3.1</span>
                <p>
                  JioPrime Partner shall place / facilitate orders, as per the
                  procedure laid down in relevant Annexure(s), orders for the
                  Products and/or materials related to Services to RRL for the
                  purpose of ensuring that the demand thereof is continually
                  serviced. The processes related to placing / facilitating
                  orders, invoicing, fulfilment of orders, delivery & inspection
                  of the Products and/or materials related to Services and
                  related/ associated terms and conditions including
                  consideration, maximum retail price, payment terms, cost of
                  delivery, return and refund policies, applicable indirect
                  taxes etc. shall be detailed out in the relevant Annexure of a
                  Service and/or Product and/or Manual as may be communicated to
                  JioPrime Partner from time to time.
                </p>
              </div>
              <div class="section-content">
                <span>3.2</span>
                <p>
                  JioPrime Partner shall be responsible for any and all
                  applicable duties and taxes levied or imposed by any national,
                  regional or local governmental authority arising out of or in
                  relation to any of the transactions contemplated under this
                  Agreement.
                </p>
              </div>
              <div class="section-content">
                <span>3.3</span>
                <p>
                  Without prejudice to RRL’s rights as provided hereunder or
                  otherwise, the Parties agree that timely payment is the
                  essence of this Agreement. JioPrime Partner shall not deduct
                  any amount from the payment on account of any claim etc. and
                  will make payment as per RRL’s invoice value. Any delay in
                  payment beyond the stated credit period in the invoices shall
                  entitle RRL (i) to levy interest on delayed payment at the
                  then prevailing Marginal Cost Lending Rate (MCLR) of SBI,
                  calculated from the date of invoice to the actual date of
                  payment; and/or (ii) to terminate the Agreement. In case
                  interest to be recovered for delayed payment of invoice value,
                  RRL shall raise debit note on JioPrime Partner for interest
                  amount with GST.
                </p>
              </div>
              <div class="section-content">
                <span>3.4</span>
                <p>
                  Notwithstanding whatever stated in this Agreement or any
                  Annexure(s) and such other Annexures as may be added from time
                  to time, it is agreed by JioPrime Partner that, if RRL and/or
                  any Consumer Finance Company incurs any loss or suffer any
                  liability for the reasons directly or indirectly attributable
                  to the JioPrime Partner, then RRL without prejudice to other
                  remedies available in this Agreement or at law, is entitled to
                  set off such loss or liability from the amount, if any,
                  payable by RRL to JioPrime Partner.
                </p>
              </div>
              <div class="section-title">
                <span>4.</span>
                <strong>
                  {" "}
                  REPRESENTATIONS, WARRANTIES AND COVENANTS OF JIOPRIME PARTNER
                </strong>
              </div>
              <div class="section-content">
                <span>4.1</span>
                <p>
                  JioPrime Partner is duly organized, validly existing, in good
                  standing, qualified to do business within the Territory and is
                  legally authorized to enter into this Agreement;
                </p>
              </div>
              <div class="section-content">
                <span>4.2</span>
                <p>
                  JioPrime Partner is entering into this Agreement on his/her
                  own, to carry out Activities by himself/herself, and not for
                  or on behalf of any third Person, and JioPrime Partner shall
                  provide RRL with proof of the same, as and when required by
                  RRL.
                </p>
              </div>
              <div class="section-content">
                <span>4.3</span>
                <p>
                  JioPrime Partner represents and warrants that he/she is not
                  registered as a GST dealer in the State in which he/she
                  primarily conducts his/her activities. For avoidance of doubt,
                  he/she further declares that, his/her business turnover does
                  not exceed the limits specified in his/her State for
                  registration as GST dealer. This representation will not be
                  applicable for JioPrime Partner, who’s registered as a GST
                  dealer and who provides proof of GST registration to RRL.
                  JioPrime Partner -Activation shall be liable to registered
                  under SGST /UTGST Act in the State or Union territory from
                  where he makes taxable supply of goods or service or both if
                  his/her/its aggregate turnover in the financial year exceeds
                  the amount prescribed by Applicable Law in this regard. In
                  case his/her/its turnover does not exceeds the amount
                  prescribed by Applicable Law in this regard, then the JioPrime
                  Partner shall submit RRL, GST threshold limit declaration as
                  per attached format.
                </p>
              </div>
              <div class="section-content">
                <span>4.4</span>
                <p>
                  JioPrime Partner shall install and use, certain software,
                  systems and/or platforms (including but not limited to those
                  for point of sale, inventory management, etc.) of the
                  specification as detailed out in the Manual at the Outlet
                  (hereinafter referred to as “Software”).JioPrime Partner shall
                  use such Software solely for the purposes of carrying on the
                  business related to Products and/or Services and as advised
                  RRL from time to time.
                </p>
              </div>
              <div class="section-content">
                <span>4.5</span>
                <p>
                  JioPrime Partner shall be required to acquire/purchase, as
                  decided by RRL, such Equipment(s) and/or Software(s) as
                  communicated by RRL from time to time. The Equipment(s) and/or
                  Software(s) which are required to be acquired and/or purchased
                  by JioPrime Partner will differ as per the category in which
                  the JioPrime Partner is empaneled /registered with RRL. RRL,
                  at its sole discretion, and on case to case basis, may arrange
                  financial assistance for such Equipment(s) and/or Software(s).
                  Till such time the financial assistance, if any provided by
                  RRL or by the Consumer Finance Company, is not completely
                  repaid, the title and security interest in such Equipment(s)
                  and/or Software(s) shall remain solely with RRL and/or
                  Consumer Finance Company.
                </p>
              </div>
              <div class="section-content">
                <span>4.6</span>
                <p>
                  JioPrime Partner and/or its employees, agents or
                  representatives, shall utilize such Equipment(s) and/or
                  Software(s) only for the limited purpose as prescribed in this
                  Agreement and Annexure(s) as may be modified from time to
                  time. In no event shall JioPrime Partner or its employees,
                  agent, representative, utilize the Equipment(s) and/or
                  Software(s) for any third person or entity or any other
                  purpose. JioPrime Partner shall not use, Equipment(s) and/or
                  Software(s) for any purpose not permitted by Applicable Law.
                  JioPrime Partner further agrees and acknowledges that
                  Equipment(s) and/or Software(s) to which it has been granted
                  right to use is very crucial and pertinent to RRL and that any
                  improper use thereof, intentionally or otherwise, shall result
                  in huge losses to RRL. In the event RRL incurs any loss or
                  damage due to such improper use and/or negligence, JioPrime
                  Partner undertakes to indemnify RRL fully with respect
                  thereto. It is however agreed that RRL may, in certain cases,
                  require JioPrime Partner to purchase or lease or otherwise
                  make available certain Equipment(s) and/or Software(s) from
                  RRL or any of its nominated agency as per such payment
                  mechanism as may be mutually agreed between the Parties in
                  writing.
                </p>
              </div>
              <div class="section-content">
                <span>4.7</span>
                <p>
                  JioPrime Partner shall prepare, maintain and submit to RRL, on
                  a periodic basis, all documentations and reports as may be
                  requested by RRL from time to time;
                </p>
              </div>
              <div class="section-content">
                <span>4.8</span>
                <p>
                  JioPrime Partner shall promptly and properly comply with all
                  the terms and conditions of this Agreement, as may be amended
                  from time to time including all Applicable Laws, Manual,
                  Annexure, etc., any change in the provisions of this Agreement
                  which may become necessary due to change in any Applicable Law
                  or any change in the conditions of the license granted by any
                  regulatory authority in relation to the Products and/or
                  Services to RRL and/or Vendor or their respective Affiliates;
                </p>
              </div>
              <div class="section-content">
                <span>4.9</span>
                <p>
                  The warranties in relation to the Services and/or Products
                  shall be on such terms and in such manner as provided by the
                  Vendor and as specified in the relevant Annexure
                </p>
              </div>
              <div class="section-content">
                <span>4.10</span>
                <p>
                  RRL may provide advertisement and marketing material to
                  JioPrime Partner from time to time and JioPrime Partner shall
                  use such material for advertising purposes strictly in
                  accordance with the Manual or instructions provided by RRL.
                  JioPrime Partner agrees that all rights in and related to the
                  advertising material provided by RRL shall belong to RRL or
                  relevant Vendor and JioPrime Partner shall not misuse or copy
                  the same for any other purpose
                </p>
              </div>
              <div class="section-content">
                <span>4.11</span>
                <p>
                  JioPrime Partner shall not, directly or indirectly, including
                  through any agents, distribute any Services and/or Products
                  outside the Territory and shall not solicit orders for
                  Products and/or Services, advertise the Services and/or
                  Products or keep any stock of the Products and/or materials
                  related to Services outside the Territory. JioPrime Partner
                  shall not distribute the Services and/or Products on a bulk
                  and/or wholesale basis. JioPrime Partner further agrees and
                  acknowledges that he shall not distribute the Services and/or
                  Products either directly or indirectly, through any other
                  means otherwise stated in this Agreement, including without
                  limitation online channels/e-commerce portals, etc.;
                </p>
              </div>
              <div class="section-content">
                <span>4.12</span>
                <p>
                  JioPrime Partner shall endeavor to promptly address all
                  communications from customers including resolution of
                  complaints and grievances and shall at all times assist in
                  providing support services to customers of the Services and/or
                  Products in the Territory, if any
                </p>
              </div>
              <div class="section-content">
                <span>4.13</span>
                <p>
                  In relation to the Outlet, JioPrime Partner represents and
                  warrants that it holds absolute, valid, enforceable and sole
                  lease/license/ownership rights to use the Outlet for the
                  purposes of its business and that the Outlet have been
                  licensed by concerned government authorities to be used for
                  commercial and business purposes. It is agreed that any cost,
                  expenses, rent, fee, fines, penalties etc. in relation to
                  Outlet shall be solely borne by JioPrime Partner and that, RRL
                  shall not be liable for the same in any manner whatsoever and
                  JioPrime Partner agrees to keep RRL indemnified in this regard
                </p>
              </div>
              <div class="section-content">
                <span>4.14</span>
                <p>
                  The Products and/or materials related to Services shall be
                  supplied in the manner stated by RRL or included in the terms
                  and conditions prescribed by the Vendors and JioPrime Partner
                  shall ensure that the quality, quantity, specifications,
                  packaging, labelling are the same as provided by the Vendor
                  and/or RRL except normal wear and tear. With respect to the
                  Services, JioPrime Partner shall make available the Products
                  and/or materials related to Services (as applicable) which
                  will enable the ultimate customer to avail the related
                  Services for which the customers shall directly enter into a
                  contract with the concerned Vendor. It is agreed that JioPrime
                  Partner shall not make any commitments, promises or
                  representations, warranties, express or implied, to the
                  customer with respect to the Products and/or Services other
                  than those specifically intimated by RRL and/or stated in the
                  Annexure. It is further agreed that JioPrime Partner shall
                  ensure that all related statutory/legal compliances as
                  prescribed in the relevant Annexure and/or Manual have been
                  complied with by it including the DoT/TRAI guidelines issued
                  from time to time and shall sign the ACT letter and
                  acknowledge and abide by the contents and provisions of the
                  same;
                </p>
              </div>
              <div class="section-content">
                <span>4.15</span>
                <p>
                  JioPrime Partner agrees that it will not use in any way for
                  its own account or the account of any third party, nor
                  disclose to any third party, any such Confidential Information
                  revealed to it in written or other tangible form or orally,
                  identified as confidential, by RRL and/or Vendor and/or RRL.
                  JioPrime Partner shall take every reasonable precaution to
                  protect the confidentiality of such Confidential Information;
                </p>
              </div>
              <div class="section-content">
                <span>4.16</span>
                <p>
                  JioPrime Partner acknowledges that the Intellectual Property
                  is owned by or licensed (explicit or implied) to RRL or Vendor
                  and/or respective Affiliates and that JioPrime Partner shall
                  acquire no rights in the Intellectual Property by reason of
                  use of the same by JioPrime Partner. JioPrime Partner shall
                  use the Intellectual Property exclusively for carrying out its
                  obligations as envisaged under this Agreement. Any fit-outs,
                  installations within the Outlet, including of any signage
                  pertaining to the Products and/or Services, shall be subject
                  to prior written approval of RRL
                </p>
              </div>
              <div class="section-content">
                <span>4.17</span>
                <p>
                  JioPrime Partner represents and warrants that he shall
                  maintain adequate and appropriate insurance from an insurance
                  company for all losses and damages arising out of or in
                  relation to the operation of this Agreement or arising out of
                  the acts or omissions of the JioPrime Partner and shall not
                  hold RRL liable for the same; and
                </p>
              </div>
              <div class="section-content">
                <span>4.18</span>
                <p>
                  JioPrime Partner hereby agrees to indemnify and hold harmless
                  RRL and/or Vendors and their respective shareholders,
                  directors and employees (collectively the “Indemnified
                  Parties”), against all actions, proceedings, claims,
                  liabilities (including statutory liabilities), penalties,
                  demands and costs (including without limitation, legal costs),
                  awards, damages, losses and/or expenses however arising as a
                  result of breach of any representation, warranty, covenant,
                  undertaking given by JioPrime Partner herein, or for any
                  fines, penalties or interest imposed directly or indirectly on
                  the Indemnified Parties on account of JioPrime Partner’s
                  activities, commissions or omissions under this Agreement, or
                  for any gross negligent act of JioPrime Partner (or of any of
                  its employees, agents, visitors etc.) or any claim or
                  liability made against the Indemnified Parties which pertains
                  to the terms of this Agreement and may be claimed against the
                  Indemnified Parties including without limitation, tax and
                  other statutory liabilities, salaries and other dues towards
                  employees, consumer claims, intellectual property claims,
                  claims with respect to compliance with Applicable Laws and for
                  any damages, additional costs incurred on account of any
                  default by JioPrime Partner pertaining to day to day sale,
                  maintenance, hygiene, safety etc. of the Outlet.
                </p>
              </div>
              <div class="section-content">
                <span>4.19</span>
                <p>
                  JioPrime Partner shall provide RRL with the details of any of
                  the transactions carried out by JioPrime Partner – Activation
                  pursuant to this Agreement and Annexures thereto, whenever
                  required by RRL
                </p>
              </div>
              <div className="subCont">
                <div class="section-content">
                  <span>4.20</span>
                  <p>
                    JioPrime Partner, who’s registered under the GST Law, hereby
                    undertakes to comply with following:
                  </p>
                </div>
                <div class="section-content">
                  <span>4.20.1</span>
                  <p>
                    To raise invoice compliant with GST Law and this Agreement;
                  </p>
                </div>
                <div class="section-content">
                  <span>4.20.2</span>
                  <p>
                    To pay applicable GST in appropriate Government Treasury in
                    time as specified under the provisions of GST Law;
                  </p>
                </div>
                <div class="section-content">
                  <span>4.20.3</span>
                  <p>
                    Timely uploading of invoice details and filing of GST
                    returns on GSTN portal;
                  </p>
                </div>
                <div class="section-content">
                  <span>4.20.4</span>
                  <p>
                    Co-operate with RRL wherever there is mismatch in the
                    invoice details on GSTN portal and take corrective action to
                    rectify the mismatches in time;
                  </p>
                </div>
                <div class="section-content">
                  <span>4.20.5</span>
                  <p>
                    In the event any back charges, liquidated damages, or
                    damages on any other account are payable by the User to RRL,
                    then RRL shall raise an invoice as per GST Law for any such
                    amounts, the User is liable to pay RRL along with the
                    applicable GST amount calculated thereon and the User shall
                    pay or reimburse such amount forthwith along with GST
                    amount.
                  </p>
                </div>
                <div class="section-content">
                  <span>4.20.6</span>
                  <p>
                    Indemnify and hold RRL Indemnified Parties harmless from and
                    against any and all (a) claims, suits and actions which are
                    brought against; and (b) all losses (including loss of input
                    tax credit, payment of interest, or imposition of penalties)
                    incurred by RRL Indemnified Parties for or relating to
                    non-compliance by the user of the requirements under the GST
                    Law, including as set forth in this Section.
                  </p>
                </div>
                <div class="section-content">
                  <span>4.20.7</span>
                  <p>
                    In the event RRL had reimbursed the GST amount to the user
                    for which RRL is denied input tax credit for any reasons
                    attributable to the user, then RRL shall be entitled to
                    adjust, off-set from the amounts owed by RRL to the User or
                    recover from the user such loss of input tax credit, as RRL
                    may deem appropriate.
                  </p>
                </div>
              </div>
              <div class="section-content">
                <span>4.21</span>
                <p>
                  JioPrime Partner shall provide and/or make available to RRL,
                  whenever required by RRL, documents related to its
                  incorporation, income tax registration, GST registration,
                  documents pertaining to proof of identity, proof of address,
                  including details of the authorized signatory(ies), authorized
                  representative(s), and any other relevant document pertaining
                  to carrying out business as JioPrime Partner (collectively
                  referred to as “Relevant Document”). JioPrime Partner further
                  agrees that RRL, any of its Affiliates, Vendors, Consumer
                  Finance Company, etc. shall be entitled to use the Relevant
                  Document(s) provided by JioPrime Partner, for the purpose of
                  and/or related to the business, present or future, pursuant to
                  this Agreement. In the event of any change, correction or
                  modification in any of its Relevant Document(s), JioPrime
                  Partner agrees and undertakes to provide to RRL copy(ies) of
                  the same, on immediate basis.
                </p>
              </div>
              <div class="section-title">
                <span>5.</span>
                <strong>GOVERNING LAW AND DISPUTE RESOLUTION</strong>
              </div>
              <div class="section-content">
                <span>5.1</span>
                <p>
                  This Agreement shall be construed and interpreted in
                  accordance with laws of India. Any dispute or difference
                  arising out of or in connection with the interpretation or
                  implementation or breach / alleged breach of this
                  Agreement(‘Dispute’), shall be referred to and settled by
                  arbitration in accordance with the Arbitration and
                  Conciliation Act, 1996 by a sole arbitrator appointed by RRL.
                  The arbitration shall be conducted in the English language.
                  The seat of arbitration shall be Mumbai. Subject to what is
                  stated hereinabove, the Courts at Mumbai shall have sole and
                  exclusive jurisdiction in respect of all matters arising out
                  of or in connection with this Agreement.
                </p>
              </div>
              <div class="section-title">
                <span>6.</span>
                <strong>GOVERNING LAW AND DISPUTE RESOLUTION</strong>
              </div>
              <div class="section-content">
                <span>6.1</span>
                <p>
                  Term: This Agreement shall come into force and effect from the
                  Execution Date and shall remain in force unless terminated
                  earlier under the provisions of this Agreement (the “Term”).
                  The term for carrying out specific Activity shall be as
                  specified in the specific Annexures. RRL shall be entitled to
                  terminate this Agreement without any cause during the Term of
                  the Agreement, after serving a written notice of 15 days to
                  JioPrime Partner.
                </p>
              </div>
              <div class="section-content">
                <span>6.2</span>
                <p>
                  The JioPrime Partner’s rights under this Agreement and all the
                  then existing Annexure(s) shall terminate automatically
                  without notice from RRL, if the JioPrime Partner fails to
                  comply with any of the terms and conditions of this Agreement,
                </p>
              </div>
              <div class="section-content">
                <span>6.3</span>
                <p>
                  If JioPrime Partner commits any breach of any of the
                  provisions of this Agreement, RRL will be entitled to suspend,
                  either part or full operations, of JioPrime Partner under this
                  Agreement or any of the Annexure(s). In such situation, until
                  RRL terminates the Agreement, the consequences as stated under
                  Clause 6.5 will not be applicable, however during such
                  suspension period, JioPrime Partner will not be entitled for
                  any rights under this Agreement or any individual Annexure(s).
                  Such suspension period, if any, needs to be explicitly, and in
                  writing, withdrawn by RRL in order for JioPrime Partner to
                  continue to function pursuant to this Agreement and any of the
                  Annexure(s).
                </p>
              </div>
              <div class="section-content">
                <span>6.4</span>
                <p>
                  Upon termination of this Agreement or any Annexure(s), RRL
                  shall be entitled to disallow or terminate access and/or
                  ability to use the Equipment for any of the activity under
                  this Agreement or relevant Annexure(s), as long as the
                  JioPrime Partner has not made complete payment along with any
                  other relevant charges to the Finance Company and/or RRL.
                </p>
              </div>
              <div className="subCont">
                <div class="section-content">
                  <span>6.5</span>
                  <p>Consequences of Termination</p>
                </div>
                <div class="section-content">
                  <span>6.5.1</span>
                  <p>
                    JioPrime Partner shall cease holding itself out as a
                    retailer of the Services and/or Products and cease to use
                    the Intellectual Property of RRL or Vendor or its respective
                    Affiliates. Upon termination of the Agreement, JioPrime
                    Partner shall return, as applicable, all the Equipment(s),
                    other than which JioPrime Partner has purchased and made
                    full payment to RRL and/or to the Finance Company (wherever
                    applicable), and Software(s) lying with it on terms and
                    conditions set out in the Agreement and all the applicable
                    Annexures;
                  </p>
                </div>
                <div class="section-content">
                  <span>6.5.2</span>
                  <p>
                    After effective date of termination of this Agreement, RRL
                    shall complete the reconciliation, which shall include but
                    not limited to, return of Equipment(s) and assets of RRL,
                    return of Confidential Information, return on promotional
                    material(s), etc. On completion of reconciliation, RRL shall
                    determine the amount receivable or payable from/to JioPr
                    ment of account with RRL. JioPrime Partner, before
                    receipt/payment of money, need to accept full & final
                    statement provided by RRL. JioPrime Partner will not be
                    entitled to raise any claims against RRL once JioPrime
                    Partner accepts the full & final settlement statement. RRL,
                    on the other hand, will be entitled to raise any claims
                    against JioPrime Partner which are not captured in full &
                    final settlement and/or revealed after the full & final
                    settlement. The full & final settlement of JioPrime Partner
                    will not relieve JioPrime Partner from the obligations which
                    are continual in nature till such time such obligations are
                    completely fulfilled by JioPrime Partner.
                  </p>
                </div>
                <div class="section-content">
                  <span>6.5.3</span>
                  <p>
                    JioPrime Partner shall cease using and return all
                    Confidential Information, Software, Equipment and
                    Intellectual Property and remove all Software (including any
                    back-up and cache files), promotional material, design,
                    interiors and other materials belonging to RRL and/or
                    Vendor. JioPrime Partner shall not make or retain any
                    copies/back-up files of any Confidential Information and
                    Intellectual Property that may have been entrusted to it.
                    JioPrime Partner’s use and enjoyment of any such property or
                    any benefits accruing thereof shall be considered as illegal
                    and deemed to be a criminal misappropriation and trespass.
                  </p>
                </div>
                <div class="section-content">
                  <span>6.5.4</span>
                  <p>
                    Unless otherwise stated in a particular Annexure or
                    otherwise agreed in writing by RRL, the JioPrime Partner
                    shall cease using Equipment(s) and/or Software(s) and return
                    the same to RRL in the same condition as it had received
                    them subject to normal wear and tear except in case where
                    such Equipment(s) and/or Software(s) were purchased by
                    JioPrime Partner
                  </p>
                </div>
              </div>
              <div class="section-content">
                <span>6.6</span>
                <p>
                  Notwithstanding anything contained in this Agreement, RRL
                  shall have a right to inspect and take copies of any records
                  maintained by JioPrime Partner with respect to performance of
                  its obligations hereunder, even after termination of this
                  Agreement for up to seven (7) years post termination.
                </p>
              </div>
              <div class="section-title">
                <span>7.</span>
                <strong>MISCELLANEOUS</strong>
              </div>
              <div class="section-content">
                <span>7.1</span>
                <p>
                  Assignment: It is hereby agreed that JioPrime Partner shall
                  not assign or transfer the Agreement or any rights and
                  obligations hereunder to any other person without the prior
                  written approval of RRL. Any change in the management or
                  Control (as defined under “Affiliates”) of JioPrime Partner
                  shall be deemed to be an assignment requiring the prior
                  written approval of RRL
                </p>
              </div>
              <div class="section-content">
                <span>7.2</span>
                <p>
                  Notices: All notices, requests, claims, demands and other
                  communications under the Agreement shall be in writing
                  addressed to the Party concerned at the address available in
                  the records of RRL. In addition, and notwithstanding, JioPrime
                  Partner hereby expressly consents and authorizes RRL and/or
                  its authorised representative to send any communications,
                  document, content by way of WhatsApp or any other electronic
                  mode of communication.
                </p>
              </div>
              <div class="section-content">
                <span>7.3</span>
                <p>
                  Amendment: Any amendment to this Agreement shall be
                  communicated and/or published from time to time by RRL to
                  JioPrime Partner. The JioPrime Partner should always keep
                  itself aware about any changes/updates communicated or
                  published on the portal to which JioPrime Partner shall be
                  given access for performance of the Services under this
                  Agreement. In such case if JioPrime Partner continues to
                  perform its services under this Agreement, it shall be treated
                  as its acceptance of the amendment.
                </p>
              </div>
              <div class="section-content">
                <span>7.4</span>
                <p>
                  Waiver: No waiver of any breach of any provision of the
                  Agreement shall constitute a waiver of any prior, concurrent
                  or subsequent breach of the same or of any other provisions
                  hereof.
                </p>
              </div>
              <div class="section-content">
                <span>7.5</span>
                <p>
                  Order of Preference: This Agreement shall automatically
                  supersede any and all previous agreements, terms & conditions
                  between the JioPrime Partner (in any capacity) and RRL (“Terms
                  and Conditions”) and such Terms and Conditions shall stand
                  terminated, from the Effective Date of this Agreement.
                </p>
              </div>
              <div class="section-content">
                <span>7.6</span>
                <p>
                  The Parties confirm having received independent professional
                  legal and tax advice in relation to this Agreement and have
                  relied on such advice to enter into this Agreement.
                </p>
              </div>
              <div class="section-content">
                <span>7.7</span>
                <p>
                  If the terms and conditions stated herein are in conflict with
                  any terms and conditions agreed by the parties, in such
                  situation the terms which are subsequently, to the extent in
                  conflicting with earlier terms and conditions shall survive.
                </p>
              </div>
              <div class="section-content">
                <span>7.8</span>
                <p>
                  This is system generated document and signature of RRL is not
                  required.
                </p>
              </div>
              <div class="section-title">
                <strong>ANNEXURE – P1 DEVICES</strong>
              </div>
              <div className="subCont">
                <div class="section-content">
                  <span>1.</span>
                  <p>
                    "Product": The term Product shall mean mobile handsets, IT
                    products, consumer durables, electronic goods, electrical
                    appliances, related accessories, spares & supplies, and such
                    other products as may be added by RRL as per the terms of
                    the Agreement having:
                  </p>
                </div>
                <div class="section-content">
                  <span>a.</span>
                  <p>Vendor, as specified in the product list</p>
                </div>
                <div class="section-content">
                  <span>b.</span>
                  <p>issued from time to time;</p>
                </div>
                <div class="section-content">
                  <span>c.</span>
                  <p>
                    Model, as specified in the product list issued from time to
                    time; and
                  </p>
                </div>
                <div class="section-content">
                  <span>d.</span>
                  <p>
                    Specifications, as detailed in the product list issued from
                    time to time.
                  </p>
                </div>
              </div>
              <div class="section-content">
                <span>2.</span>
                <p>
                  "Sale Price": Prices payable by the JioPrime Partner and
                  charged by RRL for the Products shall be as per the prices
                  mentioned in the product price list provided/published by RRL
                  from time to time. JioPrime Partner acknowledges that RRL
                  shall have the right, exercisable from time to time in RRL’s
                  sole discretion, to change the price list. Sale Price shall be
                  inclusive of all taxes including GST or similar other tax on
                  sale / supply of goods by whatever name called. RRL may
                  separately charge for transit insurance, shipping/ freight
                  charges and other charges. JioPrime Partner’s selling price to
                  the Consumer shall not exceed the Maximum Retail Price (MRP)
                  as printed on the relevant Product(s).
                </p>
              </div>
              <div class="section-content">
                <span>3.</span>
                <p>
                  "Business Plan": JioPrime Partner shall provide weekly rolling
                  sale and/or fulfilment and/or supply and/or inventory forecast
                  for the Products including the forecast at the regional and
                  local level.
                </p>
              </div>
              <div class="section-content">
                <span>4.</span>
                <p>
                  "Inventory Planning & Order Processing": JioPrime Partner
                  shall maintain inventory for each of the Products as per the
                  mutually agreed Business Plan. Pursuant to the agreed
                  inventory level and Business Plan, JioPrime Partner shall
                  place purchase orders for the Products on RRL as per the
                  process detailed in the Manual. RRL may, in order to achieve
                  efficiencies in stocking and/or sales and/or fulfilment and/or
                  supply at its discretion, specify minimum ordering quantities
                  (MOQ) for each Product and all purchase orders shall be
                  subject to such MOQ. MOQ for each Product shall be subject to
                  change by RRL from time to time.
                </p>
              </div>
              <div class="section-content">
                <span>5.</span>
                <p>
                  "Order fulfilment & Delivery of Products": Upon receipt of the
                  purchase order, RRL will raise an invoice and make deliveries
                  as per the quantity billed in the invoice (“Invoice”). RRL
                  shall make every effort to execute the Invoices efficiently
                  but shall not be responsible, for any loss, damage, or other
                  liability suffered or incurred by JioPrime Partner, due to any
                  failure or delay on RRL’s part in effecting delivery of any
                  consignment(s) of the Products. RRL shall deliver the Products
                  to the designated delivery points as specified in the purchase
                  order and as mutually agreed (“Delivery Point”). The delivery
                  of the Products to the Delivery Point shall be the
                  responsibility of RRL. It is hereby agreed that it shall be
                  the responsibility of JioPrime Partner to collect the Products
                  from the Delivery Point immediately. JioPrime Partner shall
                  inspect the Products and intimate in writing, within 2 days
                  the damaged/defective Products or otherwise shortages in the
                  Products so delivered, to RRL. RRL, after investigation, shall
                  replace the said damaged/ short shipment of Products or shall
                  issue credit note for the same. In case no intimation is
                  received as per the above timelines, JioPrime Partner shall be
                  deemed to have accepted the Products as being in order and any
                  defects caused or found in the Products thereafter shall be to
                  the account of JioPrime Partner and RRL shall not be liable
                  for the same in any manner whatsoever.
                </p>
              </div>
              <div class="section-content">
                <span>6.</span>
                <p>
                  "Stocking and Fulfilmen" : JioPrime Partner, in order to
                  fulfill its obligations under this Annexure, may, subject to
                  consents/approvals from RRL, procure certain Product(s) for
                  stocking and effective fulfilment.
                </p>
              </div>
              <div class="section-content">
                <span>7.</span>
                <p>
                  "Payment Terms & Penalties on Delay": All payments shall be
                  made on the due date failing which the JioPrime Partner shall
                  be liable for interest as per the policy for delayed payments.
                  In case interest to be recovered for delayed payment of
                  invoice value, RRL shall issue debit note for interest amount
                  with GST.
                </p>
              </div>
              <div class="section-content">
                <span>8.</span>
                <p>
                  "TDS" : RRL shall deduct applicable TDS (tax deducted at
                  source) or any other withholding taxes that may replace TDS in
                  accordance with the provisions of the Income Tax Act, 1961.
                </p>
              </div>
              <div className="subCont">
                <div class="section-content">
                  <span>9.</span>
                  <p>
                    "Invoicing by JioPrime Partner to Consumer:": In the event
                    the Consumer wishes to purchase Product(s) from JioPrime
                    Partner, then the JioPrime Partner shall adopt following
                    steps:
                  </p>
                </div>
                <div
                  class="fDc section-content"
                  
                >
                  <span>a. Finalization of the Product</span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>
                      i. JioPrime Partner shall select and reconfirm with
                      Consumer the exact Product Code,
                    </p>
                    <p>
                      ii. Wherever applicable, the JioPrime Partner shall find
                      out the availability status of the Product and tentative
                      delivery date.
                    </p>
                    <p>
                      iii. JioPrime Partner shall take best efforts for
                      completion of transaction by the Consumer.
                    </p>
                  </div>
                </div>
                <div
                  class="fDc section-content"
                  
                >
                  <span>b. Invoice to Consumer</span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>
                      i. JioPrime Partner shall issue an invoice to the Consumer
                      who purchases/acquires the Product(s), in the format
                      proposed /approved by RRL. The invoice will be generated
                      with the help of the Equipment(s) and/or Software(s)
                      provided by RRL to JioPrime Partner, as per applicable
                      category and as communicated from time to time by RRL.
                    </p>
                  </div>
                </div>
                <div
                  class="fDc section-content"
                  
                >
                  <span>c. Payment</span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>
                      i. JioPrime Partner shall accept the payment with the help
                      of any of the payment option, including but not limited
                      to, Cash, Debit Card, Credit Card, UPI, Net banking,
                      JioMoney, Wallet, etc. and such other digital or
                      authorised mode of payment as may be recommended by
                      Reliance from time to time
                    </p>
                  </div>
                </div>
                <div
                  class="fDc section-content"
                  
                >
                  <span>d. Delivery</span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>
                      i. Delivery by JioPrime Partner to Ship to Address:
                      JioPrime Partner shall, wherever possible, suggest to the
                      Consumer, the slot(s) for delivery of the Product(s), and
                      record the choice of the Consumer for delivery date and
                      slot.
                    </p>
                    <p>
                      ii. Whenever JioPrime Partner is delivering the Product at
                      Ship to Address, JioPrime Partner shall adhere to the
                      standard delivery guidelines for delivery.
                    </p>
                  </div>
                </div>
                <div
                  class="fDc section-content"
                  
                >
                  <span>
                    e. Pick-up facilitation at Pick-up location & process
                    adherence
                  </span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>
                      i. JioPrime Partner shall create dedicated space for
                      storage of items delivered at the Store to be handed over
                      to the Consumer.
                    </p>
                    <p>
                      ii. JioPrime Partner shall also ensure that, there’s no
                      inter-exchange for deliveries of same/similar
                      Product/orders.
                    </p>
                    <p>
                      Certain Products, which shall include but not limited to
                      the Products requiring installation, bulky & heavy
                      Products, etc., shall qualify for delivery at Ship to
                      Address.
                    </p>
                  </div>
                </div>
              </div>
              <div className="subCont">
                <div class="section-content">
                  <span>9.</span>
                  <p>
                    Flow of financial assistance, in case of consumer finance:
                  </p>
                </div>
                <div class="section-content">
                  <span>a.</span>
                  <p>
                    Consumer avails financial assistance under any of the
                    finance scheme of the Consumer Finance Company, through
                    JioPrime Partner at the time purchasing the Product(s).
                  </p>
                </div>
                <div class="section-content">
                  <span>b.</span>
                  <p>
                    Consumer Finance Company, within agreed period of time, will
                    transfer to RRL the amount equivalent to financial
                    assistance availed by the Consumer.
                  </p>
                </div>
                <div class="section-content">
                  <span>c.</span>
                  <p>
                    RRL shall, transfer to JioPrime Partner, the amount
                    equivalent to the financial assistance availed by the
                    Consumer, after deductions/adjustments of receivables from
                    JioPrime Partner.
                  </p>
                </div>
                <div class="section-content">
                  <span>d.</span>
                  <p>
                    Payment received by RRL from Consumer Finance Company shall
                    be considered by JioPrime Partner as if the payment is
                    received by JioPrime Partner for the relevant Product
                    purchased by the Consumer, and accordingly JioPrime Partner
                    shall deliver the relevant Product(s) to the Consumer.
                  </p>
                </div>
              </div>
              <div className="subCont">
                <div class="section-content">
                  <span>10.</span>
                  <p>Consumer Finance:</p>
                </div>
                <div class="section-content">
                  <span>a.</span>
                  <p>
                    RRL and/or Consumer Finance Company shall, from time to
                    time, announce the consumer finance schemes for the
                    Consumers
                  </p>
                </div>
                <div class="section-content">
                  <span>b.</span>
                  <p>
                    JioPrime Partner shall communicate all such consumer finance
                    schemes to the Consumer(s).
                  </p>
                </div>
                <div class="section-content">
                  <span>c.</span>
                  <p>
                    JioPrime Partner shall collect all such supporting documents
                    as may be required as per RBI guidelines, and as
                    communicated by RRL and/or Consumer Finance Company from
                    time to time (hereinafter collectively referred to as
                    “Documents”).
                  </p>
                </div>
                <div class="section-content">
                  <span>d.</span>
                  <p>
                    JioPrime Partner shall collect the Documents in diligent
                    manner and wherever applicable, check the Documents with
                    original and certify the same. In the event of any
                    negligence on the part of JioPrime Partner, the same shall
                    be treated as one of the reasons for indemnification by
                    JioPrime Partner to RRL and/or Consumer Finance Company.
                  </p>
                </div>
                <div class="section-content">
                  <span>e.</span>
                  <p>
                    JioPrime Partner will facilitate reporting and resolution of
                    Consumer complaints relating to the finance facility to RRL
                    and / or Consumer Finance Company.
                  </p>
                </div>
                <div class="section-content">
                  <span>f.</span>
                  <p>
                    JioPrime Partner hereby agrees to be engaged as a repayment
                    channel agent for RRL and / or Consumer Finance Company
                  </p>
                </div>
                <div class="section-content">
                  <span>g.</span>
                  <p>
                    JioPrime Partner to provide all required assistance to RRL
                    and / or Consumer Finance Company for them to take action
                    against the Consumer for the default committed by such
                    Consumer.
                  </p>
                </div>
                <div class="section-content">
                  <span>h.</span>
                  <p>
                    JioPrime Partner shall not issue any written
                    communication(s), make unsolicited calls, make commitments
                    or negotiate terms of the finance scheme on behalf of RRL
                    and / or Consumer Finance Company. JioPrime Partner shall
                    strictly adhere to all privacy laws including Information
                    Technology (Reasonable Security Practices and Procedures and
                    Sensitive Data or Information) Rules, 2011.
                  </p>
                </div>
              </div>
              <div className="subCont">
                <div class="section-content">
                  <span>11.</span>
                  <p>Warranty:</p>
                </div>
                <div class="section-content">
                  <span>a.</span>
                  <p>
                    Product shall be covered by Vendor’s Limited Warranty
                    (“MLW”) as specified with each Product by the Vendor.
                  </p>
                </div>
                <div class="section-content">
                  <span>b.</span>
                  <p>
                    JioPrime Partner shall sell / make available the Products
                    with the MLW.
                  </p>
                </div>
                <div class="section-content">
                  <span>c.</span>
                  <p>
                    JioPrime Partner shall not combine Products, sell Products
                    outside the geographical limits of India or do any action to
                    invalidate MLW.
                  </p>
                </div>
                <div class="section-content">
                  <span>d.</span>
                  <p>
                    JioPrime Partner is expressly prohibited from making
                    representations or extending any warranties to any third
                    party with respect to any of the Products except for
                    warranties made/provided by the Vendor and included in the
                    packaging with each of the Products or in any promotional
                    literature provided by RRL to the JioPrime Partner
                  </p>
                </div>
                <div class="section-content">
                  <span>e.</span>
                  <p>
                    JioPrime Partner shall be solely responsible for any errors,
                    faults or defects to Products caused by the JioPrime Partner
                    or its personnel after delivery to the JioPrime Partner.
                    Vendor's warranties shall exclude losses caused by improper
                    or insufficient maintenance, normal wear and tear, any
                    improper operating, storing, handling, installation, bracing
                    of the Product by the JioPrime Partner, and/or usage of the
                    Product in violation of the instructions furnished by the
                    Vendor,
                  </p>
                </div>
                <div class="section-content">
                  <span>f.</span>
                  <p>
                    Any and all warranties hereunder shall immediately terminate
                    in the event that any parts and/or components of the Product
                    are altered or modified by the JioPrime Partner or its
                    personnel without the express prior written consent of RRL,
                    as well as in the event of a finding, given the
                    circumstances, that the defect did not exist at the time RRL
                    supplied the Products.
                  </p>
                </div>
              </div>
              <div class="section-content">
                <span>12.</span>
                <p>
                  “After Sales Support": After Sales Support shall be as per
                  Vendor’s after sales service & support policy as may be
                  applicable for each Product.
                </p>
              </div>
              <div class="section-content">
                <span>13.</span>
                <p>
                  "Price Support": Price Support for the Products will be as per
                  the Vendor’s policy and as communicated from time to time.
                </p>
              </div>
              <div class="section-content">
                <span>14.</span>
                <p>
                  "Return Policy": Products sold shall not be returnable unless
                  specifically approved.
                </p>
              </div>
              <div class="section-content">
                <span>15.</span>
                <p>
                  "Reporting": JioPrime Partner shall prepare and maintain, and
                  submit to RRL on a timely basis, all documentation and reports
                  as may be requested by RRL from time to time to be prepared,
                  maintained or submitted in a prescribed format as may be
                  communicated to JioPrime Partner from time to time. JioPrime
                  Partner shall be responsible for sharing the monthly/
                  quarterly Business Plan and the sell-out information for the
                  purpose of forecasting demand for the Products
                </p>
              </div>
              <div class="section-content">
                <span>16.</span>
                <p>
                  "Statutory Compliance": It will be an integral responsibility
                  of JioPrime Partner to ensure that all obligations that are
                  required to be mandatorily fulfilled as prescribed by any
                  Competent Authority and/ or any other mandatory requirements
                  under the Applicable Law, are fulfilled by the JioPrime
                  Partner or authorized persons at the point of sale/Pick-up
                  Location(s).
                </p>
              </div>
              <div class="section-content">
                <span>17.</span>
                <p>"Territory": As defined from time to time</p>
              </div>
              <div class="section-content">
                <span>18.</span>
                <p>
                  "Term": This Annexure (Annexure-P1) shall be valid from the
                  date of execution till the date of termination.
                </p>
              </div>
              <div class="section-content">
                <span>19.</span>
                <p>
                  Termination of Annexure – P1: RRL shall be entitled to
                  terminate this Annexure (Annexure–P1) without any cause during
                  the Term of the Annexure–P1 after serving a written notice of
                  termination to the JioPrime Partner. Notwithstanding the above
                  this Annexure (Annexure–P1) shall get automatically terminated
                  and cease to be effective upon termination of the Agreement
                  pursuant to Clause 6 of the Agreement.
                </p>
              </div>
              <div class="section-title">
                <strong>ANNEXURE – S1 Assisted Sales Transactions</strong>
              </div>
              <div className="subCont">
                <div class="section-content">
                  <span>1.</span>
                  <p>Definitions:</p>
                </div>
                <div class="section-content">
                  <span>a.</span>
                  <p>
                    “Consumer” shall mean the Person in whose name RRL shall
                    generate invoice for the Product.
                  </p>
                </div>
                <div class="section-content">
                  <span>b.</span>
                  <p>
                    “Booking” shall mean intention to purchase the Product by
                    the Potential Customer, and making payment for the same
                    on/through Reliance Platform through JioPrime Partner.
                  </p>
                </div>
                <div class="section-content">
                  <span>c.</span>
                  <p>
                    “Booking Confirmation” shall mean the automated confirmation
                    sent to the Consumer by RRL on this Mobile Number, after
                    Booking of the Product.
                  </p>
                </div>
                <div class="section-content">
                  <span>d.</span>
                  <p>
                    “Hardware Device” shall mean device, along with accessories,
                    required and/or used to access the Reliance Platform by
                    JioPrime Partner for carrying out his obligations pursuant
                    to this Annexure, and the technical specifications of the
                    device shall be as communicated by Reliance from time to
                    time.
                  </p>
                </div>
                <div class="section-content">
                  <span>e.</span>
                  <p>
                    “Compensation” shall mean an amount paid by Reliance to
                    JioPrime Partner on completing designated obligations by
                    JioPrime Partner under this Annexure.
                  </p>
                </div>
                <div class="section-content">
                  <span>f.</span>
                  <p>
                    “Fulfilment Obligations” shall mean obligations of JioPrime
                    Partner as detailed in this Annexure and as communicated by
                    Reliance from time to time.
                  </p>
                </div>
                <div class="section-content">
                  <span>g.</span>
                  <p>
                    “Lead Generation” shall mean all of the obligations of
                    JioPrime Partner as detailed in this Annexure and as
                    communicated by Reliance from time to time.
                  </p>
                </div>
                <div class="section-content">
                  <span>h.</span>
                  <p>
                    “Marks” shall mean trademarks, service marks, logos,
                    designs, trade dress and domain names and such other
                    trademarks, service marks, logos, designs and trade dress
                    relating to the development, ownership, operation, promotion
                    and management of the Products which are either owned by
                    and/or licensed to Reliance and/or its Affiliates.
                  </p>
                </div>
                <div class="section-content">
                  <span>i.</span>
                  <p>
                    “Mobile Number” means a working mobile number under the
                    active subscription of the approved network carrier and used
                    by Consumer or Potential Customer for Booking and/or lead
                    capturing by JioPrime Partner.
                  </p>
                </div>
                <div class="section-content">
                  <span>j.</span>
                  <p>
                    “Pin Code” shall mean the six-digit code in the Indian
                    postal code system to uniquely identify a location
                  </p>
                </div>
                <div class="section-content">
                  <span>k.</span>
                  <p>
                    “Potential Customer” shall mean the Person who is approached
                    by JioPrime Partner and who expresses his interest to
                    purchase the Product and who purchases the Product or,
                    on/through Reliance Platform
                  </p>
                </div>
                <div class="section-content">
                  <span>l.</span>
                  <p>
                    “Product” shall mean the product Listed on the Reliance
                    Platform, and as may be amended from time to time.
                  </p>
                </div>
                <div class="section-content">
                  <span>m.</span>
                  <p>
                    “Product Code” shall mean the unique reference to identify a
                    particular Product
                  </p>
                </div>
                <div class="section-content">
                  <span>n.</span>
                  <p>
                    “Proof of Payment” / “Payment confirmation” shall mean an
                    automated communication forwarded by Reliance Platform to
                    the Mobile Number of the Consumer or any other printed
                    receipt issued by JioPrime Partner in the format prescribed
                    by RRL, and to the registered Mobile Number of JioPrime
                    Partner, confirming receipt of payment for the Product
                    booked by the Consumer.
                  </p>
                </div>
                <div class="section-content">
                  <span>o.</span>
                  <p>
                    “Reliance Outlet” shall mean and include stores and outlets
                    of Reliance or its group companies
                  </p>
                </div>
                <div class="section-content">
                  <span>p.</span>
                  <p>
                    “Reliance Platform” shall mean and include online platforms
                    of Reliance and its Affiliates, where interface for Lead
                    Generation, Booking, etc. for the Products to be carried out
                    by JioPrime Partner
                  </p>
                </div>
                <div class="section-content">
                  <span>q.</span>
                  <p>
                    “Service” shall mean the sales and related activities
                    performed by JioPrime Partner pursuant to this Annexure.
                  </p>
                </div>
                <div class="section-content">
                  <span>r.</span>
                  <p>
                    “Ship to Address” shall mean the address where the Product
                    is to be shipped/delivered by Reliance, as per the selection
                    by the Consumer.
                  </p>
                </div>
                <div class="section-content">
                  <span>s.</span>
                  <p>
                    “Outlet” shall mean such location, as designated by
                    Reliance, for pick-up of the Product by Consumer, as per the
                    selection by the Consumer.
                  </p>
                </div>
              </div>
              <div class="section-content">
                <span>2.</span>
                <p>
                  “Infrastructure Requirements": JioPrime Partner shall ensure
                  availability of Hardware Device(s) for accessing the Reliance
                  Platform as may be communicated by RRL from time to time.
                </p>
              </div>
              <div className="subCont">
                <div class="section-content">
                  <span>3.</span>
                  <p>Data Privacy & Protection:</p>
                </div>
                <div class="section-content">
                  <span>a.</span>
                  <p>
                    JioPrime Partner shall comply with relevant security
                    standards/ regulations/guidelines, system requirements
                    including software and hardware as may be applicable to the
                    conduct of the business contemplated under this Agreement,
                    and for protection, confidentiality requirements with
                    respect to Consumer information, transaction data, and
                    ensure that there are proper encryption and security
                    measures at its Hardware Device(s), including software,
                    application, etc. used in the Hardware Device(s). JioPrime
                    Partner shall comply with its statutory obligations as
                    regards data security compliances including but not limited
                    to the Information Technology Act, 2000, and Rules made
                    thereunder (as amended from time to time). JioPrime Partner
                    shall comply with any and all data protection, privacy or
                    similar laws of India (“Data Protection Laws”), that apply
                    in relation to any personal (including sensitive personal
                    information) data, delivered, stored or processed while
                    dealing with the Consumer in connection with such
                    information or data (“Protected Data”), and render such
                    assistance and co-operation as is reasonably necessary or
                    reasonably requested by RRL. JioPrime Partner shall be
                    obligated to deliver all Protected Data to RRL and shall not
                    retain or use the Protected Data for any purposes
                    whatsoever, save and except to the extent that such
                    Protected Data or part thereof is necessary for the
                    rendering of the Services contemplated herein. JioPrime
                    Partner shall strictly comply with all data protection
                    requirements, which shall be as per the Applicable Laws,
                    directions issued by, GoI, etc., and shall be complied by
                    the JioPrime Partner to facilitate/process the Products
                    and/or Services to Consumers. JioPrime Partner shall ensure
                    confidentiality of Consumer data, Protected Data, and shall
                    not engage in any actions which may infringe the privacy of
                    the Consumers.
                  </p>
                </div>
                <div class="section-content">
                  <span>b.</span>
                  <p>
                    JioPrime Partner shall, at its own expense, defend any claim
                    arising on account of violation by JioPrime Partner of any
                    Protected Data of any third party, RRL, its Affiliates or
                    their respective directors, employees, representatives, etc.
                    and RRL shall be kept indemnified in this regard.
                  </p>
                </div>
              </div>
              <div className="subCont">
                <div class="section-content">
                  <span>4.</span>
                  <p>Lead:</p>
                </div>
                <div class="section-content">
                  <span>a.</span>
                  <p>
                    Approach Potential Consumer(s): JioPrime Partner shall
                    approach Potential Customers for the purpose of sourcing
                    business for RRL. JioPrime Partner shall understand or
                    visualize the requirements of the Potential Customer, for
                    the Product.
                  </p>
                </div>
                <div class="section-content">
                  <span>b.</span>
                  <p>
                    Convince Potential Customer to buy Product: JioPrime
                    Partner, subject to provisions of Clause 11 (Representations
                    and Warranties), shall explain the details, specifications,
                    features, etc. of the Product, to the Potential Customer
                  </p>
                </div>
                <div
                  class="fDc section-content"
                  
                >
                  <span>
                    c. Ensure Booking: JioPrime Partner shall ensure the closure
                    of the Booking and completion of the Transaction, by
                    enabling the payment for the Product in favour of RRL.
                    JioPrime Partner shall capture, including but not limited to
                    following information :
                  </span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>i. Correct postal Pin Code.</p>
                    <p>
                      ii. Consumer details such as name, contact number, Email
                      ID, and delivery address & billing address, GST number(if
                      Consumer wants to claim GST benefits).
                    </p>
                  </div>
                </div>
                <div class="section-content">
                  <span>d.</span>
                  <p>
                    Lead Capturing: For Potential Customer(s) who express its
                    interest to purchase Product(s), JioPrime Partner shall
                    capture details of such Potential Customer(s) on Reliance
                    Platform. Reliance will, directly or through its channel,
                    approach the Potential Customer, to take the lead forward
                    and try to convert the same into Booking and completion of
                    the Transaction. JioPrime Partner will be able to check the
                    status of leads captured by JioPrime Partner, on the
                    Reliance Platform, and shall co-ordinate & co-operate with
                    Reliance in completion of the Transaction. The details of
                    such captured leads shall be the property of Reliance, and
                    the JioPrime Partner shall not, directly or indirectly,
                    share this information for his personal or any third party’s
                    gain or profit or commercial exploitation.
                  </p>
                </div>
              </div>

              <div className="subCont">
                <div class="section-content">
                  <span>5.</span>
                  <p>
                    Fulfilment Obligations: In the event the Consumer wishes to
                    book Product(s) from JioPrime Partner, then the JioPrime
                    Partner shall adopt following steps:
                  </p>
                </div>
                <div
                  class="fDc section-content"
                  
                >
                  <span>a. Finalization of the Product</span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>
                      i. JioPrime Partner shall select and reconfirm with
                      Consumer the exact Product Code, .
                    </p>
                    <p>
                      ii. JioPrime Partner shall find out the availability
                      status of the Product and tentative delivery date.
                    </p>
                    <p>
                      iii. JioPrime Partner shall take best efforts for
                      completion of transaction by the Consumer.{" "}
                    </p>
                  </div>
                </div>
                <div
                  class="fDc section-content"
                  
                >
                  <span>b. Payment</span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>
                      i. JioPrime Partner shall accept the payment with the help
                      of any of the payment option, such as, Debit Card, Credit
                      Card, UPI, Net banking, JioMoney, Wallet, etc. and such
                      other digital mode of payment as may be activated by
                      Reliance from time to time.
                    </p>
                    <p>
                      ii. JioPrime Partner shall, either, ask the Consumer to
                      process the payment on ‘Pay via Link’ option on the
                      Reliance Platform to send link to Consumer
                    </p>
                    <p>
                      iii. Consumer will make payment on “Pay via Link”, and if
                      he wants he can avail EMI , if available for the
                      transaction
                    </p>
                    <p>
                      iv. It shall be responsibility of the JioPrime Partner to
                      ensure that the method used by Consumer to make the
                      payment is genuine, authorised and secured.
                    </p>
                  </div>
                </div>
                <div
                  class="fDc section-content"
                  
                >
                  <span>c. Delivery:</span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>
                      i. Delivery by RRL or JioPrime Partner to Ship to Address:
                      JioPrime Partner shall, wherever possible, suggest to the
                      Consumer, the slot(s) for delivery of the Product(s), as
                      available on the Reliance Platform, and record the choice
                      of the Consumer for delivery date and slot.
                    </p>
                    <p>
                      ii. Self-Pick up by Consumer from RRL / JioPrime Partner:
                      JioPrime Partner shall suggest to the Consumer option of
                      Self-Pick up of the Product(s) from RRL / JioPrime
                      Partner, and if accepted by the Consumer, then record the
                      choice of the Consumer for delivery. RRL / JioPrime
                      Partner shall, by separate communication, intimate to the
                      Consumer about the readiness of the Product for pick-up
                      from RRL or JioPrime Partner, as the case may be.
                    </p>
                  </div>
                </div>
                <div
                  class="fDc section-content"
                  
                >
                  <span>
                    d. Outlet pick-up facilitation & process adherence:
                  </span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>
                      i. JioPrime Partner shall create dedicated space for
                      storage of items delivered at the Store to be handed over
                      to the Consumer.
                    </p>
                    <p>
                      ii. Ensure delivery of the Product which has been made
                      available / delivered by RRL to JioPrime Partner. JioPrime
                      Partner shall also ensure that, there’s no inter-exchange
                      for deliveries of same/similar Product/orders.
                    </p>
                    <p>
                      iii. Complete the handover process through One Time
                      Password (OTP) based confirmation or as per the defined
                      process.
                    </p>
                    <p>
                      iv. Certain Products, which shall include but not limited
                      to the Products requiring installation, bulky & heavy
                      Products, etc., shall qualify only for delivery at Ship to
                      Address.
                    </p>
                  </div>
                </div>
                <div
                  class="fDc section-content"
                  
                >
                  <span>e. After-Sales:</span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>
                      i. Support the Consumer by addressing their queries
                      related to the purchases made such as order status,
                      payment update, delivery status, order cancellations and
                      order returns/replacement status in the “MyOrders” section
                      of the Jio Mart Site at www.jiomart.com
                    </p>
                    <p>
                      ii. However, in case JioPrime Partner is not able to
                      satisfactorily close the Consumer query, JioPrime Partner
                      will advise the Consumer to reach out to the Consumer
                      Contact Center of RRL at (Phone No.: 1800 890 1222 from
                      08:00 AM to 08:00 PM, and/or Email ID: cs@jiomart.com)
                    </p>
                    <p>
                      iii. Additionally, JioPrime Partner will also guide
                      Consumer to avail WhatsApp based chatbot or any other
                      specified platforms provided by RRL which will help answer
                      any queries related to the Consumer's recent purchases.
                    </p>
                  </div>
                </div>
              </div>
              <div className="subCont">
                <div
                  class="section-content fDc"
                  //
                >
                  <span>6. Marketing related Obligations of JioPrime Partner:JioPrime
                    Partner shall be responsible for, including but not limited
                    to, following obligations:</span>
                 
                </div>
                <div class="section-content">
                  <span>a.</span>
                  <p>
                    Deploy the required marketing and promotion materials, as
                    provided by RRL and/or its behalf, as per the guidelines
                    approved and communicated by RRL from time to time
                  </p>
                </div>
                <div class="section-content">
                  <span>b.</span>
                  <p>
                    Use the Marks exactly as they appear in the marketing and
                    promotional materials and programs prepared and provided by
                    RRL
                  </p>
                </div>
                <div class="section-content">
                  <span>c.</span>
                  <p>
                    Use the Marks on business checks, cards, stationery,
                    interior décor, and promotional items post approval
                  </p>
                </div>
                <div class="section-content">
                  <span>d.</span>
                  <p>
                    Promptly discontinue any use of the POS material, promotion
                    scheme related communication material or product brochures /
                    leaflets, if determined improper by RRL
                  </p>
                </div>
                <div class="section-content">
                  <span>e.</span>
                  <p>
                    Publish a variety of sales aid and marketing materials
                    specifically designed to promote business at the store, as
                    provided by RRL. This may include elements like permanent
                    and temporary signages, proprietary software and hardware,
                    demo and/or dummy devices, etc.
                  </p>
                </div>
                <div class="section-content">
                  <span>f.</span>
                  <p>
                    JioPrime Partner shall not use marketing assets provided by
                    RRL, at any place(s) other than the authorised Store. All
                    such marketing assets are only for the use at authorised
                    Store.
                  </p>
                </div>
                <div class="section-content">
                  <span>g.</span>
                  <p>
                    JioPrime Partner shall ensure upkeep and maintenance of all
                    such assets provided for promotion and sales enablement.
                  </p>
                </div>
                <div class="section-content">
                  <span>h.</span>
                  <p>
                    JioPrime Partner shall note that, regular audits will be
                    conducted by RRL to review the status of these assets and
                    undertake action if default found.
                  </p>
                </div>
              </div>
              <div className="subCont">
                <div class="section-content">
                  <span>7. Projection:</span>
                </div>
                <div
                  class="fDc section-content"
                  
                >
                  <span>a.Target Achievement</span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>
                      i. Target (overall/category/brand) for the monthly or
                      period as shared will be communicated at the start of the
                      month
                    </p>
                    <p>
                      ii. Applicable pay-out for the above defined target will
                      be shared as-well
                    </p>
                  </div>
                </div>
                <div
                  class="fDc section-content"
                  
                >
                  <span>b. Marketing Intelligence inputs</span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>
                      i. Provide relevant inputs on the local market trends
                      related to sales, supply chain, after-sales, Consumer
                      issues, etc. from time to time
                    </p>
                  </div>
                </div>
              </div>
              <div className="subCont">
                <div class="section-content">
                  <span>8. Fees, Invoicing and Payment Terms</span>
                </div>             
                <div class="section-content fDc">
                  <span>a. Entitlement parameters</span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>
                      i. JioPrime Partner will be entitled for Compensation for
                      successful Booking of the Product by the Consumer, subject
                      to following conditions: - The Consumer have made complete
                      payment of Product invoice generated by Reliance, with the
                      help of one or more payment options - The Consumer have
                      not cancelled or opted for return of the Product, till
                      completion of applicable return period. - Consumer have
                      taken/accepted the delivery of the Product at Ship to
                      Address or pick-up from Outlet.
                    </p>
                  </div>
                </div>
                <div
                  class="fDc section-content"
                  
                >
                  <span>b. Compensation </span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>
                      i. JioPrime Partner shall be entitled to Compensation on
                      successful consummation of transaction as per above stated
                      Entitlement parameters.
                    </p>
                    <p>
                      ii. The amount to be paid to JioPrime Partner shall be as
                      communicated by Reliance from time to time, and shall
                      differ Product-by Product.{" "}
                    </p>
                  </div>
                </div>
                <div class="section-content">
                  <span>c. Recovery/ Re-imbursement</span>
                  <p>
                    RRL shall be entitled for recovery/ re-imbursement of
                    expenses incurred by RRL on account of, including but not
                    limited to, Payment Gateway Charges. RRL will be entitled to
                    set off the charges against the compensation payable to
                    JioPrime Partner.
                  </p>
                </div>
                <div
                  class="fDc section-content"
                  
                >
                  <span>d. Invoicing </span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>
                      i. As applicable, the JioPrime Partner shall raise tax
                      invoice on Reliance for the payment of Compensation, for
                      the entitlement determined as per point a and b above.
                    </p>
                    <p>
                      ii. The frequency of raising invoice shall be as
                      communicated by Reliance from time to time.
                    </p>
                  </div>
                </div>
                <div
                  class="fDc section-content"
                  
                >
                  <span>e. Payment </span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>
                      i. For the invoices which are raised by JioPrime Partner
                      shall be paid subject to : - The invoice is not disputed
                      by Reliance and/or the dispute pertaining to invoice has
                      been successfully resolved - No payment will be released
                      unless the dispute is resolved - The payment shall be made
                      excluding GST component and once JioPrime Partner produces
                      the proof of GST payment and matching of tax on GSTN
                      portal, Reliance shall make payment of GST component of
                      the invoice. All payments / credits by RRL shall be
                      subject to deduction of tax at source as per applicable
                      provisions of the Income-tax Act and rules made
                      thereunder. RRL shall issue TDS certificate in respect of
                      tax deducted in accordance with the provisions of the
                      Income-tax Act, and Income-tax Rules.
                    </p>
                  </div>
                </div>
              </div>
              <div className="subCont">
                <div
                  class="fDc section-content"
                  
                >
                  <span>9. Cancellation </span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p> i Cancellation by Consumer: 
                      A Consumer is permitted to fully or partially cancel order
                      prior to the said order being invoiced by RRL. Once the
                      Product is invoiced then the Consumer will not be in
                      position to cancel the order. For the eligible cancelation
                      of the order, the Consumer can ask the store to cancel the
                      order via the jiomart.com.
                    </p>
                  </div>
                </div>
                <div
                  class="fDc section-content"
                  
                >
                  <span>ii. Cancellation by RRL</span>

                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>
                      RRL may of its own reserve the rights to cancel any order
                      for the reasons stated below: - Inadequate inventory -
                      Inaccuracies or errors in product or pricing information -
                      For any other reasons at the discretion of RRL. The
                      payment charged for any Products that are cancelled by RRL
                      shall be refunded into the Consumer account using the same
                      payment channel used by the Consumer as provided in this
                      section 10.
                    </p>
                  </div>
                </div>
                <div class="section-content">
                  <span>b. Return</span>
                  <p>
                    As per respective Product Policy, as communicated from time
                    to time or as made available on applicable website.
                  </p>
                </div>
                <div
                  class="fDc section-content"
                  
                >
                  <span>c. Refund</span>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: "1",
                      marginLeft: "15px",
                    }}
                  >
                    <p>
                      i In all eligible cancellations, RRL shall initiate
                      refunds.
                    </p>
                    <p>
                      ii The refund on any account will reflect in the
                      Consumer’s original Mode of Payment (MOP) used at the time
                      of purchase viz., credit card, debit card, net-banking,
                      ROne Points, account within such reasonable time depending
                      upon the MOP used by the Consumer (subject to the policies
                      of the relevant bank/financial intermediaries/
                      organization/entity (hereinafter called as “MOP
                      Administering Entities”), as the case may be). All refunds
                      shall be subject to applicable charges as may be charged &
                      deducted by RRL and or the MOP Administering Entities. In
                      case the Consumer has made Cash payment then, the refund,
                      after deduction, if any as stated above, will be credited
                      to the Consumer’s bank account of which the Consumer has
                      provided the details.
                    </p>
                  </div>
                </div>
                <div class="section-content">
                  <span>d. Non-Delivery / Delay in delivery of Product</span>
                  <p>As communicated from time to time.</p>
                </div>
              </div>
              <div className="subCont">
                <span>10.  Flow of financial assistance, in case of consumer finance:</span>
                <div className="section-content">
                  <p>
                    a. Consumer avails financial assistance under any of the
                    finance scheme of the Consumer Finance Company, through
                    JioPrime Partner at the time purchasing the Product(s).{" "}
                  </p>
                </div>
                <div className="section-content">
                  <p>
                    b. Consumer Finance Company, within agreed period of time,
                    will transfer to RRL the amount equivalent to financial
                    assistance availed by the Consumer.{" "}
                  </p>
                </div>
                <div className="section-content">
                  <p>
                    c. RRL shall, transfer to JioPrime Partner, the amount
                    equivalent to the financial assistance availed by the
                    Consumer, after deductions/adjustments of receivables from
                    JioPrime Partner.{" "}
                  </p>
                </div>
                <div className="section-content">
                  <p>
                    d. Payment received by RRL from Consumer Finance Company
                    shall be considered by JioPrime Partner as if the payment is
                    received by JioPrime Partner for the relevant Product
                    purchased by the Consumer, and accordingly JioPrime Partner
                    shall deliver the relevant Product(s) to the Consumer.
                  </p>
                </div>
              </div>
              <div className="subCont">
                <span>11. Consumer Finance:</span>
                <div className="section-content">
                  <p>
                    a. RRL and/or Consumer Finance Company shall, from time to
                    time, announce the consumer finance schemes for the
                    Consumers.
                  </p>
                </div>
                <div className="section-content">
                  <p>
                    b. JioPrime Partner shall communicate all such consumer
                    finance schemes to the Consumer(s).
                  </p>
                </div>
                <div className="section-content">
                  <p>
                    c. JioPrime Partner shall collect all such supporting
                    documents as may be required as per RBI guidelines, and as
                    communicated by RRL and/or Consumer Finance Company from
                    time to time (hereinafter collectively referred to as
                    “Documents”).
                  </p>
                </div>
                <div className="section-content">
                  <p>
                    d. JioPrime Partner shall collect the Documents in diligent
                    manner and wherever applicable, check the Documents with
                    original and certify the same. In the event of any
                    negligence on the part of JioPrime Partner, the same shall
                    be treated as one of the reasons for indemnification by
                    JioPrime Partner to RRL and/or Consumer Finance Company.{" "}
                  </p>
                </div>
                <div className="section-content">
                  <p>
                    e. JioPrime Partner will facilitate reporting and resolution
                    of Consumer complaints relating to the finance facility to
                    RRL and / or Consumer Finance Company.
                  </p>
                </div>
                <div className="section-content">
                  <p>
                    f. JioPrime Partner hereby agrees to be engaged as a
                    repayment channel agent for RRL and / or Consumer Finance
                    Company{" "}
                  </p>
                </div>
                <div className="section-content">
                  <p>
                    g. JioPrime Partner to provide all required assistance to
                    RRL and / or Consumer Finance Company for them to take
                    action against the Consumer for the default committed by
                    such Consumer.{" "}
                  </p>
                </div>
                <div className="section-content">
                  <p>
                    h. JioPrime Partner shall not issue any written
                    communication(s), make unsolicited calls, make commitments
                    or negotiate terms of the finance scheme on behalf of RRL
                    and / or Consumer Finance Company. JioPrime Partner shall
                    strictly adhere to all privacy laws including Information
                    Technology (Reasonable Security Practices and Procedures and
                    Sensitive Data or Information) Rules, 2011.
                  </p>
                </div>
              </div>
              <div className="subCont">
                <span>12. "After Sales Support":</span>
                <div className="section-content">
                  <p>
                    After Sales Support shall be as per Vendor’s after sales
                    service & support policy as may be applicable for each
                    Product.
                  </p>
                </div>
              </div>
              <div className="subCont">
                <span>13. Reporting:</span>
                <div className="section-content">
                  <p>
                    JioPrime Partner shall prepare and maintain, and submit to
                    RRL on a timely basis, all documentation and reports as may
                    be requested by RRL from time to time to be prepared,
                    maintained or submitted in a prescribed format as may be
                    communicated to JioPrime Partner from time to time.
                  </p>
                </div>
              </div>
              <div className="subCont">
                <span> 14. Representations and Warranties   JioPrime Partner represents and warrants, that JioPrime
                  Partner:</span>
                <div className="section-content">
                  <p>
                    a. Shall provide exact Product information and specification
                    to the Potential Customer and to the Consumer
                  </p>
                  </div>
                  <div className="section-content">
                  <p>
                    b. Not to make any claim(s) about the features,
                    specification, functionality, warranty, etc. of the Products
                    without being approved by RRL and/or relevant brand
                  </p>
                  </div>
                  <div className="section-content">
                  <p>
                    c. Shall use Reliance Platform only, for the purpose of (i)
                    explaining about the Product information and specification
                    to the Potential Customer and the Consumer, (ii) taking
                    Booking for the Product, (iii) making payment for the
                    Product, (iv) selecting delivery time/slot/location, etc.
                  </p>
                  </div>
                  <div className="section-content">
                  <p>
                    d. Shall comply with Applicable Laws and guidelines with
                    respect of Store, storage, delivery, etc. of Product.{" "}
                  </p>
                  </div>
                  <div className="section-content">
                  <p>
                    e. Shall not open the Product packing under any
                    circumstances
                  </p>
                  </div>
                  <div className="section-content">
                  <p>
                    f. Shall not announce any schemes, promotion, offers, etc.
                    without prior consent from RRL.
                  </p>
                  </div>
                  <div className="section-content">
                  <p>
                    g. Shall pass on all benefits, such as freebies, vouchers,
                    promotional material, gifts, etc., to the Consumer.
                  </p>
                  </div>
                  <div className="section-content">
                  <p>
                    h. Shall not accept or receive any Product for any post-sale
                    activity related to the Product.
                  </p>
                  </div>
                  <div className="section-content">
                  <p>
                    i. Shall not tamper with the Product packing, shall not
                    cover any statutory information, or alter any details on the
                    Product packing.
                  </p>
                  </div>
                  <div className="section-content">
                  <p>
                    j. Shall not make any claims or representations for and/or
                    on behalf of RRL and/or in respect of the Product.
                  </p>
                </div>
              </div>
              <div className="section-content">
                <span>15.</span>
                <p>Security Deposit: As communicated by Reliance from time to time</p>
              </div>
              <div className="section-content">
                <span>16.</span>
                <p>Territory: As defined from time to time</p>
              </div>
              <div className="section-content">
                <span>17.</span>
                <p>Term: This Annexure shall be valid from execution till the date of
                    termination</p>
              </div>
              <div className="section-content">
                <span>18.</span>
                <p>Termination of Annexure: RRL shall be entitled to terminate this Annexure without any
                    cause during the Term of the Annexure after serving a
                    written notice of termination to the JioPrime Partner.
                    Notwithstanding the above, this Annexure shall get
                    automatically terminated and cease to be effective upon
                    termination of the Agreement.</p>
              </div>
            </Grid>
          </Grid>
        </Container>
      </div>
    </>
  );
};

export default TermsAndCondtion;
