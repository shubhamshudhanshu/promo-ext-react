import React from "react";
import BasicButton from "../../../components/CustomButton";
import Link from "../../../components/CustomLink";
import Text from "../../../components/CustomText";
import "./style.scss";

function Footer({
  btnLabel,
  submitDisabled,
  showSkip,
  showSubtitle,
  btnLoading,
}) {
  return (
    <div className="footer-container">
      <BasicButton
        fullWidth
        type="submit"
        label={btnLabel}
        disabled={submitDisabled}
        loading={btnLoading}
      />
      {showSkip && (
        <div className="skip">
          <Link textAppearance="body-s" title="Skip" href="/onboarding/form" />
        </div>
      )}
      {showSubtitle && (
        <Text className="terms" appearance="body-xxs" color="primary-grey-80">
          By continuning, you agree to our{" "}
          <Link
            style={{ padding: "0 5px" }}
            href="/onboarding/termsAndConditions"
            newTab
            onClick={function noRefCheck() {}}
            textAppearance="body-xxs"
            title="Terms of Service"
          />
        </Text>
      )}
    </div>
  );
}

export default Footer;
