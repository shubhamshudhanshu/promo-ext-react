/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useSearchParams } from "react-router-dom";
import { retailerActions } from "../../../redux/slices/retailer";
import { retailerMobileActions } from "../../../redux/slices/retailerMobile";
import { goNext } from "../../../redux/slices/step";
import OTPLayout from "../otpLayout";

function VerifyOtp() {
  const [searchParams] = useSearchParams();
  const otpType = searchParams.get("otptype");
  let navigate = useNavigate();
  const dispatch = useDispatch();
  const { mobile, isMobileVerified, otp, otpError, resendTimer } = useSelector(
    (state) => state.retailerMobile
  );

  const { verifyingAdhaar } = useSelector((state) => state.retailerData);
  const { submittingOtp } = useSelector((state) => state.retailerMobile);

  const { showOtp } = useSelector((state) => state.stepSlice);

  const isMobileOtp = otpType === "mobile" ? true : false;
  const otpLength = isMobileOtp ? 4 : 6;

  useEffect(() => {
    return () => {
      dispatch(retailerMobileActions.onChangeOtp(""));
      dispatch(retailerMobileActions.setOtpError(""));
    };
  }, []);

  useEffect(() => {
    if (!showOtp) {
      if (isMobileOtp) navigate("/onboarding/enter-mobile");
      else navigate("/onboarding/form");
    }
  }, [showOtp, isMobileOtp]);

  const stepState = useSelector((state) => state.stepSlice);

  const submitMobile = () => {
    dispatch(retailerMobileActions.submitMobile())
      .unwrap()
      .then(() => {
        navigate(`/onboarding/enter-otp?otptype=mobile`);
      });
  };

  const verifyMobileOTP = () => {
    dispatch(retailerMobileActions.submitOtp())
      .unwrap()
      .then((res) => {
        dispatch(retailerActions.updateSellerData(res.data));
        navigate("/onboarding/enter-gstin");
      });
  };

  const submitAadhar = () => {
    dispatch(
      retailerActions.sendAdhaarOtp({
        aadhaarNo: stepState.enteredAdhaarNumber,
      })
    )
      .unwrap()
      .then((res) => {
        navigate(`/onboarding/enter-otp?otptype=adhaar`);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const verifyAadharOtp = () => {
    const body = {
      aadhaarNo: stepState.enteredAdhaarNumber,
      otp,
    };

    dispatch(retailerActions.verifyAdhaarOtp(body))
      .unwrap()
      .then((res) => {
        if (res.data) {
          dispatch(retailerActions.updateSellerData(res.data));
          dispatch(goNext());
          sessionStorage.removeItem("adh_no");
          navigate("/onboarding/form");
        }
      });
  };

  useEffect(() => {
    if (otpType === "mobile" && isMobileVerified) {
      navigate(`/onboarding/enter-mobile`);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [otpType, isMobileVerified]);

  const handler = useCallback(
    (e) => {
      if (e.keyCode === 13) {
        e.preventDefault();
        if (otp.length < otpLength) {
          return;
        }
        if (isMobileOtp) {
          verifyMobileOTP();
        } else {
          verifyAadharOtp();
        }
      }
    },
    [otp]
  );

  useEffect(() => {
    document.addEventListener("keydown", handler);
    return () => {
      console.log("remove event listener");
      document.removeEventListener("keydown", handler);
    };
  }, [handler]);
  return (
    <OTPLayout
      title="Verify OTP"
      subtitle={
        otpType === "adhaar"
          ? `OTP has been sent to Registered Aadhaar Number`
          : ` OTP has been sent to ${mobile}`
      }
      footerProps={{
        btnLabel: "Verify OTP",
        btnLoading: isMobileOtp ? submittingOtp : verifyingAdhaar,
        btnDisabled: otp.length < otpLength,
      }}
      inputProps={{
        value: otp,
        pattern: isMobileOtp ? /\b\d{4}\b/g : /\b\d{6}\b/g,
        handleChange: (val) => {
          dispatch(retailerMobileActions.onChangeOtp(val));
        },
        setError: (value) => dispatch(retailerMobileActions.setOtpError(value)),
        error: otpError,
        length: otpLength,
        resendTimer,
      }}
      showEditNumber={isMobileOtp}
      resendOtp={() => {
        dispatch(retailerMobileActions.onChangeOtp(""));
        dispatch(retailerMobileActions.setOtpError(""));
        if (isMobileOtp) {
          submitMobile();
        } else {
          submitAadhar();
        }
      }}
      next={() => {
        if (isMobileOtp) {
          verifyMobileOTP();
        } else {
          verifyAadharOtp();
        }
      }}
    />
  );
}

export default VerifyOtp;
