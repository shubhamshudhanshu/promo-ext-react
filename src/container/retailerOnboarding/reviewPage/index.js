import React from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import BasicButton from "../../../components/CustomButton";
import Text from "../../../components/CustomText";
import back from "./../../../assets/images/onboarding/homepage-desktop-images/back.png";
import "./style.scss";

function ReviewPage() {
  const info = useSelector(
    (state) => state.retailerData.retailerData.data || {}
  );
  const navigate = useNavigate();

  console.log(info);

  const {
    storeName = "",
    blockNumber = "",
    buildingName = "",
    streetName = "",
    city = "",
    pincode = "",
    state = "",
  } = info?.addresses[0] || {};
  return (
    <div className="wrapper-detail">
      <div className="Page">
        <div className="details">
          <div className="heading-card">
            <Text appearance="body-xs-bold" color="primary-grey-80">
              Personal Details
            </Text>
          </div>
          <div className="bank">
            <div className="bankdetails">
              <Text appearance="heading-xs" color="primary-grey-100">
                Name
              </Text>
              <div>
                <Text appearance="heading-xs" color="primary-grey-80">
                  {info.sellerName}
                </Text>
              </div>
            </div>

            <div className="bankdetails">
              <Text appearance="heading-xs" color="primary-grey-100">
                PAN Number
              </Text>
              <div>
                <Text appearance="heading-xs" color="primary-grey-80">
                  {info.panDetail?.pan || "-"}
                </Text>
              </div>
            </div>

            <div className="bankdetails">
              <Text appearance="heading-xs" color="primary-grey-100">
                Aadhar
              </Text>
              <div>
                <Text appearance="heading-xs" color="primary-grey-80">
                  {info.aadharDetail?.aadhar || "-"}
                </Text>
              </div>
            </div>
          </div>

          <div className="heading-card">
            <Text appearance="body-xs-bold" color="primary-grey-80">
              Registered Address
            </Text>
          </div>
          <div className="address">
            <div
              style={{
                marginLeft: "10px",
              }}
            >
              <Text appearance="body-xs-bold" color="primary-grey-100">
                {storeName}
              </Text>
            </div>
            <div style={{ marginLeft: "10px" }}>
              <Text appearance="body-xs" color="primary-grey-80">
                {buildingName && `${buildingName},`}
                {blockNumber && ` ${blockNumber},`}
                {streetName && ` ${streetName},`}
                {city}-{pincode}, {state} <br />
                Mobile:{info.mobileNumber || "-"}
              </Text>
            </div>
          </div>
          <div className="heading-card">
            <Text appearance="body-xs-bold" color="primary-grey-80">
              Bank Details
            </Text>
          </div>
          <div className="bank">
            <div className="bankdetails">
              <Text appearance="heading-xs" color="primary-grey-100">
                Account Number
              </Text>
              <div id="dets">
                <Text appearance="heading-xs" color="primary-grey-80">
                  {info.bankDetail.accountNumber}
                </Text>
              </div>
            </div>
            <div className="bankdetails">
              <Text appearance="heading-xs" color="primary-grey-100">
                Account Name
              </Text>
              <div id="dets">
                <Text appearance="heading-xs" color="primary-grey-80">
                  {info.bankDetail.accountName}
                </Text>
              </div>
            </div>

            <div className="bankdetails">
              <Text appearance="heading-xs" color="primary-grey-100">
                Bank Name
              </Text>
              <div id="dets">
                <Text appearance="heading-xs" color="primary-grey-80">
                  {info.bankDetail.bankName}{" "}
                </Text>
              </div>
            </div>

            <div className="bankdetails">
              <Text appearance="heading-xs" color="primary-grey-100">
                IFSC Code
              </Text>
              <div id="dets">
                <Text appearance="heading-xs" color="primary-grey-80">
                  {info.bankDetail.ifsc}
                </Text>
              </div>
            </div>
          </div>

          <div className="bank">
            <div className="bankdetails">
              <Text appearance="heading-xs" color="primary-grey-100">
                Table Device
              </Text>
              <div id="dets">
                <Text appearance="heading-xs" color="primary-grey-80">
                  {info.tabletDevice ? "Required" : "NA"}
                </Text>
              </div>
            </div>
          </div>
          <div className="bank">
            <div className="bankdetails">
              <Text appearance="heading-xs" color="primary-grey-100">
                Whether your total turnover during preceding FY (FY 2020-2021)
                is ₹10Cr?
              </Text>

              <div id="dets">
                <Text appearance="heading-xs" color="primary-grey-80">
                  {info.totalTurnover10Crore ? "Yes" : "No"}
                </Text>
              </div>
            </div>
            <div className="bankdetails">
              <Text appearance="heading-xs" color="primary-grey-100">
                Whether TDS U/S 194Q is applicable on your purchases from RRL
                (all Business) and TDS will be deducted from invoice value
                payable to RRL?
              </Text>

              <div id="dets">
                <Text appearance="heading-xs" color="primary-grey-80">
                  {info.tds194Q ? "Yes" : "No"}
                </Text>
              </div>
            </div>
          </div>
          <div style={{ marginTop: "25px" }}>
            <Text appearance="heading-xxs" color="primary-grey-80">
              By continuning, you agree to our{" "}
              <span style={{ color: "#3535f3" }}> Terms of Service</span> and
              <span style={{ color: "#3535f3" }}> Privacy & Legal Policy</span>
            </Text>
          </div>
          <div className="review-footer">
            <div>
              <BasicButton
                kind="secondary"
                onClick={() => {
                  navigate("/onboarding/form");
                }}
              >
                <img
                  style={{ height: "20px", width: "20px" }}
                  src={back}
                  alt="back"
                />
              </BasicButton>
            </div>
            <div>
              <BasicButton
                fullWidth
                style={{ width: "230px" }}
                label={"Complete"}
                onClick={() => {
                  navigate("/onboarding/fetching");
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ReviewPage;
