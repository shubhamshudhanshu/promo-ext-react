import React, { useEffect } from "react";
import "./style.scss";
import { useNavigate } from "react-router-dom";
import Text from "../../../components/CustomText";
import { CircularProgress } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { retailerActions } from "../../../redux/slices/retailer";
function Fetching({ title, subtitle, next }) {
  let navigate = useNavigate();

  const state = useSelector((state) => state.retailerData.retailerData);
  const bank = state.data.bankDetail;
  let dispatch = useDispatch();

  useEffect(() => {
    if (bank.bankDetailId) {
      dispatch(retailerActions.verifyBankDetails(bank))
        .unwrap()
        .then(() => {
          navigate("/onboarding/success");
        })
        .catch(() => {
          navigate("/onboarding/form");
        });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [bank]);

  return (
    <div className="layout-container">
      <div className="fetching-margin">
        <div
          style={{
            display: "inline-block",
            marginBottom: "20px",
          }}
        >
          <CircularProgress />
        </div>
        <div>
          <Text appearance="heading-m" color="primary-grey-100">
            Please wait a moment.
          </Text>
          <Text
            className="subtitle"
            appearance="body-m"
            color="primary-grey-80"
          >
            We’re verifying your bank account details...
          </Text>
        </div>
      </div>
    </div>
  );
}

export default Fetching;
