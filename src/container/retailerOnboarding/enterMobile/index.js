import React from "react";
import Text from "../../../components/CustomText";
import MainLayout from "../mainLayout";
import { useSelector, useDispatch } from "react-redux";
import {
  getError,
  getMobile,
  retailerMobileActions,
} from "../../../redux/slices/retailerMobile";
import { useNavigate } from "react-router-dom";
import verifiedIcon from "./../../../assets/images/onboarding/Icon-verified.svg";

function EnterMobile() {
  const dispatch = useDispatch();
  let navigate = useNavigate();

  const submitMobile = () => {
    dispatch(retailerMobileActions.submitMobile())
      .unwrap()
      .then(() => {
        navigate(`/onboarding/enter-otp?otptype=mobile`);
      });
  };
  const mobile = useSelector(getMobile);
  const error = useSelector(getError);
  const { isMobileVerified, submittingMobile } = useSelector(
    (state) => state.retailerMobile
  );

  return (
    <MainLayout
      title="Enter Mobile Number"
      subtitle="You’ll receive a 4 digit code to verify next"
      footerProps={{
        btnLabel: isMobileVerified ? "Next" : "Get OTP",
        btnLoading: submittingMobile,
      }}
      inputProps={{
        name: "Mobile number",
        label: "Enter your mobile number",
        type: "text",
        mask: "##########",
        value: mobile,
        error: error,
        setError: (value) => dispatch(retailerMobileActions.setError(value)),
        handleChange: (e) =>
          dispatch(retailerMobileActions.onChangeMobile(e.target.value)),
        required: true,
        prefix: (
          <Text appearance="body-m" color="primary-grey-80">
            +91
          </Text>
        ),
        fullWidth: true,
        readOnly: isMobileVerified,
        suffix: isMobileVerified && <img src={verifiedIcon} alt="" />,
      }}
      next={(val) => {
        if (isMobileVerified) {
          navigate(`/onboarding/enter-gstin`);
        } else submitMobile(val);
      }}
    />
  );
}

export default EnterMobile;
