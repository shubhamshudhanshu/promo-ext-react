import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import Header from "./header/header";
import RetailerDetailsForm from "./RetailerDetailsForm";
import "./style.scss";
import HomePage from "./homePage";
import EnterMobile from "./enterMobile";
import DesktopLayout from "./desktopLayout";
import EnterGst from "./enterGst";
import VerifyOtp from "./verifyOtp";
import VerifyAadhar from "./verifyAdhaar";
import { useSelector } from "react-redux";
import EditAddress from "./editAddress";
import ReviewPage from "./reviewPage";
import Fetching from "./fetching";
import Success from "./success";
import Loader from "../../components/Loading";
import withProgress from "../../helper/hocs/withProgress";
// import { dummyData } from "./../../container/admin/retailerList.js/dummy";

function Pages() {
  const fetchingRetailerData = useSelector(
    (state) => state.retailerData.fetchingRetailerData
  );

  return (
    <div style={{ position: "relative" }}>
      <Header />
      <div className="home-page-container">{<DesktopLayout />}</div>
      <div className="pages-container">
        {fetchingRetailerData && <Loader />}
        <Routes>
          <Route path="/" element={<Navigate to="/onboarding/home" />} />
          <Route path="/home" element={<HomePage />} />
          <Route exact path="/enter-mobile" element={<EnterMobile />} />
          <Route exact path="/edit-address" element={<EditAddress />} />
          {/* <Route exact path="/confirm-location" element={<ConfirmLocation />} /> */}
          <Route exact path="/review" element={<ReviewPage />} />
          <Route exact path="/fetching" element={<Fetching />} />
          <Route exact path="/success" element={<Success />} />
          <Route path="/enter-gstin" exact element={<EnterGst />} />
          <Route path="/enter-otp" exact element={<VerifyOtp />} />
          <Route path="/verify-adhaar" exact element={<VerifyAadhar />} />
          <Route path="/form" exact element={<RetailerDetailsForm />} />
          {/* <Route path="*" element={<Navigate to="/onboarding/home" />} /> */}
        </Routes>
      </div>
    </div>
  );
}

export default withProgress(Pages);
