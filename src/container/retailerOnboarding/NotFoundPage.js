import React from 'react';
import { Link } from 'react-router-dom';
import "./style.scss"
// import PageNotFound from '../assets/images/PageNotFound';
class NotFoundPage extends React.Component {
    render() {
        return <div className=''>
            {/* <img src={PageNotFound} /> */}
            <p className='notfound' style={{ textAlign: "center" }}>
                {/* <Link to="/onboarding">Go to Home </Link> */}

                Page Not Found
            </p>
        </div>;
    }
}
export default NotFoundPage;