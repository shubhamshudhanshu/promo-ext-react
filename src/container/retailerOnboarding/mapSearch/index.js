import React, { useEffect, useState } from "react";
import { Loader } from "@googlemaps/js-api-loader";
import "./style.scss";
import { useDispatch } from "react-redux";
import { retailerActions } from "../../../redux/slices/retailer";
import MyLocationIcon from "@mui/icons-material/MyLocation";
import Tooltip from "@mui/material/Tooltip";
import { NotificationActions } from "../../../redux/slices/notification";

const loader = new Loader({
  apiKey: "AIzaSyAVCJQAKy6UfgFqZUNABAuGQp2BkGLhAgI",
  version: "weekly",
  libraries: ["places"],
});

const autocompleteOptions = {
  fields: ["formatted_address", "address_components", "geometry", "name"],
  strictBounds: false,
  types: ["establishment"],
};

function MapSearchBox() {
  const [formattedAddress, setFormattedAddress] = useState("");
  const dispatch = useDispatch();
  useEffect(() => {
    initMap();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const initMap = async () => {
    try {
      const google = await loader.load();
      const geocoder = new google.maps.Geocoder();
      // search location input
      const searchLocationInput = document.getElementById("search-location");
      const autocomplete = new google.maps.places.Autocomplete(
        searchLocationInput,
        autocompleteOptions
      );

      onChangeAutocomplete({
        autocomplete,
      });

      //location button
      const locationButton = document.getElementById("my-location");
      locationButton.addEventListener("click", (e) => {
        dispatch(retailerActions.setFetchingCurrentAddress(true));
        onCurrentLocation(geocoder);
      });
      //gecoder
    } catch (err) {
      console.log("first");
    }
  };

  function fillInAddress(place) {
    // const name = place.name;
    setFormattedAddress(place.formatted_address);
    formatAddress(place.address_components);
  }

  const onCurrentLocation = (geocoder) => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        geocode(geocoder, { location: pos });
      });
    }
  };
  function geocode(geocoder, request) {
    geocoder
      .geocode(request)
      .then((result) => {
        const { results } = result;
        setFormattedAddress(results?.[0]?.formatted_address);
        formatAddress(results?.[0]?.address_components);
        dispatch(retailerActions.setFetchingCurrentAddress(false));
        return results;
      })
      .catch((e) => {
        dispatch(
          NotificationActions.addNotification({
            message: "Not able to fetch current location!",
            type: "error",
          })
        );
        dispatch(retailerActions.setFetchingCurrentAddress(false));
      });
  }

  const formatAddress = (addressComponent) => {
    const address = {
      addressId: "",
      addressType: "NOT_VERIFIED",
      buildingName: "",
      streetName: "",
      floor: "",
      landmark: "",
      house: "",
      pincode: "",
      city: "",
      district: "",
      state: "",
      retailerId: "",
    };
    for (const component of addressComponent) {
      const componentType = component.types[0];
      switch (componentType) {
        case "premise": {
          address.streetName = component.long_name ? component.long_name : "";
          break;
        }
        case "street_number": {
          address.streetName = component.long_name;
          break;
        }
        case "route": {
          address.streetName = address.streetName + ", " + component.long_name;
          break;
        }
        case "neighborhood": {
          address.streetName = address.streetName + ", " + component.long_name;
          address.landmark = component.long_name;
          break;
        }
        case "political": {
          address.streetName = address.streetName + ", " + component.long_name;
          break;
        }
        case "sublocality_level_3": {
          address.streetName = address.streetName + ", " + component.long_name;
          break;
        }
        case "sublocality_level_2": {
          address.streetName = address.streetName + ", " + component.long_name;
          break;
        }
        case "sublocality_level_1": {
          address.streetName = address.streetName + ", " + component.long_name;
          break;
        }
        case "locality": {
          address.district = component.long_name;
          break;
        }
        case "administrative_area_level_2": {
          address.city = component.long_name;
          break;
        }
        case "postal_code": {
          address.pincode = component.long_name;
          break;
        }
        case "postal_code_suffix": {
          address.pincode = `${address.pincode}-${component.long_name}`;
          break;
        }
        case "administrative_area_level_1": {
          address.state = component.long_name;
          break;
        }
        case "country":
          break;
        default:
      }
    }
    dispatch(retailerActions.setCompleteAddress(address));
  };

  const onChangeAutocomplete = ({ autocomplete }) => {
    autocomplete.addListener("place_changed", () => {
      const place = autocomplete.getPlace();
      fillInAddress(place);
    });
  };

  return (
    <div>
      <div className="searchbox-container">
        <input
          defaultValue={formattedAddress}
          id="search-location"
          type="text"
          placeholder="Search location"
        />

        <Tooltip
          enterDelay={300}
          enterNextDelay={300}
          className="my-location-container"
          placement="top"
          title="Current Location"
        >
          <MyLocationIcon id="my-location" sx={{ color: "#3535f3" }} />
        </Tooltip>
      </div>
    </div>
  );
}

export default MapSearchBox;
