// import React, { useEffect, useState } from "react";
// import "./style.scss";
// import BasicButton from "../../../components/CustomButton";
// import { Loader } from "@googlemaps/js-api-loader";
// import mapMarkerSvg from "./../../../assets/images/onboarding/map-marker.svg";
// import { CircularProgress } from "@mui/material";
// import { useDispatch } from "react-redux";
// import { useNavigate } from "react-router-dom";
// import { retailerActions } from "../../../redux/slices/retailer";

// const loader = new Loader({
//   apiKey: "AIzaSyAVCJQAKy6UfgFqZUNABAuGQp2BkGLhAgI",
//   version: "weekly",
//   libraries: ["places"],
// });

// const autocompleteOptions = {
//   fields: ["formatted_address", "address_components", "geometry", "name"],
//   strictBounds: false,
//   types: ["establishment"],
// };

// const mapOptions = {
//   center: {
//     lat: 0,
//     lng: 0,
//   },
//   zoom: 15,
//   mapTypeControl: false,
//   streetViewControl: false,
//   disableDefaultUI: true,
// };
// navigator?.geolocation?.getCurrentPosition(function (position) {
//   let lat = position.coords.latitude || 9;
//   let lng = position.coords.longitude || 10;
//   mapOptions.center = { lat, lng };
// });

// function ConfirmLocation() {
//   const [showMap, setShowMap] = useState(false);
//   const [formattedAddress, setFormattedAddress] = useState("");
//   const [name, setName] = useState("");
//   const [addressObj, setAddressObj] = useState({});
//   const dispatch = useDispatch();
//   const navigate = useNavigate();
//   useEffect(() => {
//     initMap();
//   }, []);

//   const updateAddress = () => {
//     dispatch(retailerActions.setCompleteAddress(addressObj));
//     navigate("/onboarding/edit-address");
//   };

//   const initMap = async () => {
//     try {
//       const google = await loader.load();
//       const map = new google.maps.Map(
//         document.getElementById("map"),
//         mapOptions
//       );
//       const geocoder = new google.maps.Geocoder();

//       google.maps.event.addListener(map, "dragend", function (e) {
//         const pos = map.getCenter();
//         geocode(geocoder, { location: pos });
//       });

//       // search location input
//       const searchLocationInput = document.getElementById("search-location");
//       map.controls[google.maps.ControlPosition.TOP_LEFT].push(
//         searchLocationInput
//       );

//       const autocomplete = new google.maps.places.Autocomplete(
//         searchLocationInput,
//         autocompleteOptions
//       );
//       autocomplete.bindTo("bounds", map);
//       setShowMap(true);
//       onChangeAutocomplete({
//         map,
//         autocomplete,
//       });
//       //location button

//       const locationButton = document.createElement("button");

//       locationButton.textContent = "Use my current location";
//       locationButton.classList.add("custom-map-control-button");

//       map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(
//         locationButton
//       );
//       onCurrentLocation(map, geocoder);
//       locationButton.addEventListener("click", (e) => {
//         onCurrentLocation(map, geocoder);
//       });
//       //gecoder
//     } catch (err) {
//       console.log("first");
//     }
//   };

//   function fillInAddress(place) {
//     const name = place.name;
//     setName(name);
//     setFormattedAddress(place.formatted_address);
//     formatAddress(place.address_components);
//   }

//   const onCurrentLocation = (map, geocoder) => {
//     if (navigator.geolocation) {
//       navigator.geolocation.getCurrentPosition((position) => {
//         const pos = {
//           lat: position.coords.latitude,
//           lng: position.coords.longitude,
//         };
//         map.setZoom(20);
//         map.setCenter(pos);
//         geocode(geocoder, { location: pos });
//       });
//     }
//   };
//   function geocode(geocoder, request) {
//     geocoder
//       .geocode(request)
//       .then((result) => {
//         const { results } = result;
//         setFormattedAddress(results?.[0]?.formatted_address);
//         formatAddress(results?.[0]?.address_components);
//         return results;
//       })
//       .catch((e) => {
//         alert("Geocode was not successful for the following reason: " + e);
//       });
//   }

//   const formatAddress = (addressComponent) => {
//     const address = {
//       addressId: "",
//       addressType: "NOT_VERIFIED",
//       buildingName: "",
//       streetName: "",
//       floor: "",
//       landmark: "",
//       house: "",
//       pincode: "",
//       city: "",
//       district: "",
//       state: "",
//       retailerId: "",
//     };
//     for (const component of addressComponent) {
//       const componentType = component.types[0];
//       switch (componentType) {
//         case "street_number": {
//           address.streetName = component.long_name;
//           break;
//         }
//         case "route": {
//           address.streetName = address.streetName + ", " + component.long_name;
//           break;
//         }
//         case "neighborhood": {
//           address.streetName = address.streetName + ", " + component.long_name;
//           break;
//         }
//         case "sublocality_level_3": {
//           address.streetName = address.streetName + ", " + component.long_name;
//           break;
//         }
//         case "sublocality_level_2": {
//           address.streetName = address.streetName + ", " + component.long_name;
//           break;
//         }
//         case "sublocality_level_1": {
//           address.streetName = address.streetName + ", " + component.long_name;
//           break;
//         }
//         case "postal_code": {
//           address.pincode = component.long_name;
//           break;
//         }
//         case "postal_code_suffix": {
//           address.pincode = `${address.pincode}-${component.long_name}`;
//           break;
//         }
//         case "locality":
//           address.city = component.long_name;
//           break;
//         case "administrative_area_level_1": {
//           address.state = component.long_name;
//           break;
//         }
//         case "administrative_area_level_2": {
//           address.district = component.long_name;
//           break;
//         }
//         case "country":
//           break;
//         default:
//       }
//     }
//     setAddressObj(address);
//   };

//   const onChangeAutocomplete = ({ autocomplete, map }) => {
//     autocomplete.addListener("place_changed", () => {
//       const place = autocomplete.getPlace();
//       fillInAddress(place);
//       if (place.geometry.viewport) {
//         map.fitBounds(place.geometry.viewport);
//       } else {
//         map.setCenter(place.geometry.location);
//         map.setZoom(17);
//       }
//     });
//   };

//   return (
//     <div className="confirm-location-margin">
//       <div
//         className="map-container"
//         style={{ visibility: showMap ? "visible" : "hidden" }}
//       >
//         <div id="map-center-marker">
//           <img src={mapMarkerSvg} alt="marker" className="map-marker" />
//         </div>
//         <div id="map-center-tooltip">
//           <div className="tooltip-container">{name || formattedAddress}</div>
//           <div className="divider"></div>
//         </div>
//         <input id="search-location" type="text" placeholder="Search location" />
//         <div id="map"></div>
//         <div id="infowindow-content">
//           <span id="place-name" className="title"></span>
//           <br />
//           <span id="place-address"></span>
//         </div>
//       </div>
//       <CircularProgress
//         style={{ visibility: !showMap ? "visible" : "hidden" }}
//       />
//       <div className="address-name">{name}</div>
//       <div className="formatted-address">{formattedAddress}</div>
//       <BasicButton
//         label="Confirm Location"
//         onClick={() => {
//           updateAddress();
//         }}
//       />
//     </div>
//   );
// }

// export default ConfirmLocation;
