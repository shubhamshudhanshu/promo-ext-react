import React from "react";
import "./style.scss";
import Logo from "./../../../assets/images/onboarding/logo.svg";
import Container from "@mui/material/Container";
function Header() {
  return (
    <div className="header-wrapper">
      <Container maxWidth="xl">
        <div className="content">
          <img src={Logo} alt="logo" className="logo" />
          {/* <a style={{cursor:"pointer"}} href="tel:+18008899999"><img src={headphone} alt="call-icon" className="headphone" /></a> */}
        </div>
      </Container>
    </div>
  );
}

export default Header;
