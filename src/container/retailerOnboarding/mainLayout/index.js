import React, { useEffect } from "react";
import Footer from "../footer/footer";
import "./style.scss";
import Text from "../../../components/CustomText";
import CustomInputField from "../../../components/CustomInput";
function MainLayout({
  title,
  subtitle,
  children,
  next,
  redirect = () => {},
  footerProps = {},
  inputProps = {},
}) {
  const { error = "", value = "" } = inputProps;
  const {
    showSkip = false,
    btnLabel = "Submit",
    btnDisabled = false,
    showSubtitle = true,
    btnLoading = false,
  } = footerProps;

  useEffect(() => {
    redirect();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="layout-container">
      <form
        className="content-margin"
        onSubmit={(e) => {
          e.preventDefault();
          if (!error) {
            next && next(value);
          }
        }}
      >
        <div>
          <Text appearance="heading-m" color="primary-grey-100">
            {title}
          </Text>
          <Text
            className="subtitle"
            appearance="body-m"
            color="primary-grey-80"
          >
            {subtitle}
          </Text>
          {children || (
            <div className="input-container">
              <CustomInputField {...inputProps} />
            </div>
          )}
        </div>
        <Footer
          btnLabel={btnLabel}
          submitDisabled={btnDisabled ? btnDisabled : error ? true : false}
          showSkip={showSkip}
          showSubtitle={showSubtitle}
          btnLoading={btnLoading}
        />
      </form>
    </div>
  );
}

export default MainLayout;
