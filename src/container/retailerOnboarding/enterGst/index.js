import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { retailerActions } from "../../../redux/slices/retailer";
import { retailerMobileActions } from "../../../redux/slices/retailerMobile";
import { toggleShowForm } from "../../../redux/slices/step";

import MainLayout from "../mainLayout";
import verifiedIcon from "./../../../assets/images/onboarding/Icon-verified.svg";

function EnterGst() {
  const dispatch = useDispatch();
  let navigate = useNavigate();
  const { gstError, submittingGst, gstVerified } = useSelector(
    (state) => state.retailerData
  );

  const gstDetail = useSelector(
    (state) => state.retailerData.retailerData.data.gstDetail
  );

  const submitGst = () => {
    dispatch(retailerMobileActions.submitGst())
      .unwrap()
      .then(() => {
        dispatch(toggleShowForm(true));
        navigate(`/onboarding/form`);
      });
  };

  return (
    <MainLayout
      title="GSTIN Details"
      inputProps={{
        name: "GSTIN number",
        label: "Enter GSTIN",
        required: true,
        fullWidth: true,
        error: gstError,
        mask: "##$$$$$####$#$$",
        value: (gstDetail && gstDetail.gstin?.toUpperCase()) || "",
        setError: (value) => dispatch(retailerActions.setGstError(value)),
        handleChange: (e) =>
          dispatch(retailerActions.onChangeGst(e.target.value)),
        suffix: gstVerified && <img src={verifiedIcon} alt="" />,
        readOnly: gstVerified,
        uppercase: true,
      }}
      next={() => {
        if (gstVerified) {
          navigate(`/onboarding/form`);
        } else submitGst();
      }}
      footerProps={{
        showSkip: !gstVerified,
        btnLabel: gstVerified ? "Next" : "Validate",
        btnLoading: submittingGst,
      }}
    />
  );
}

export default EnterGst;
