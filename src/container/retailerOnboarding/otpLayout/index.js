import React from "react";
import MainLayout from "../mainLayout";
import "./style.scss";
import { useNavigate } from "react-router-dom";
import Link from "../../../components/CustomLink";
import CustomOtpInputField from "../../../components/CustomOtpInput";
import CustomOtpTimer from "../../../components/CustomOtpTimer";

function OTPLayout({
  title,
  subtitle,
  showEditNumber,
  resendOtp,
  next,
  footerProps = {},
  inputProps = {},
}) {
  let navigate = useNavigate();

  return (
    <MainLayout
      title={title}
      subtitle={subtitle}
      next={() => next()}
      footerProps={footerProps}
    >
      {showEditNumber && (
        <div style={{ margin: "10px 0" }}>
          <Link
            onClick={function noRefCheck() {
              navigate(-1);
            }}
            textAppearance="body-xxs-bold"
            title="Edit Number"
          />
        </div>
      )}
      <div className="otp-form">
        <div className="input-container">
          <CustomOtpInputField {...inputProps} />
        </div>
        <div className="timer">
          <CustomOtpTimer
            secs={inputProps.resendTimer || 60}
            resend={resendOtp}
            resendBtnClass={"resend"}
            timerClass={"time"}
          />
        </div>
      </div>
    </MainLayout>
  );
}

export default OTPLayout;
