import React from "react";
import { useDispatch, useSelector } from "react-redux";
import CustomInputField from "../../../components/CustomInput";
import Link from "../../../components/CustomLink";
import "./style.scss";
// import locationIcon from "./../../../assets/images/onboarding/locationIcon.svg";
// import rightArrow from "./../../../assets/images/onboarding/rightArrow.svg";
import BasicButton from "../../../components/CustomButton";
import { useNavigate } from "react-router-dom";
import { retailerActions } from "../../../redux/slices/retailer";
import { AddressType } from "../../../helper/constants";
// import ConfirmLocation from "../confirmLocation";
import MapSearchBox from "../mapSearch";
import { CircularProgress } from "@mui/material";

const addressInputs = [
  // {
  //   type: "text",
  //   label: "First Name",
  //   name: "firstName",
  //   required: true,
  //   pattern: null,
  //   maxLength: 100,
  // },
  // {
  //   type: "text",
  //   label: "Last Name",
  //   name: "lastName",
  //   required: true,
  //   pattern: null,
  //   maxLength: 100,
  // },
  // {
  //   type: "text",
  //   label: "Mobile Number",
  //   name: "mobileNumber",
  //   required: true,
  //   pattern: /^[6-9]\d{9}$/,
  //   maxLength: 100,
  //   prefix: "+91",
  // },
  {
    type: "text",
    label: "Flat No./Block No.",
    name: "floor",
    required: true,
    pattern: null,
    maxLength: 15,
  },
  {
    type: "text",
    label: "Building",
    name: "buildingName",
    required: false,
    pattern: null,
    maxLength: 100,
  },
  {
    type: "text",
    label: "Street",
    name: "streetName",
    required: true,
    pattern: null,
    maxLength: 100,
  },
  {
    type: "text",
    label: "Area/Landmark",
    name: "landmark",
    required: false,
    pattern: null,
    maxLength: 100,
  },
  {
    type: "text",
    label: "City",
    name: "city",
    required: true,
    pattern: null,
    maxLength: 100,
  },
  {
    type: "text",
    label: "Pincode",
    name: "pincode",
    required: false,
    pattern: /^[1-9][0-9]{5}$/,
    maxLength: 6,
    mask: "######",
  },
  {
    type: "text",
    label: "District",
    name: "district",
    required: true,
    pattern: null,
    maxLength: 100,
  },
  {
    type: "text",
    label: "State",
    name: "state",
    required: true,
    pattern: null,
    maxLength: 100,
  },
];
function EditAddress() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const values = useSelector((state) => {
    const addresses = state.retailerData.retailerData.data.addresses;
    const notVerifiedAddress = addresses.filter((p) => {
      return p.addressType === AddressType.NOT_VERIFIED;
    });
    return notVerifiedAddress[0] || {};
  });

  const errors = useSelector((state) => state.retailerData.addressError);
  const fetchingCurrentAddress = useSelector(
    (state) => state.retailerData.fetchingCurrentAddress
  );

  const updateAddress = () => {
    dispatch(retailerActions.setIsAddressEdited(true));
    navigate(-1);
  };

  const onSubmitForm = (e) => {
    e.preventDefault();
    updateAddress();
  };

  return (
    <div className="address-form-margin">
      {fetchingCurrentAddress && (
        <div className="address-loader-container">
          <CircularProgress />
        </div>
      )}
      <form className="address-form-container" onSubmit={onSubmitForm}>
        <div className="header">
          <MapSearchBox />
        </div>

        <hr className="partition" />
        {addressInputs.map((input) => {
          return (
            <CustomInputField
              fullWidth
              key={input.name}
              value={values[input.name] || ""}
              handleChange={(e) => {
                dispatch(
                  retailerActions.setAddressFieldValue({
                    value: e.target.value,
                    fieldName: input.name,
                  })
                );
              }}
              setError={(txt) => {
                dispatch(
                  retailerActions.setAddressFieldError({
                    text: txt,
                    fieldName: input.name,
                  })
                );
              }}
              error={errors[input.name]}
              {...input}
            />
          );
        })}
        <div className="save-address-footer">
          <Link
            textAppearance="body-xxs-bold"
            title="Cancel"
            onClick={() => {
              navigate(-1);
            }}
          />
          <BasicButton
            type="submit"
            label={"Save Address"}
            style={{ padding: "5px 14px" }}
          />
        </div>
      </form>
    </div>
  );
}

export default EditAddress;
