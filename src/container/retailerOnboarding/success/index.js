import React from "react";
import "./style.scss";
import Text from "../../../components/CustomText";
import successIcon from "./../../../assets/images/onboarding/success_icon_xxl.svg";
import BasicButton from "../../../components/CustomButton";
function Success({ title, subtitle, next }) {
  return (
    <div className="layout-container">
      <div className="success-margin">
        <div>
          <div
            style={{
              display: "inline-block",
              marginBottom: "20px",
            }}
          >
            <img src={successIcon} alt="green check" />
          </div>
          <div>
            <Text appearance="heading-m" color="primary-grey-100">
              Congratulations
            </Text>
            <Text
              className="subtitle"
              appearance="body-m"
              color="primary-grey-80"
            >
              You are provisionally approved as a JioMart Digital Merchant.
            </Text>
            <div className="info">
              <p className="infoText">
                You will receive your login credentials on your registered
                mobile number.
              </p>
            </div>
          </div>
        </div>
        <BasicButton
          label="Explore JioMart Digital"
          onClick={() => {
            window.location = "https://jmd-b2b.jiohostx0.de/";
          }}
        />
      </div>
    </div>
  );
}

export default Success;
