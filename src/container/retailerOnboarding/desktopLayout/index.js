import React from "react";
import Image from "./../../../assets/images/onboarding/homepage-desktop-images/Onboarding V0.01/Header/.DNA/Image.jpg";
import home from "./../../../assets/images/onboarding/homepage-desktop-images/home.png";
import percent from "./../../../assets/images/onboarding/homepage-desktop-images/percent.png";
import rs from "./../../../assets/images/onboarding/homepage-desktop-images/rs.png";
import sales from "./../../../assets/images/onboarding/homepage-desktop-images/sales.png";
import video from "./../../../assets/images/onboarding/homepage-desktop-images/video.png";
import retailer1 from "./../../../assets/images/onboarding/homepage-desktop-images/retailer1.png";
import white from "./../../../assets/images/onboarding/homepage-desktop-images/white.png";
import yellow from "./../../../assets/images/onboarding/homepage-desktop-images/yellow.png";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import AddIcon from "@mui/icons-material/Add";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import "./style.scss";

export default function DesktopLayout() {
  return (
    <div className="homepage-desktop-container">
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <div id="main" className="banner">
            <Container maxWidth="xl">
              <div className="heading">
                Grow <br />
                your business with
                <br /> JioMart Digital.
                <p style={{ color: "white", fontSize: "24px" }}>
                  We help our partners build a successful retail business.
                </p>
              </div>
            </Container>
          </div>
        </Grid>
      </Grid>
      <Container maxWidth="xl">
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <h4 className="subHeading">
              Why should I <br />
              Partner with JioMart Digital?
            </h4>
            <div className="partner">
              <div>
                <div className="box">
                  <div className="boxlogo">
                    <img src={sales} alt="" />
                  </div>
                  <div className="caption">
                    Installation and after
                    <br /> sales service
                  </div>
                </div>
                <div className="box">
                  <div className="boxlogo">
                    <img src={rs} alt="" />
                  </div>
                  <div className="caption">Easy EMI’s</div>
                </div>
              </div>
              <div>
                <div className="box">
                  <div className="boxlogo">
                    <img src={home} alt="" />
                  </div>
                  <div className="caption">Home delivery/ Store pickup</div>
                </div>

                <div className="box">
                  <div className="boxlogo">
                    <img src={percent} alt="" />
                  </div>
                  <div className="caption"> resQ – Demo</div>
                </div>
              </div>
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className="questions">
              <h3 className="faqTitle">Frequently Asked Questions</h3>
              <br />
              <Accordion>
                <AccordionSummary
                  expandIcon={<AddIcon style={{ color: "#000093" }} />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography>
                    <b>How do I check the status of my order?</b>
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Suspendisse malesuada lacus ex, sit amet blandit leo
                    lobortis eget.
                  </Typography>
                </AccordionDetails>
              </Accordion>
              <Accordion>
                <AccordionSummary
                  expandIcon={<AddIcon style={{ color: "#000093" }} />}
                  aria-controls="panel2a-content"
                  id="panel2a-header"
                >
                  <Typography>
                    <b>
                      {" "}
                      Payment processed but Order number has not got generated
                    </b>
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Suspendisse malesuada lacus ex, sit amet blandit leo
                    lobortis eget.
                  </Typography>
                </AccordionDetails>
              </Accordion>
              <Accordion>
                <AccordionSummary
                  expandIcon={<AddIcon style={{ color: "#000093" }} />}
                  aria-controls="panel2a-content"
                  id="panel2a-header"
                >
                  <Typography>
                    <b>
                      Can I change the shipping address after placing an order?
                    </b>
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Suspendisse malesuada lacus ex, sit amet blandit leo
                    lobortis eget.
                  </Typography>
                </AccordionDetails>
              </Accordion>
              <Accordion>
                <AccordionSummary
                  expandIcon={<AddIcon style={{ color: "#000093" }} />}
                  aria-controls="panel2a-content"
                  id="panel2a-header"
                >
                  <Typography>
                    <b>I can’t apply Coupon to my order?</b>
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Suspendisse malesuada lacus ex, sit amet blandit leo
                    lobortis eget.
                  </Typography>
                </AccordionDetails>
              </Accordion>
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className="video">
              <div className="faqTitle">Learn about JioMart Digital</div>
              <br />
              <img src={video} alt="" height="384px" width="683px" />
            </div>
          </Grid>
        </Grid>
      </Container>
      <div className="experiences">
        <Container maxWidth="xl">
          <h3 className="faqTitle">Experience retailers love to talk about</h3>
          <Grid container spacing={2}>
            <Grid item xs={3}>
              <div className="card">
                <img src={retailer1} alt="" />
                <div>
                  Supriya Sinha
                  <h6>Electro Mart, Punjab</h6>
                  <div
                    style={{
                      display: "flex",
                      width: "150px",
                      justifyContent: "space-between",
                    }}
                  >
                    <div>
                      <img src={yellow} alt="" />
                    </div>
                    <div>
                      <img src={yellow} alt="" />
                    </div>
                    <div>
                      <img src={yellow} alt="" />
                    </div>
                    <div>
                      <img src={yellow} alt="" />
                    </div>
                    <div>
                      <img src={white} alt="" />
                    </div>
                  </div>
                </div>
              </div>
            </Grid>
            <Grid item xs={3}>
              <div className="card">
                <img src={retailer1} alt="" />
                <div>
                  Supriya Sinha
                  <h6>Electro Mart, Punjab</h6>
                  <div
                    style={{
                      display: "flex",
                      width: "150px",
                      justifyContent: "space-between",
                    }}
                  >
                    <div>
                      <img src={yellow} alt="" />
                    </div>
                    <div>
                      <img src={yellow} alt="" />
                    </div>
                    <div>
                      <img src={yellow} alt="" />
                    </div>
                    <div>
                      <img src={yellow} alt="" />
                    </div>
                    <div>
                      <img src={white} alt="" />
                    </div>
                  </div>
                </div>
              </div>
            </Grid>
            <Grid item xs={3}>
              <div className="card">
                <img src={retailer1} alt="" />
                <div>
                  Supriya Sinha
                  <h6>Electro Mart, Punjab</h6>
                  <div
                    style={{
                      display: "flex",
                      width: "150px",
                      justifyContent: "space-between",
                    }}
                  >
                    <div>
                      <img src={yellow} alt="" />
                    </div>
                    <div>
                      <img src={yellow} alt="" />
                    </div>
                    <div>
                      <img src={yellow} alt="" />
                    </div>
                    <div>
                      <img src={yellow} alt="" />
                    </div>
                    <div>
                      <img src={white} alt="" />
                    </div>
                  </div>
                </div>
              </div>
            </Grid>
            <Grid item xs={3}>
              <div className="card">
                <img src={retailer1} alt="" />
                <div>
                  Supriya Sinha
                  <h6>Electro Mart, Punjab</h6>
                  <div
                    style={{
                      display: "flex",
                      width: "150px",
                      justifyContent: "space-between",
                    }}
                  >
                    <div>
                      <img src={yellow} alt="" />
                    </div>
                    <div>
                      <img src={yellow} alt="" />
                    </div>
                    <div>
                      <img src={yellow} alt="" />
                    </div>
                    <div>
                      <img src={yellow} alt="" />
                    </div>
                    <div>
                      <img src={white} alt="" />
                    </div>
                  </div>
                </div>
              </div>
            </Grid>
          </Grid>
        </Container>
      </div>
      <div className="footer">
        <Container maxWidth="xl">
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <img src={Image} alt="" id="logo" />
              <p>
                Copyright © {(new Date().getFullYear())} Reliance Jio Infocomm Ltd. All rights reserved.
              </p>
            </div>
            <ul className="footerDeskLink">
              <li><a href="/onboarding/termsAndConditions">Terms & Conditions</a></li>
            </ul>
          </div>
        </Container>
      </div>
    </div>
  );
}
