import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import MainLayout from "../mainLayout";
import userIcon from "./../../../assets/images/onboarding/userIcon.svg";
import bankIcon from "./../../../assets/images/onboarding/bankIcon.svg";
import gstIcon from "./../../../assets/images/onboarding/gstIcon.svg";
import Text from "../../../components/CustomText";
import { useDispatch } from "react-redux";
import "./style.scss";
import { retailerMobileActions } from "../../../redux/slices/retailerMobile";

const steps = [
  {
    icon: userIcon,
    title: "Verify yourself",
    subtitle: "Verify yourself with any one document (PAN/Aadhar/GSTN.",
  },
  {
    icon: gstIcon,
    title: "Enter your GSTN/PAN & Aadhar",
    subtitle: "Verify your Identity",
  },
  {
    icon: bankIcon,
    title: "Provide your Bank account details",
    subtitle: "Bank account number/IFSC code & other details.",
  },
];

function LandingPage() {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(retailerMobileActions.unVerifyMobile());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <MainLayout
      title="Grow your business with JioMart Digital"
      subtitle="Become a JioMart Digital partner with 3 easy steps!"
      footerProps={{
        btnLabel: "Get Started",
        showSubtitle: false,
      }}
      next={(val) => {
        navigate("/onboarding/enter-mobile");
      }}
    >
      {steps.map((item, index) => {
        return (
          <div key={index} className="homepage-content">
            <div className="icon">
              <img src={item.icon} alt="Step Icon" />
            </div>
            <div className="right">
              <Text appearance="body-m" color="primary-grey-100">
                {item.title}
              </Text>
              <Text appearance="body-xxs" color="primary-grey-80">
                {item.subtitle}
              </Text>
            </div>
          </div>
        );
      })}
    </MainLayout>
  );
}

export default LandingPage;
