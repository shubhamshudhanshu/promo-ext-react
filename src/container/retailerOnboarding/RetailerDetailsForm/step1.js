import React, { useEffect, useImperativeHandle, useState } from "react";
import CustomInputField from "../../../components/CustomInput";
import CustomizedRadios from "../../../components/CustomRadioGroup";
import "./step.scss";
import Text from "../../../components/CustomText";
import { useDispatch, useSelector } from "react-redux";
import { retailerActions } from "../../../redux/slices/retailer";
import { Divider, MenuItem, Select } from "@mui/material";
import VerifiedIcon from "../../../assets/images/onboarding/Icon-verified.svg";
import editIcon from "./../../../assets/images/onboarding/edit-icon.svg";
import addIcon from "./../../../assets/images/onboarding/add-icon.svg";
import { useNavigate } from "react-router-dom";
import { AddressProofType, AddressType } from "../../../helper/constants";
import CloseIcon from "@mui/icons-material/Close";
import CancelIcon from "@mui/icons-material/Cancel";
const options = [
  {
    label: "Yes",
    value: true,
  },
  {
    label: "No",
    value: false,
  },
];
const initialSellerValues = [
  {
    value: "",
    error: "",
    type: "text",
    label: "Owner Name",
    name: "sellerName",
    required: true,
    pattern: null,
    validation: false,
    maxLength: 100,
    verified: false,
  },
  {
    value: "",
    error: "",
    type: "text",
    label: "Store Name",
    name: "storeName",
    required: true,
    pattern: null,
    validation: false,
    maxLength: 100,
    verified: false,
  },
];

const SellerDetailsForm = React.forwardRef((props, ref) => {
  const [formFields, setFormFields] = useState(initialSellerValues);
  const [totalTurnover10Crore, setTotalTurnover10Crore] = useState(null);
  const [tds194Q, setTds194Q] = useState(null);
  const [isTabletRequired, setIsTabletRequired] = useState(null);
  const [addressDetail, setAddressDetail] = useState(null);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const state = useSelector((state) => state.retailerData);

  useEffect(() => {
    props.updateDisableButton(!isStepValid());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    formFields,
    tds194Q,
    totalTurnover10Crore,
    isTabletRequired,
    state.storePhotos,
    addressDetail,
    state.addressProof,
  ]);

  useEffect(() => {
    const { retailerData } = props;
    if (retailerData) {
      let {
        addresses = [],
        tds194Q,
        totalTurnover10Crore,
        gstDetail = {},
        tabletDevice,
        storeName,
        sellerName,
      } = retailerData;
      let copyField = [...formFields];
      if (gstDetail) {
        copyField[0].value = gstDetail.sellerName || "";
        if (gstDetail?.sellerName) {
          copyField[0].verified = true;
        }
      } else {
        copyField[0].value = sellerName || "";
      }
      copyField[1].value = storeName || "";
      setFormFields(copyField);

      totalTurnover10Crore =
        totalTurnover10Crore !== null ? totalTurnover10Crore?.toString() : null;
      tds194Q = tds194Q !== null ? tds194Q?.toString() : null;

      setTotalTurnover10Crore(totalTurnover10Crore || null);
      setTds194Q(tds194Q || null);
      setIsTabletRequired(tabletDevice);
      if (addresses && addresses.length > 0 && addresses[0]) {
        const gstAddress = addresses.filter((p) => {
          return p.addressType === AddressType.GST;
        });
        const notVerifiedAddress = addresses.filter((p) => {
          return p.addressType === AddressType.NOT_VERIFIED;
        });
        if (gstAddress.length > 0) {
          setAddressDetail(gstAddress[0]);
        } else if (notVerifiedAddress.length > 0) {
          setAddressDetail(notVerifiedAddress[0]);
        } else {
          setAddressDetail(null);
        }
      } else {
        setAddressDetail(null);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.retailerData]);

  useImperativeHandle(ref, () => ({
    isStepValid,
    getStepData,
  }));

  const address = useSelector(
    (state) => state.retailerData.retailerData.data.addresses[0]
  );

  const callApi = async () => {
    try {
      const newAddress = {
        ...address,
        addressType: undefined,
        addressId: undefined,
      };
      const newBody = {
        tabletDevice: isTabletRequired,
        sellerName: formFields[0].value,
        shopName: formFields[1].value,
        consent: {
          totalTurnover10Crore,
          tds194Q,
        },
        address: newAddress,
      };
      dispatch(retailerActions.updateSellerDetails(newBody))
        .unwrap()
        .then((res) => {
          dispatch(retailerActions.updateSellerData(res.data));
          props.next();
        })
        .catch((err) => {
          console.log("ERROR", err);
        });
      return false;
    } catch {
      return false;
    }
  };

  const isStepValid = () => {
    let { addresses = [] } = props.retailerData;
    const sellerFieldCheck = formFields.every((p) => {
      if (p.required) {
        return !p.error && p.value;
      } else {
        return true;
      }
    });
    const questionData =
      totalTurnover10Crore !== null &&
      tds194Q !== null &&
      isTabletRequired !== null;
    const storeImages = "";
    let isAddressFound = false;
    if (addressDetail) {
      const isGstAddress = addresses.some(
        (val) => val.addressType === AddressType.GST
      );
      const isManualAddress = addresses.some(
        (val) => val.addressType === AddressType.NOT_VERIFIED
      );
      if (isGstAddress) {
        isAddressFound = true;
      } else if (isManualAddress) {
        const addressProofAttached = state.addressProof;
        if (addressProofAttached) {
          isAddressFound = true;
        }
      }
    }
    if (
      sellerFieldCheck &&
      questionData &&
      state.storePhotos?.length > 0 &&
      isAddressFound
    ) {
      return true;
    }
    return false;
  };
  const getStepData = async () => {
    const ans = await callApi();
    return ans;
  };

  const handleInput = (e, element, index) => {
    const newForm = [...formFields];
    const { value } = e.target;
    newForm[index].value = value;
    dispatch(
      retailerActions.setSellerFieldValue({ type: newForm[index].name, value })
    );
    setFormFields(newForm);
  };

  const getFormField = (element, index, type) => {
    return (
      <CustomInputField
        {...element}
        key={index + "step1"}
        handleChange={(e) => handleInput(e, element, index)}
        readOnly={element.verified}
        suffix={element.verified && <img src={VerifiedIcon} alt="" />}
      />
    );
  };

  return (
    <div
      style={{
        width: "100%",
        marginBottom: "80px",
      }}
    >
      {addressDetail ? (
        <div className="address-detail row-step">
          <h6>Address</h6>
          <div className="address-container">
            <div className="left">
              <div className="title-address">
                {addressDetail?.blockNumber && (
                  <span>{addressDetail.blockNumber.toLowerCase()},</span>
                )}
                {addressDetail?.buildingName && (
                  <span> {addressDetail.buildingName.toLowerCase()}</span>
                )}
              </div>
              <div className="content-address">
                <span>
                  {addressDetail?.streetName && (
                    <span>{addressDetail.streetName.toLowerCase()},</span>
                  )}
                  {addressDetail?.district && (
                    <span> {addressDetail.district.toLowerCase()},</span>
                  )}
                  {addressDetail?.city && (
                    <span>{addressDetail.city.toLowerCase()},</span>
                  )}
                  {addressDetail?.pincode && (
                    <span>{addressDetail.pincode.toLowerCase()}</span>
                  )}
                  {addressDetail?.state && (
                    <span> {addressDetail.state.toLowerCase()}</span>
                  )}
                  {/* {props?.retailerData?.mobileNumber && (
                    <span>
                      {" "}
                      {props?.retailerData?.mobileNumber.toLowerCase()}
                    </span>
                  )} */}
                </span>
              </div>
            </div>
            <div className="right">
              {addressDetail?.addressType === "GST" ? null : (
                <div
                  className="edit-icon-container"
                  onClick={() => {
                    navigate("/onboarding/edit-address");
                  }}
                >
                  <img className="edit-icon" src={editIcon} alt="edit" />
                </div>
              )}
            </div>
          </div>
        </div>
      ) : (
        <div
          className="add-btn"
          onClick={() => {
            navigate("/onboarding/edit-address");
          }}
        >
          <img src={addIcon} alt="" />
          <div>Add Address</div>
          <span className="required-asterisk">*</span>
        </div>
      )}
      {state.isAddressEdited ? (
        <>
          <Text className="row-step" appearance="body-s-bold">
            Attach Address proof
          </Text>
          <Select
            fullWidth
            labelId="demo-simple-select-standard-label"
            id="demo-simple-select-standard"
            value={state.typeOfAddressProof}
            onChange={(e) =>
              dispatch(
                retailerActions.onChangeTypeOfAddressProof(e.target.value)
              )
            }
            label=" "
          >
            <MenuItem value={AddressProofType.ELECTRICITY_BILL}>
              Electricity Bill
            </MenuItem>
            <MenuItem value={AddressProofType.AADHAR}>Aadhar</MenuItem>
          </Select>
          {state.typeOfAddressProof ? (
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "flex-end",
                gap: "20px",
              }}
            >
              <CustomInputField
                fullWidth
                readOnly
                suffix={
                  state.addressProof?.name && (
                    <CloseIcon
                      style={{ cursor: "pointer" }}
                      onClick={() => {
                        dispatch(retailerActions.onRemoveAddressProof());
                      }}
                      sx={{ color: "blue", fontSize: "20px" }}
                    />
                  )
                }
                value={state.addressProof?.name || "Select file"}
              />
              <input
                style={{ display: "none" }}
                accept="image/*"
                id="contained-button-file"
                type="file"
                onChange={(e) => {
                  const file = e.target.files[0];
                  dispatch(retailerActions.onUploadAddressProof(file));
                }}
              />
              <label htmlFor="contained-button-file">
                <div className="browse-btn" onClick={() => {}}>
                  <div>Browse</div>
                </div>
              </label>
            </div>
          ) : null}
        </>
      ) : null}
      <form autoComplete="on">
        {formFields.map((element, index) => {
          return <div key={index}>{getFormField(element, index)}</div>;
        })}

        <input
          style={{ display: "none" }}
          accept="image/*"
          id="store-photos"
          type="file"
          multiple
          onChange={(e) => {
            const files = [...e.target.files];
            console.log(files, typeof files);
            dispatch(retailerActions.onUploadStorePhotos(files));
          }}
        />
        <label htmlFor="store-photos" style={{ width: "100%" }}>
          <div className="add-btn" style={{ margin: "20px 0" }}>
            <img src={addIcon} alt="" />
            <div>Add Store Photos</div>
            <span className="required-asterisk">*</span>
          </div>
        </label>
        <div className="store-photos">
          {state.storePhotos?.map((item, i) => {
            return (
              <div className="photo-container" key={i}>
                <CancelIcon
                  className="cross"
                  onClick={() => {
                    dispatch(retailerActions.deletePhotos(i));
                  }}
                />
                <img key={i} className="photo" alt="photos" src={item} />
              </div>
            );
          })}
        </div>

        <div className="row-step">
          <Text appearance="body-xs">
            Do you want a tablet device?
            <span className="required-asterisk">*</span>
          </Text>
          <div style={{ display: "flex" }}>
            <CustomizedRadios
              row
              handleChange={(e) => {
                setIsTabletRequired(e.target.value);
              }}
              options={options}
              value={isTabletRequired}
            />
          </div>
        </div>
        <Divider />
        <Text className="row-step" appearance="body-s-bold">
          Declaration
        </Text>
        <div className="row-step">
          <Text appearance="body-xs">
            Whether your total turnover during preceding FY (FY 2020-2021) is
            ₹10Cr?<span className="required-asterisk">*</span>
          </Text>
          <div style={{ display: "flex" }}>
            <CustomizedRadios
              row
              handleChange={(e) => {
                setTotalTurnover10Crore(e.target.value);
              }}
              options={options}
              value={totalTurnover10Crore}
            />
          </div>
        </div>
        <div className="row-step">
          <Text appearance="body-xs">
            Whether TDS U/S 194Q is applicable on your purchases from RRL (all
            Business) and TDS will be deducted from invoice value payable to
            RRL?<span className="required-asterisk">*</span>
          </Text>
          <div style={{ display: "flex" }}>
            <CustomizedRadios
              row
              handleChange={(e) => {
                setTds194Q(e.target.value);
              }}
              options={options}
              value={tds194Q}
            />
          </div>
        </div>
      </form>
    </div>
  );
});
export default SellerDetailsForm;
