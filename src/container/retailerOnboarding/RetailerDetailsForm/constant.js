import SellerDetailsForm from "./step1"
import PersonalDetailsForm from "./step2"
import BankDetailsForm from "./step3"

export const steps = (gstVerified) => {
    if (gstVerified) {
        return [{

            name: "Seller Details",
            stepIndex: 0,
            component: SellerDetailsForm
        },
        {
            name: "Personal Details",
            stepIndex: 1,
            component: PersonalDetailsForm

        },
        {
            name: "Bank Details",
            stepIndex: 2,
            component: BankDetailsForm

        }
        ]
    }

    return [{

        name: "Personal Details",
        stepIndex: 0,
        component: PersonalDetailsForm

    },
    {
        name: "Seller Details",
        stepIndex: 1,
        component: SellerDetailsForm

    },
    {
        name: "Bank Details",
        stepIndex: 2,
        component: BankDetailsForm

    }
    ]
}