import React, { useRef, useState, useEffect } from "react";
import { steps } from "./constant";
import "./step.scss";
import OnBoardingCompleted from "./onBoardingCompleted";

import Box from "@mui/material/Box";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import { StepLabel } from "@mui/material";
import {
  QontoStepIcon,
  QontoConnector,
} from "../../../components/customStepperJio.js/index.js";
import BasicButton from "../../../components/CustomButton";
import BackIcon from "../../../assets/images/onboarding/back-icon.svg";
import {
  goNext,
  goBack,
  toggleShowOtp,
  toggleAdhaarVerifyPage,
} from "../../../redux/slices/step";
import { useDispatch, useSelector } from "react-redux";
import { NotificationActions } from "../../../redux/slices/notification";

const RetailerForm2 = (props) => {
  const [sellerFormData] = useState(null);

  const [disableNextBtn, setDisableNextBtn] = useState(true);
  const { gstVerified } = useSelector((state) => state.retailerData);
  const ref = useRef();

  const dispatch = useDispatch();
  const state = useSelector((state) => state.retailerData);
  const stepState = useSelector((state) => state.stepSlice);
  const retailerData = state?.retailerData;
  const activeStep = stepState?.currentStep;

  useEffect(() => {
    dispatch(toggleShowOtp(false));
    dispatch(toggleAdhaarVerifyPage(false));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useEffect(() => {
    sessionStorage.setItem("form_step", activeStep);
  }, [activeStep]);
  const updateDisableButton = (notValid) => {
    setDisableNextBtn(notValid);
  };

  const totalSteps = () => {
    return steps(gstVerified).length;
  };

  const completedSteps = () => {
    return activeStep;
  };

  const allStepsCompleted = () => {
    return completedSteps() === totalSteps();
  };
  const next = () => {
    dispatch(goNext());
  };

  const handleBack = () => {
    dispatch(goBack());
  };

  const updateRetailerData = (data) => {};
  const handleNext = async () => {
    const ans = ref.current.isStepValid();

    if (ans) {
      await ref.current.getStepData();
    } else {
      dispatch(
        NotificationActions.addNotification({
          message: "Please fill all the required fields!",
          type: "error",
        })
      );
    }
  };

  const renderstepDynamicContent = (index, ref) => {
    const Component = steps(gstVerified)[index]?.component;

    return (
      <Component
        id={index}
        ref={ref}
        sellerFormData={sellerFormData}
        updateForm={(data) => {
          updateRetailerData(data);
        }}
        next={next}
        retailerData={retailerData?.data}
        updateDisableButton={updateDisableButton}
      />
    );
  };

  const renderFooter = () => {
    return (
      <div className="footer-form">
        <div className="content">
          {activeStep > 0 && (
            <BasicButton handleClick={handleBack} kind="secondary">
              <img
                style={{ height: "20px", width: "20px" }}
                src={BackIcon}
                alt="back"
              />
            </BasicButton>
          )}

          <BasicButton
            loading={state.stepLoading}
            fullWidth
            label={"Next"}
            handleClick={handleNext}
            disabled={disableNextBtn}
          />
        </div>
      </div>
    );
  };

  return (
    <div className="content-margin">
      {allStepsCompleted() ? (
        <div
          style={{
            height: "100%",
            paddingBottom: "80px",
          }}
        >
          <OnBoardingCompleted />
        </div>
      ) : (
        <div
          style={{
            height: "100%",
          }}
        >
          <Stepper
            activeStep={activeStep}
            alternativeLabel
            className="stepper-custom"
            connector={<QontoConnector />}
          >
            {steps(gstVerified).map((item, index) => (
              <Step key={item.name + index}>
                <StepLabel StepIconComponent={QontoStepIcon}>
                  {item.name}
                </StepLabel>
              </Step>
            ))}
          </Stepper>
          <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
            {renderstepDynamicContent(activeStep, ref)}
            {renderFooter()}
          </Box>
        </div>
      )}
    </div>
  );
};

export default RetailerForm2;
