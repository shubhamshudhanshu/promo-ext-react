
import React from "react";
import Text from "../../../components/CustomText";
import BasicButton from "../../../components/CustomButton";
import SuccessIcon from '../../../assets/images/onboarding/success_icon_xxl.svg'

const OnBoardingCompleted = (props) => {
  return (
    <div
      horizontalAlignment="stretch"
      margin="base"
      layout="undefined"
      className="onboarding-completed"
    >
      <div>
        <img src={SuccessIcon} alt='success icon' />
      </div>
      <h4>Congratulations</h4>
      <Text appearance="body-m">Onboarding completed successfully!</Text>
      <BasicButton
        style={{ maxWidth: "363px", width: "100%" }}
        label="View Details"
      />
    </div>
  );
};
export default OnBoardingCompleted;
