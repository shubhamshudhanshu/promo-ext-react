import React, {
  useState,
  forwardRef,
  useImperativeHandle,
  useEffect,
} from "react";
import { useDispatch } from "react-redux";
import { retailerActions } from "../../../redux/slices/retailer";
import CustomInputField from "../../../components/CustomInput";
import { useNavigate } from "react-router-dom";

const initialValues = [
  {
    value: "",
    error: "",
    type: "text",
    label: "Account Number",
    name: "accountNumber",
    required: true,
    pattern: /^[0-9]+$/,
    validation: true,
    maxLength: 100,
    strictCheck: true,
  },
  {
    value: "",
    error: "",
    type: "text",
    label: "Account Name",
    name: "accountName",
    required: true,
    pattern: /^[a-zA-Z0-9 ]*$/,
    validation: true,
    maxLength: 100,
  },
  {
    value: "",
    error: "",
    type: "text",
    label: "Bank Name",
    name: "bankName",
    required: true,
    pattern: /^[a-zA-Z0-9 ]*$/,
    validation: true,
    maxLength: 100,
  },
  {
    value: "",
    error: "",
    type: "text",
    label: "IFSC Code",
    name: "ifsc",
    required: true,
    validation: true,
    mask: "$$$$#******",
    maxLength: 11,
    minLength: 11,
    strictCheck: true,
  },
];

const BankDetailsForm = forwardRef((props, ref) => {
  const dispatch = useDispatch();
  // const state = useSelector((state) => state.retailerData);
  const [formFields, setFormFields] = useState(initialValues);

  useEffect(() => {
    const { retailerData } = props;
    if (retailerData) {
      let { bankDetail } = retailerData;
      if (bankDetail && Object.keys(bankDetail).length > 0) {
        let copyField = [...formFields];
        copyField.forEach((res) => {
          res.value = bankDetail[res.name] || "";
        });
        setFormFields(copyField);
      }
    }
    props.updateDisableButton(!isStepValid());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.retailerData]);
  const navigate = useNavigate();

  useEffect(() => {
    props.updateDisableButton(!isStepValid());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [formFields]);

  const isStepValid = () => {
    const sellerFieldCheck = formFields.every((p) => {
      if (p.required) {
        return !p.error && p.value;
      } else {
        if (p.name === "accountName") {
          return checkBankAccountName(p.value);
        }
        return true;
      }
    });
    if (sellerFieldCheck) {
      return true;
    }
    return false;
  };
  useImperativeHandle(ref, () => ({
    isStepValid,
    getStepData,
  }));
  const createBankDetailsObject = () => {
    const bankDetail = {};
    formFields.forEach((res) => {
      bankDetail[res.name] = res.value;
    });
    return bankDetail;
  };

  const callApi = async () => {
    try {
      const bodyObject = createBankDetailsObject();
      await dispatch(retailerActions.postBankDetails(bodyObject))
        .unwrap()
        .then((res) => {
          dispatch(retailerActions.updateSellerData(res.data));
          navigate("/onboarding/review");
        })
        .catch((err) => {
          console.log(err);
        });
    } catch (err) {}
  };

  const checkBankAccountName = (val) => {
    const { retailerData } = props;
    const gstName = retailerData.gstDetail?.sellerName?.toLowerCase();
    const panName = retailerData.panDetail?.name?.toLowerCase();
    const aadhaarName = retailerData.aadharDetail?.name?.toLowerCase();
    const value = val.toLowerCase();
    if (value !== aadhaarName && value !== panName && value !== gstName) {
      return false;
    }
    return true;
  };

  const getStepData = async () => {
    await callApi();
    return true;
  };

  // const handleError = (index, text) => {
  //   var newFormFields = [...formFields];
  //   newFormFields[index].error = text;
  //   console.log("Setting error")
  //   setFormFields(newFormFields);
  // }

  // // const pulkit = (e, element, index) => {

  // //   setFormFields(newFormFields);
  // // }

  const handleInput = (e, element, index) => {
    e.preventDefault();
    var newFormFields = [...formFields];
    // newFormFields[index].value = e.target.value;

    if (e.target.value.length === 0) {
      newFormFields[index].value = "";
      newFormFields[index].error = "";
      setFormFields(newFormFields);
      return;
    }
    newFormFields[index].value = e.target.value;
    newFormFields[index].error = "";
    if (element.validation) {
      const regex = element.pattern;
      const ans = regex.test(e.target.value);
      if (!ans) {
        newFormFields[index].error = "Invalid Input";
      }
    }
    if (index === 1) {
      if (!checkBankAccountName(e.target.value)) {
        newFormFields[index].error =
          "Bank name does not match with adhaar, pan or gst name!";
      } else {
        newFormFields[index].error = "";
      }
    }
    setFormFields(newFormFields);
  };

  return (
    <div
      style={{
        width: "100%",
        marginBottom: "80px",
      }}
    >
      <form autoComplete="on">
        {formFields.map((element, index) => {
          return (
            <CustomInputField
              {...element}
              key={index + "step3"}
              handleChange={(e) => handleInput(e, element, index)}
            />
          );
        })}
        <br />
      </form>
    </div>
  );
});
export default BankDetailsForm;
