import React, {
  useState,
  useImperativeHandle,
  forwardRef,
  useEffect,
} from "react";
import { useNavigate } from "react-router-dom";
import CustomInputField from "../../../components/CustomInput";
import { toggleAdhaarVerifyPage } from "../../../redux/slices/step";
import { useDispatch } from "react-redux";
import { updateAdhaarNumber } from "../../../redux/slices/step";
import { retailerActions } from "../../../redux/slices/retailer";
import VerifiedIcon from "../../../assets/images/onboarding/Icon-verified.svg";
import { ProofStatus } from "../../../helper/constants";
const initialValues = [
  {
    value: "",
    error: "",
    type: "text",
    name: "user_name",
    label: "Name",
    required: true,
    pattern: /^[A-Za-z0-9]+$/,
    validation: false,
    maxLength: 50,
    verified: false,
    show: false,
  },
  {
    value: "",
    error: "",
    type: "text",
    name: "pan_number",
    label: "Pan Number",
    required: true,
    pattern: /^[A-Za-z]{5}\d{4}[A-Za-z]{1}$/,
    mask: "$$$$$####$",
    validation: true,
    maxLength: 10,
    verified: false,
    show: true,
  },
  {
    value: "",
    error: "",
    type: "text",
    label: "Aadhaar Number",
    name: "aadhaar_number",
    required: true,
    pattern: /^[0-9]{12}$/,
    mask: "############",
    validation: true,
    maxLength: 12,
    verified: false,
    show: false,
  },
];

const PersonalDetailsForm = forwardRef((props, ref) => {
  const [formFields, setFormFields] = useState(initialValues);

  const dispatch = useDispatch();
  let navigate = useNavigate();

  useEffect(() => {
    props.updateDisableButton(!isStepValid());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [formFields]);

  const isStepValid = () => {
    const sellerFieldCheck = formFields.every((p) => {
      if (p.required && p.show) {
        return !p.error && p.value;
      } else {
        return true;
      }
    });
    if (sellerFieldCheck) {
      return true;
    } else {
      return false;
    }
  };
  const getStepData = async () => {
    const isPanVerified = formFields[1].verified;
    const isAadharVerified = formFields[2].verified;
    const newFormFields = [...formFields];
    if (!isPanVerified) {
      await addPanDetails(formFields[1].value, newFormFields);
    } else if (!isAadharVerified) {
      sessionStorage.setItem('adh_no', formFields[2].value);
      await dispatch(updateAdhaarNumber(formFields[2].value));
      dispatch(toggleAdhaarVerifyPage(true));
      navigate("/onboarding/verify-adhaar");
      return true;
    } else {
      props.next();
    }
    return false;
  };

  const addPanDetails = async (panNumber, newFormFields) => {
    ///call pan api here
    dispatch(retailerActions.postPanNumber(panNumber))
      .unwrap()
      .then((res) => {
        newFormFields[0].show = true;
        newFormFields[2].show = true;
        newFormFields[1].verified = true;
        const panDetail = res.data?.panDetail;
        if (panDetail && panDetail?.name) {
          newFormFields[0].value = panDetail.name;
          newFormFields[0].verified = true;
          newFormFields[0].show = true;
        }
        setFormFields(newFormFields);
        dispatch(retailerActions.updateSellerData(res.data));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useImperativeHandle(ref, () => ({
    isStepValid,
    getStepData,
  }));
  useEffect(() => {
    const { retailerData } = props;
    if (retailerData) {
      let copyField = [...formFields];
      const { panDetail, aadharDetail } = retailerData;
      if (panDetail?.pan) {
        copyField[1].value = panDetail.pan;
        if (panDetail.status === ProofStatus.VERIFIED) {
          copyField[0].value = panDetail.name;
          copyField[0].verified = true;
          copyField[1].verified = true;
          copyField[0].show = true;
          copyField[2].show = true;
        }
      }
      if (aadharDetail?.aadhar) {
        copyField[2].value = aadharDetail.aadhar;
        if (aadharDetail.status === ProofStatus.VERIFIED) {
          copyField[2].verified = true;
        }
      }

      setFormFields(copyField);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.retailerData]);

  const handleInput = (e, element, index) => {
    e.preventDefault();

    var newFormFields = [...formFields];
    if (e.target.value.length === 0) {
      newFormFields[index].value = "";
      newFormFields[index].error = "";
      setFormFields(newFormFields);
      return;
    }
    if ((element.name = "panNumber")) {
      e.target.value = e.target.value.toUpperCase();
    }
    newFormFields[index].value = e.target.value;
    newFormFields[index].error = "";
    if (element.validation) {
      const regex = element.pattern;
      const ans = regex.test(e.target.value);
      if (!ans) {
        newFormFields[index].error = "Invalid Input";
      }
    }
    setFormFields(newFormFields);
  };

  return (
    <div
      style={{
        width: "100%",
        marginBottom: "80px",
      }}
    >
      <form
        autoComplete="off"
        onSubmit={(e) => {
          e.preventDefault();
        }}
      >
        {formFields.map((element, index) => {
          return (
            element.show && (
              <CustomInputField
                {...element}
                key={index + "step2"}
                readOnly={element.verified}
                handleChange={(e) => handleInput(e, element, index)}
                suffix={element.verified && <img src={VerifiedIcon} alt="" />}
              />
            )
          );
        })}
      </form>
    </div>
  );
});
export default PersonalDetailsForm;
