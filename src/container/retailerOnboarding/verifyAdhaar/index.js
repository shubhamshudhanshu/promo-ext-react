import { Box } from "@mui/material";
import React, { useEffect, useState } from "react";
import BasicButton from "../../../components/CustomButton";
import CustomInputField from "../../../components/CustomInput";
import Text from "../../../components/CustomText";
import BackIcon from "../../../assets/images/onboarding/back-icon.svg";
import { useLocation, useNavigate } from "react-router-dom";
import { goNext, toggleAdhaarVerifyPage, toggleShowOtp, updateAdhaarNumber } from "../../../redux/slices/step";
import { useDispatch, useSelector } from "react-redux";
import CrossIcon from "../../../assets/images/onboarding/Icon-cross.svg";

import "./verifyAdhaar.scss";
import { retailerActions } from "../../../redux/slices/retailer";

const VerifyAadhar = (props) => {
  const [imageData, setImageData] = useState({
    front: {
      data: null,
      error: "",
    },
    back: {
      data: null,
      error: "",
    },
  });

  const [, setBackImage] = useState({
    data: null,
    error: "",
  });
  const dispatch = useDispatch();
  const location = useLocation()

  const stepState = useSelector((state) => state.stepSlice);




  const { submittingAdhaar, stepLoading } = useSelector((state) => state.retailerData);

  useEffect(() => {

    const getAdhaarNo = sessionStorage.getItem('adh_no');

    console.log("location.state", location.state, stepState)
    if (!getAdhaarNo) {
      navigate(`/onboarding/form`);
    }
    dispatch(updateAdhaarNumber(getAdhaarNo))

  }, [])

  const handleFileUpload = (e, type) => {
    e.preventDefault();
    let formData = new FormData();
    formData.append("image", e.target.files[0]);
    const files = e.target.files[0];
    const newCopy = { ...imageData };
    newCopy[type].error = "";
    newCopy[type].data = files;
    setImageData(newCopy);
  };
  const removeFile = (type) => {
    const newCopy = { ...imageData };
    newCopy[type].data = null;
    newCopy[type].error = "";
    setBackImage(newCopy);
  };
  let navigate = useNavigate();

  const enableNextBtn = () => {
    if (!imageData["front"].data || !imageData["back"].data) {
      return true;
    }
    return false;
  };

  return (
    <div className="content-margin">
      <Box className="layout-block">
        <Text appearance="heading-m">Verify Aadhaar</Text>

        <div className="verify-block">
          <CustomInputField
            value={stepState.enteredAdhaarNumber}
            readOnly={true}
          />
          <BasicButton
            fullWidth
            kind={"tertiary"}
            handleClick={() => {
              dispatch(
                retailerActions.sendAdhaarOtp({
                  aadhaarNo: stepState.enteredAdhaarNumber,
                })
              )
                .unwrap()
                .then((res) => {
                  dispatch(toggleShowOtp(true));
                  navigate(`/onboarding/enter-otp?otptype=adhaar`);
                })
                .catch((err) => {
                  console.log(err);
                });
            }}
            loading={submittingAdhaar}
            label="Verify via OTP"
          />

          <Text>OR</Text>
          <div className="upload-layout">
            <Text appearance="body-xs" color={"grey-80"}>
              Verify via Adhaar
            </Text>
            {imageData["front"]?.data?.name ? (
              <div className="upload-preview">
                <label>{imageData["front"]?.data?.name}</label>
                <img
                  src={CrossIcon}
                  onClick={(e) => {
                    removeFile("front");
                  }}
                  alt=""
                />
              </div>
            ) : (
              <BasicButton
                fullWidth
                component="label"
                variant="outlined"
                kind={"tertiary"}
                sx={{ borderRadius: "1000px" }}
                handleClick={() => {
                  console.log("handle button click");
                }}
              >
                Attach Aadhar front view
                <input
                  type="file"
                  accept="image/*"
                  hidden
                  onChange={(e) => handleFileUpload(e, "front")}
                />
              </BasicButton>
            )}
            {imageData["back"]?.data?.name ? (
              <div className="upload-preview">
                <label>{imageData["back"]?.data?.name}</label>
                <img
                  src={CrossIcon}
                  onClick={(e) => {
                    removeFile("back");
                  }}
                  alt=""
                />
              </div>
            ) : (
              <BasicButton
                fullWidth
                component="label"
                variant="outlined"
                kind={"tertiary"}
                sx={{ borderRadius: "1000px" }}
                handleClick={() => {
                  console.log("handle button click");
                }}
              >
                Attach Aadhar back view
                <input
                  type="file"
                  accept="image/*"
                  hidden
                  onChange={(e) => handleFileUpload(e, "back")}
                />
              </BasicButton>
            )}
          </div>
        </div>

        <div className="footer-form">
          <div className="content">
            <BasicButton
              handleClick={() => {
                navigate(`/onboarding/form`);
                console.log("handle back");
              }}
              kind="secondary"
            >
              <img
                style={{ height: "20px", width: "20px" }}
                src={BackIcon}
                alt="back"
              />
            </BasicButton>

            <BasicButton
              fullWidth
              label={"Next"}
              loading={stepLoading}
              handleClick={() => {
                const body = {
                  aadhaarNo: stepState.enteredAdhaarNumber,
                };
                dispatch(retailerActions.verifyAdhaarManualy(body))
                  .unwrap()
                  .then((res) => {
                    if (res.data) {
                      dispatch(retailerActions.updateSellerData(res.data));
                      dispatch(goNext());
                      sessionStorage.removeItem("adh_no")
                      navigate(`/onboarding/form`);
                    }
                  })
                  .catch((err) => {
                    console.log(err);
                  });
              }}
              disabled={enableNextBtn()}
            />
          </div>
        </div>
      </Box>
    </div>
  );
};

export default VerifyAadhar;
