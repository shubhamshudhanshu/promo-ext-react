import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { toggleShowAdminOtp } from "../../../redux/slices/admin";
import OTPLayout from "../../retailerOnboarding/otpLayout";
import "./otp.scss";

function VerifyOtp({ otpType }) {
  let navigate = useNavigate();
  const dispatch = useDispatch();
  const { otp } = useSelector((state) => state.retailerMobile);

  return (
    <div className="otp-card">
      <OTPLayout
        title="Verify OTP"
        subtitle={`OTP has been sent to yout Registered Ril Email Id`}
        footerBtnLabel="Verify OTP"
        footerBtnDisabled={!(otp && otp.length === 6)}
        resendOtp={() => {
          console.log("resend");
        }}
        length={6}
        next={(val) => {
          console.log("verify");
          dispatch(toggleShowAdminOtp(false));
          navigate("/onboarding/admin");
          sessionStorage.setItem("admin_logged_in", true);
        }}
      />
    </div>
  );
}

export default VerifyOtp;
