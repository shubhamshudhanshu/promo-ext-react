import { useRef } from "react";
import { useEffect } from "react";
import { useState } from "react";
import BasicButton from "../../../components/CustomButton";
import CustomInputField from "../../../components/CustomInput";

const EditAddressForm = (props) => {
  const [addressDetails, setAddressDetails] = useState(props.address);
  const ref = useRef();
  useEffect(() => {
    setAddressDetails(props.address);
    Object.entries(addressDetails);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const handleChange = (e) => {
    let newCopy = { ...addressDetails };
    const { name } = e.target;
    newCopy[name] = e.target.value;
    setAddressDetails(newCopy);
  };
  return (
    <div class="address-form-container">
      <form
        onSubmit={(e) => {
          e.preventDefault();
          console.log("hello world", addressDetails);
          props.updateData(addressDetails);
        }}
        ref={ref}
      >
        {addressDetails &&
          Object.entries(addressDetails).map(([key, value]) => {
            return (
              <>
                <CustomInputField
                  name={key}
                  value={value}
                  label={key}
                  handleChange={handleChange}
                />
              </>
            );
          })}

        <BasicButton fullWidth type="submit" label={"Update Data"} />
      </form>
    </div>
  );
};
export default EditAddressForm;
