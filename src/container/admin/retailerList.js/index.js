import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { dummyData } from "./dummy";
import { makeStyles } from "@mui/styles";
import Text from "../../../components/CustomText";
import { useState } from "react";
import TransitionsModal from "../../../components/CustomModal";
import EditAddressForm from "./EditAddressForm";
import { useEffect } from "react";
import IconButton from "@mui/material/IconButton";
import ModeEditOutlineOutlinedIcon from "@mui/icons-material/ModeEditOutlineOutlined";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import CheckCircleOutlineTwoToneIcon from "@mui/icons-material/CheckCircleOutlineTwoTone";
import { useNavigate } from "react-router-dom";

export function createAddress(val) {
  const addressString =
    [
      val.house,
      val.floor,
      val.buildingName,
      val.city,
      val.district,
      val.city,
      val.state,
      val.pincode,
    ].join() || null;
  return addressString;
}
const useStyles = makeStyles({
  root: {
    // backgroundColor: "blue !important",
    fontFamily: "JioType",
    "& .MuiTableCell-head": {
      color: "white",
      // backgroundColor: "blue"
    },
  },
  tableHead: {
    color: "white",
    backgroundColor: "blue !important",
  },
});
export const createTableData = (data) => {
  const status = data.status;
  const primaryAddress =
    data?.addresses?.filter((add) => {
      return add.addressType === "GST";
    })[0] || null;
  const secondaryAddress =
    data?.addresses?.filter((add) => {
      return add.addressType === "GST"; ///"NOT_VERIFIED"
    })[0] || null;

  const isAddressEditable =
    (secondaryAddress && Object.keys(secondaryAddress).length > 0) || false;
  const address = !isAddressEditable ? primaryAddress : secondaryAddress;

  return {
    user_id: data.id,
    sellerName: data.sellerName,
    storeName: data.storeName,
    aadhar: data?.aadharDetail?.aadhar || "NA ",
    gstin: data?.gstDetail?.gstin || "NA ",
    pan: data?.panDetail?.pan || "NA",
    accountNumber: data?.bankDetail?.accountNumber,
    accountName: data?.bankDetail?.accountName,
    tabletDevice: data.tabletDevice,
    totalTurnover10Crore: data.totalTurnover10Crore,
    tds194Q: data.tds194Q,
    isAddressEditable,
    status,
    address: address,
    // address: primaryAddress || secondaryAddress,
    primaryAddress,
    secondaryAddress,
    addresses: data.addresses,
    // adddress: createAddress(data?.addresses?.[0])
  };
};

const columns = [
  { id: "sellerName", label: "Seller Name" },
  { id: "storeName", label: "Store Name" },
  {
    id: "aadhar",
    label: "Aadhar Number",
    // minWidth: 170
    // format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: "gstin",
    label: "GST Number",
    // align: 'right',    pan: 'AABCI6363G',
    accountNumber: "2345678901234",
    accountName: "Shubham Shudhanshu",
    // format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: "pan",
    label: "Pan Number",
    // minWidth: 170,
    // format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: "accountNumber",
    label: "Bank Account Number ",
    // minWidth: 170,
    //  format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: "address",
    label: "Address ",
    minWidth: 170,
    format: (element) => {
      return (
        element &&
        Object.values(element)
          .filter((p) => p)
          .join()
      );
    },
  },
  {
    id: "actions",
    label: "Actions",
    minWidth: 100,
    // format: (value) => value.toLocaleString('en-US'),
  },

  {},
];

const RetailerList = () => {
  const [page, setPage] = React.useState(0);
  const [retailerList, setRetailerList] = useState(dummyData);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [showEditModal, setShowEditModal] = useState(false);
  const [currentRow, setCurrentEditAddress] = useState(null);
  const classes = useStyles();

  const navigate = useNavigate();
  useEffect(() => {
    const admin_logged_in = sessionStorage.getItem("admin_logged_in");
    if (!admin_logged_in) {
      navigate("/onboarding/admin/login");
    }
    setRetailerList(dummyData);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getTableData = () => {
    return retailerList.map((res) => {
      return createTableData(res);
    });
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const updateData = (data) => {
    const copyRetailerData = [...retailerList];
    const index = copyRetailerData.findIndex(
      (ind) => ind.id === currentRow.user_id
    );
    if (index !== -1) {
      const copy = copyRetailerData[index].addresses.filter(
        (type) => type.addressType !== "GST"
      );
      copyRetailerData[index].addresses = [data, ...copy];
      console.log(copy, copyRetailerData[index].addresses, copyRetailerData);
      setRetailerList(copyRetailerData);
      setShowEditModal(false);
    }
  };
  const rows = getTableData();

  return (
    <div>
      <TransitionsModal
        sx={{ maxHeight: "500px" }}
        open={showEditModal}
        handleClose={() => {
          setShowEditModal(false);
        }}
      >
        <EditAddressForm
          address={currentRow?.address}
          updateData={updateData}
        />
      </TransitionsModal>

      <Text
        appearance={"body-s-bold"}
        sx={{ margin: "24px 0px", fontSize: "16px" }}
      >
        Retailer List
      </Text>
      <TableContainer>
        <Table stickyHeader aria-label="sticky table" className={classes.root}>
          <TableHead className={classes.tableHead}>
            <TableRow
              sx={{
                "& th": {
                  fontSize: "1rem",
                  color: "white",
                  backgroundColor: "#3535f3",
                  fontFamily: "JioType",
                },
              }}
            >
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {getTableData()
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                    {columns.map((column) => {
                      const value = row[column.id];
                      return column.id !== "actions" ? (
                        <TableCell key={column.id} align={column.align}>
                          {column.format ? column.format(value) : value}
                        </TableCell>
                      ) : (
                        <TableCell key={column.id}>
                          <div style={{ display: "flex" }}>
                            <IconButton
                              aria-label="edit"
                              onClick={(e) => {
                                console.log("open modal here");
                                setCurrentEditAddress(row);
                                setShowEditModal(true);
                                console.log(value, row);
                              }}
                            >
                              <ModeEditOutlineOutlinedIcon />
                            </IconButton>
                          </div>

                          <div style={{ display: "flex" }}>
                            <IconButton
                              aria-label="approve"
                              onClick={(e) => {
                                console.log("approveh here");
                              }}
                              color="success"
                              fontSize={"large"}
                            >
                              <CheckCircleOutlineTwoToneIcon
                                fontSize={"large"}
                              />
                            </IconButton>
                            <IconButton
                              aria-label="reject"
                              onClick={(e) => {
                                console.log("reject here");
                              }}
                              color="error"
                              fontSize={"large"}
                            >
                              <HighlightOffIcon fontSize={"large"} />
                            </IconButton>
                          </div>

                          {/* <Button>Aprrove</Button>
                            <Button>Reject</Button> */}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </div>
  );
};

export default RetailerList;
