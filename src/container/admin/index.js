import React from "react";
import { useSelector } from "react-redux";
import { Routes, Route, Navigate } from "react-router-dom";
import Header from "../retailerOnboarding/header/header.js";
import Login from "./login/index.js";
import RetailerList from "./retailerList.js";
import "./style.scss";
import VerifyOtp from "./VerifyOtp/index.js";

function AdminPages() {
  const adminState = useSelector((state) => state.adminSlice)
  return (
    <div className="admin-container">
      <Header />
      <div className="pages-wrapper">
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route path="/" element={<RetailerList />} />

          {adminState?.showAdminOtp && (
            <Route
              path="/enter-otp"
              exact
              element={<VerifyOtp otpType={"admin"} />}
            />)}
          <Route path="*" element={<Navigate to="/onboarding/admin/login" />} />





        </Routes>
      </div>
    </div>
  );
}

export default AdminPages;
