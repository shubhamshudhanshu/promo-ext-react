import React from "react";
import BasicButton from "../../../components/CustomButton";
import CustomInputField from "../../../components/CustomInput";
import "./style.scss";
import Text from "../../../components/CustomText";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { toggleShowAdminOtp } from "../../../redux/slices/admin";
var pattern = /[a-zA-Z]@(ril)\.com\b$/g;

function Login() {
  const [emailId, setEmailId] = useState("");
  const [error, setError] = useState("");
  const [enabledNextBtn, setEnabledNxtBtn] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    if (error) {
      setEnabledNxtBtn(false);
    } else setEnabledNxtBtn(true);
  }, [error]);
  const navigate = useNavigate();

  useEffect(() => {
    const admin_logged_in = sessionStorage.getItem("admin_logged_in");
    if (admin_logged_in) {
      navigate("/onboarding/admin/");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="login-card">
      <Text appearance="heading-m" color="primary-grey-100">
        Login
      </Text>
      <Text className="subtitle" appearance="body-m" color="primary-grey-80">
        Enter Your registered Reliance Email Id
      </Text>
      <div className="homepage-content">
        <CustomInputField
          type="text"
          value={emailId}
          name="ril-emaild"
          label="Ril Email Id"
          pattern={pattern}
          setError={setError}
          handleChange={(val) => {
            setEmailId(val.target.value);
          }}
          fullWidth
          error={error}
        />
        <BasicButton
          fullWidth
          label={"Get OTP"}
          handleClick={() => {
            dispatch(toggleShowAdminOtp(true));
            navigate("/onboarding/admin/enter-otp");
          }}
          disabled={!enabledNextBtn}
        />
      </div>
    </div>
  );
}

export default Login;
