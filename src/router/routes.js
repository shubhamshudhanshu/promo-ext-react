import React from "react";
import onboardingRoutes from "./onboardingRoutes.js";

const routes = [
  // {
  //   Path: "/",
  //   Component: React.lazy(() => import("../container/Home.js")),
  //   meta: {
  //     name: "Home page",
  //   },
  // },
  {
    Path: "/company/:company_id/",
    Component: React.lazy(() => import("../container/Home.js")),
    meta: {
      name: "Home page",
    },
  },
  {
    Path: "/company/:company_id/promos",
    Component: React.lazy(() => import("../container/Promos.js")),
    meta: {
      name: "Promos",
    },
  },
  {
    Path: "/promos",
    Component: React.lazy(() => import("../container/Promos.js")),
    meta: {
      name: "Promos",
    },
  },
  {
    Path: "/company/:company_id/:promo_id",
    Component: React.lazy(() => import("../container/PromoDetail.js")),
    meta: {
      name: "Promo detail page",
    },
  },
  {
    Path: "/:promo_id",
    Component: React.lazy(() => import("../container/PromoDetail.js")),
    meta: {
      name: "Promo detail page",
    },
  },
  {
    Path: "/:catchAll(.*)",
    Component: React.lazy(() => import("../container/NotFound.js")),
  },
  ...onboardingRoutes,
];

export default routes;
