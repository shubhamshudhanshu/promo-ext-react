import React from "react";

const onboardingRoutes = [
  {
    Path: "/",
    Component: React.lazy(() =>
      import("../container/retailerOnboarding/index.js")
    ),
    meta: {
      name: "Home page",
    },
  },
  {
    Path: "/onboarding/admin/*",
    Component: React.lazy(() => import("../container/admin/index.js")),
    meta: {
      name: "Admin page",
    },
  },
  {
    Path: "/onboarding/termsAndConditions",
    Component: React.lazy(() =>
      import("../container/retailerOnboarding/termsAndCondition/index.js")
    ),
    meta: {
      name: "Home page",
    },
  },
  {
    Path: "/onboarding/*",
    Component: React.lazy(() =>
      import("../container/retailerOnboarding/index.js")
    ),
    meta: {
      name: "Home page",
    },
  },
];

export default onboardingRoutes;
