import React, { Suspense } from "react";
import { Route, Routes } from "react-router-dom";
import routes from "./routes";

export default function Routers() {
  return (
    <>
      <Suspense >
        <Routes>
          {routes.map((Routes, i) => {
            return (
              <Route
                exact
                key={i}
                path={Routes.Path}
                element={<Routes.Component />}
              />
            );
          })}
          <Route path="*" element={<p>Page not found</p>} />
        </Routes>
      </Suspense>
    </>
  );
}
