import React, { useEffect } from "react";
import Routers from "./router";
import "./App.css";
import { Notification } from "./components/CustomNotification";

export default function App() {
  const documentHeight = () => {
    const doc = document.documentElement;
    doc.style.setProperty("--doc-height", `${window.innerHeight}px`);
  };

  useEffect(() => {
    window.addEventListener("resize", documentHeight);
    documentHeight();
    return () => {
      window.removeEventListener("resize", documentHeight);
    };
  }, []);

  return (
    <div>
      <Routers />
      <Notification />
    </div>
  );
}
