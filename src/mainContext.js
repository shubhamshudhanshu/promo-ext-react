import { createContext, useContext } from "react";
export const MainContext = createContext();

const useMainContext = () => {
  const user = useContext(MainContext);
  return user;
};

export default useMainContext;
